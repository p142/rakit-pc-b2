// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:quickalert/quickalert.dart';
import 'package:rakit_pc/Models/model_user.dart';
import 'package:rakit_pc/Screen/home_page/homepage.dart';

postDetailsToFirestore(
    FirebaseAuth auth, String nama, String action, BuildContext context) async {
  // calling our firestore
  // calling our user model
  // sedning these values

  FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  User? user = auth.currentUser;

  UserModel userModel = UserModel(email: '', name: '', uid: '');

  // writing all the values

  if (action == "loginbiasa") {
    userModel.email = user!.email!;
    userModel.uid = user.uid;
    userModel.name = nama;
  } else if (action == "logingoogle") {
    userModel.email = auth.currentUser!.email!;
    userModel.uid = auth.currentUser!.uid;
    userModel.name = auth.currentUser!.displayName!;
  }

  await firebaseFirestore
      .collection("users")
      .doc(user?.uid)
      .set(userModel.toMap());
  if (action == "logingoogle") {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => HomePage()),
        (Route<dynamic> route) => false);
    QuickAlert.show(
        context: context,
        type: QuickAlertType.success,
        title: 'Berhasil Masuk',
        confirmBtnText: "Ok",
        confirmBtnColor: Colors.green);
  } else {
    Fluttertoast.showToast(msg: "Akun berhasil dibuat :) ");
  }
}
