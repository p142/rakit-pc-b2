import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:quickalert/quickalert.dart';
import 'package:rakit_pc/Login/Screens/Login/login_screen.dart';
import 'package:rakit_pc/Login/Screens/Signup/signup_screen.dart';
import 'package:rakit_pc/Login/Screens/Welcome/components/background.dart';
import 'package:rakit_pc/Login/components/rounded_button.dart';
import 'package:rakit_pc/service/google_auth.dart';
import 'package:rakit_pc/service/post_to_firestore.dart';

class BodyMobile extends StatefulWidget {
  const BodyMobile({Key? key}) : super(key: key);

  @override
  State<BodyMobile> createState() => _BodyMobileState();
}

class _BodyMobileState extends State<BodyMobile> {
  final String _nama = '';
  final String _action = "logingoogle";
  bool isLoading = false;

  final auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/RPC_final.png",
              width: size.width * 0.5,
            ),
            SizedBox(height: size.height * 0.05),
            SizedBox(height: size.height * 0.05),
            RoundedButton(
              text: "MASUK",
              color: Colors.black,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const LoginScreen();
                    },
                  ),
                );
              },
              loading: false,
            ),
            RoundedButton(
              loading: false,
              text: "DAFTAR",
              color: Colors.white,
              textColor: Colors.black,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const SignUpScreen();
                    },
                  ),
                );
              },
            ),
            TextButton.icon(
                style: ButtonStyle(backgroundColor:
                    MaterialStateProperty.resolveWith((states) {
                  if (states.contains(MaterialState.pressed)) {
                    return const Color(0xff9837a5);
                  }
                  return Colors.transparent;
                })),
                onPressed: () async {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return const LoginScreen();
                      },
                    ),
                  );
                },
                icon: (isLoading)
                    ? const SizedBox(
                        width: 16,
                        height: 16,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: 1.5,
                        ))
                    : Image.asset(
                        "assets/img/google_logo.png",
                        width: 50,
                        height: 50,
                      ),
                label: const Text(
                  "Masuk dengan Google",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  void googleSign() async {
    try {
      await FirebaseServices().signInWithGoogle();
      postDetailsToFirestore(auth, _nama, _action, context);
    } on FirebaseAuthException catch (error) {
      QuickAlert.show(
        context: context,
        type: QuickAlertType.error,
        title: 'Error',
        confirmBtnText: 'Ok',
        text: error.toString(),
      );
    }
  }
}
