// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:quickalert/quickalert.dart';
import 'package:rakit_pc/Login/Screens/ForgotPassword/forgot_pw_page.dart';
import 'package:rakit_pc/Login/Screens/Login/components/background.dart';
import 'package:rakit_pc/Login/Screens/Signup/signup_screen.dart';
import 'package:rakit_pc/Login/components/already_have_an_account_acheck.dart';
import 'package:rakit_pc/Login/components/forgot_password_check.dart';
import 'package:rakit_pc/Login/components/rounded_button.dart';
import 'package:rakit_pc/Login/components/rounded_input_field.dart';
import 'package:rakit_pc/Login/components/rounded_password_field.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rakit_pc/service/google_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../../../../../Screen/home_page/homepage.dart';
import '../../../../../service/post_to_firestore.dart';
import 'package:rakit_pc/global.dart' as global;

// import 'package:flutter_svg/svg.dart';
// import 'package:firebase_core/firebase_core.dart';

class BodyMobile extends StatefulWidget {
  const BodyMobile({Key? key}) : super(key: key);

  @override
  State<BodyMobile> createState() => _BodyMobileState();
}

class _BodyMobileState extends State<BodyMobile> {
  String _email = '';
  final String _nama = '';
  String _password = '';
  String _action = "logingoogle";
  final auth = FirebaseAuth.instance;
  String? errorMessage;
  bool isLoading = false;
  bool isLoading2 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/RPC_final.png",
              width: size.width * 0.4,
            ),
            SizedBox(height: size.height * 0.03),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "Email",
              onChanged: (value) {
                setState(() {
                  _email = value.trim();
                });
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                setState(() {
                  _password = value.trim();
                });
              },
            ),
            ForgotPasswordCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const ForgotPasswordPage();
                    },
                  ),
                );
              },
            ),
            RoundedButton(
              text: "Masuk",
              color: Colors.black,
              press: () async {
                final SharedPreferences login =
                    await SharedPreferences.getInstance();
                login.setString('email', _email);
                setState(() {
                  isLoading = true;
                });
                await Future.delayed(const Duration(seconds: 3));
                signIn(_email, _password);
                setState(() {
                  isLoading = false;
                });
              },
              loading: isLoading,
            ),
            // SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const SignUpScreen();
                    },
                  ),
                );
              },
            ),
            TextButton.icon(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith((states) {
                    if (states.contains(MaterialState.pressed)) {
                      return Colors.transparent;
                    }
                    return Colors.transparent;
                  }),
                ),
                onPressed: () async {
                  setState(() {
                    _action = "logingoogle";
                    global.action = "logingoogle";
                  });
                  googleSign();
                  setState(() {
                    isLoading2 = true;
                  });
                  await Future.delayed(const Duration(seconds: 2));
                  setState(() {
                    isLoading2 = false;
                  });
                },
                icon: (isLoading2)
                    ? const SizedBox(
                        width: 16,
                        height: 16,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: 1.5,
                        ))
                    : Image.asset(
                        "assets/img/google_logo.png",
                        width: 50,
                        height: 50,
                      ),
                label: const Text(
                  "Masuk dengan Google",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  void googleSign() async {
    try {
      await FirebaseServices().signInWithGoogle();
      postDetailsToFirestore(auth, _nama, _action, context);
    } on FirebaseAuthException catch (error) {
      QuickAlert.show(
          context: context,
          type: QuickAlertType.error,
          title: 'Terjadi Kesalahan',
          text: error.toString(),
          confirmBtnText: "Ok",
          confirmBtnColor: Colors.green);
    }
  }

  void signIn(String email, String password) async {
    try {
      await auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((uid) => {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (Route<dynamic> route) => false),
                QuickAlert.show(
                    context: context,
                    type: QuickAlertType.success,
                    title: 'Berhasil Masuk',
                    confirmBtnText: "Ok",
                    confirmBtnColor: Colors.green),
              });
    } on FirebaseAuthException catch (error) {
      switch (error.code) {
        case "invalid-email":
          errorMessage = "Password/Email yang dimasukan tidak valid.";
          break;
        case "wrong-password":
          errorMessage = "Password/Email yang dimasukan tidak valid.";
          break;
        case "user-not-found":
          errorMessage = "User dengan email tersebut tidak ditemukan.";
          break;
        case "user-disabled":
          errorMessage = "User with this email has been disabled.";
          break;
        case "too-many-requests":
          errorMessage = "Too many requests";
          break;
        case "operation-not-allowed":
          errorMessage = "Signing in with Email and Password is not enabled.";
          break;
        case "network-request-failed":
          errorMessage = "Tidak ada koneksi internet.";
          break;
        default:
          errorMessage = "Mohon maaf ada kesalahan, silakan coba lagi.";
      }
      Fluttertoast.showToast(msg: errorMessage!);
    }
  }
}
