// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:quickalert/quickalert.dart';
import 'package:rakit_pc/Login/Screens/Login/login_screen.dart';
import 'package:rakit_pc/Login/Screens/Signup/components/background.dart';
import 'package:rakit_pc/Login/Screens/Signup/components/or_divider.dart';
import 'package:rakit_pc/Login/components/already_have_an_account_acheck.dart';
import 'package:rakit_pc/Login/components/rounded_button.dart';
import 'package:rakit_pc/Login/components/rounded_input_field.dart';
import 'package:rakit_pc/Login/components/rounded_nama_field.dart';
import 'package:rakit_pc/Login/components/rounded_password_field.dart';
//import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:rakit_pc/service/post_to_firestore.dart';
// import 'package:flutter_svg/svg.dart';

class BodyMobile extends StatefulWidget {
  const BodyMobile({Key? key}) : super(key: key);

  @override
  State<BodyMobile> createState() => _BodyMobileState();
}

class _BodyMobileState extends State<BodyMobile> {
  String _email = '';
  String _password = '';
  String _nama = '';
  String _action = '';
  final auth = FirebaseAuth.instance;
  // string for displaying the error Message
  String? errorMessage;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/RPC_final.png",
              width: size.width * 0.4,
            ),
            SizedBox(height: size.height * 0.03),
            RoundedNamaField(
              hintText: " Name",
              onChanged: (value) {
                setState(() {
                  _nama = value.trim();
                });
              },
            ),
            RoundedInputField(
              hintText: "Email",
              onChanged: (value) {
                setState(() {
                  _email = value.trim();
                });
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                setState(() {
                  _password = value.trim();
                });
              },
            ),
            RoundedButton(
              loading: isLoading,
              color: Colors.black,
              text: "DAFTAR",
              press: () async {
                setState(() {
                  _action = "loginbiasa";
                });
                setState(() {
                  isLoading = true;
                });
                await Future.delayed(const Duration(seconds: 3));
                setState(() {
                  isLoading = false;
                });
                validateUser(_email, _password);
              },
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const LoginScreen();
                    },
                  ),
                );
              },
            ),
            const OrDivider(),
          ],
        ),
      ),
    );
  }

  void validateUser(String email, String password) async {
    try {
      await auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) =>
              {postDetailsToFirestore(auth, _nama, "loginbiasa", context)});

      QuickAlert.show(
          context: context,
          type: QuickAlertType.success,
          title: "Berhasil",
          text: '\nAkun Berhasil Dibuat!',
          confirmBtnText: 'Oke',
          confirmBtnColor: HexColor("#12c06a"),
          onConfirmBtnTap: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
          });
    } on FirebaseAuthException catch (error) {
      switch (error.code) {
        case "weak-password":
          errorMessage = "Password terlalu singkat.";
          break;
        case "email-already-in-use":
          errorMessage = "Akun dengan email ini sudah ada.";
          break;
        case "invalid-email":
          errorMessage = "Email yang dimasukan tidak valid.";
          break;
        case "empty-password":
          errorMessage = "Password/Email yang dimasukan tidak valid.";
          break;
        case "empty-email":
          errorMessage = "Password/Email yang dimasukan tidak valid.";
          break;
        case "empty-username":
          errorMessage = "Password/Email yang dimasukan tidak valid.";
          break;
        case "user-not-found":
          errorMessage = "User with this email doesn't exist.";
          break;
        case "user-disabled":
          errorMessage = "Anda sudah di banned.";
          break;
        case "too-many-requests":
          errorMessage = "Too many requests";
          break;
        case "operation-not-allowed":
          errorMessage = "Signing in with Email and Password is not enabled.";
          break;
        case "network-request-failed":
          errorMessage = "Tidak ada koneksi internet.";
          break;
        default:
          errorMessage = "Semua Kolom input harus diisi.";
          break;
      }
      Fluttertoast.showToast(msg: errorMessage!);
    }
  }
}
