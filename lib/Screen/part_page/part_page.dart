// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';

import 'package:rakit_pc/global.dart' as global;

import '../home_page/floating_button.dart';

class Part extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  Part({Key? key}) : super(key: key);
  @override
  State<Part> createState() => _PartState();
}

class _PartState extends State<Part> {
  @override
  void initState() {
    super.initState();
    global.compareOrList = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,
        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        title: Text(
          'List Part',
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Casing',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 10),
                            Image.asset(
                              "assets/img/computer-case.png",
                              width: 33,
                              height: 33,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Wadah dari Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/part/list/casing');
                      setState(() {
                        global.namaPart = "Casing";
                      });
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'CPU',
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                'assets/img/cpu.png',
                                height: 33,
                                width: 33,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Otak Dari Sebuah Komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/part/list/cpu');
                        setState(() {
                          global.namaPart = "CPU";
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'CPU Cooler',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 10),
                            Image.asset(
                              "assets/img/cooler.png",
                              width: 33,
                              height: 33,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Kipas Pendingin\nCPU Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/part/list/cooler');
                      setState(() {
                        global.namaPart = "CPU Cooler";
                      });
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'VGA',
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                'assets/img/vga.png',
                                height: 33,
                                width: 33,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Penerjemah tampilan komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/part/list/vga');
                        setState(() {
                          global.namaPart = "VGA";
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Motherboard',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 5),
                            Image.asset(
                              "assets/img/motherboard.png",
                              width: 33,
                              height: 33,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Dasar Penyambung Part Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/part/list/motherboard');
                      setState(() {
                        global.namaPart = "Motherboard";
                      });
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "RAM",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                "assets/img/ram.png",
                                height: 33,
                                width: 33,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Memori Sementara Komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/part/list/ram');
                        setState(() {
                          global.namaPart = "Ram";
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Fan',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 5),
                            Image.asset(
                              "assets/img/cooling-fan.png",
                              width: 33,
                              height: 33,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Kipas Pendingin Sistem Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/part/list/fan');
                      setState(() {
                        global.namaPart = "Fan";
                      });
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Storage",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                "assets/img/ssd.png",
                                height: 35,
                                width: 35,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Penyimpanan Storage Komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/part/list/storage');
                        setState(() {
                          global.namaPart = "Storage";
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Power Supply',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 5),
                            Image.asset(
                              "assets/img/psu.png",
                              width: 33,
                              height: 33,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Pensuplai Kelistrikan Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/part/list/psu');
                      setState(() {
                        global.namaPart = "PSU";
                      });
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Monitor",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                "assets/img/monitor.png",
                                height: 35,
                                width: 35,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Output Display Tampilan Komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/part/list/monitor');
                        setState(() {
                          global.namaPart = "Monitor";
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Mouse',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 5),
                            Image.asset(
                              "assets/img/mouse.png",
                              width: 33,
                              height: 33,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Peranti Interface Penunjuk Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/part/list/mouse');
                      setState(() {
                        global.namaPart = "Mouse";
                      });
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Keyboard",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                "assets/img/keyboard.png",
                                height: 35,
                                width: 35,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Peranti Interface Input Huruf & Symbol',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/part/list/keyboard');
                        setState(() {
                          global.namaPart = "Keyboard";
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 30),
        ],
      ),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: const BottomBar(),
    );
  }
}
