// ignore_for_file: unnecessary_this, prefer_const_constructors

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../Api/api_fan_id_nyar.dart';
import '../../../../../Models/models_fan.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/widget/search_widget.dart';

class ListFan extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  ListFan({Key? key}) : super(key: key);

  @override
  State<ListFan> createState() => _ListFanState();
}

class _ListFanState extends State<ListFan> {
  late Future data;
  var formatter = NumberFormat('#,###,000');
  List<Fan> fan = [];
  String query = '';
  Timer? debouncer;
  String request = 'AZ';

  @override
  void initState() {
    super.initState();

    init();
  }

  @override
  void dispose() {
    debouncer?.cancel();
    super.dispose();
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 1000),
  }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }

    debouncer = Timer(duration, callback);
  }

  Future init() async {
    final fann = await FanApi.fetchFanIdNyar(query, request);
    setState(() => this.fan = fann);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff342C4C),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: const Color(0xFF272B40).withOpacity(0.0),
        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              if (global.compareOrList == 0) {
                Navigator.pushNamed(context, '/part');
              } else {
                Navigator.pushNamed(context, '/compare/fan');
              }
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        title: const Text('Fans',
            style: TextStyle(
                color: Color(0xffDBD8E3),
                fontFamily: 'Inter',
                fontSize: 20,
                fontWeight: FontWeight.bold)),
      ),
      body: Column(
        children: <Widget>[
          Row(children: <Widget>[
            SizedBox(width: MediaQuery.of(context).size.width * 0.069),
            buildSearch(),
            SizedBox(
              width: 5,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.06,
              child: TextButton(
                onPressed: () {
                  showModalBottomSheet(
                      context: context,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(10))),
                      builder: (context) => Container(
                            padding: EdgeInsets.all(16),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 14),
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Filter",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black,
                                      height: 1.5,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 20),
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Urutkan",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0,
                                      color: Colors.black,
                                      height: 1.5,
                                    ),
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "AZ";
                                        });
                                        init();
                                        searchGan(query);
                                        Navigator.pop(context);
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Nama A-Z",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "ZA";
                                          init();
                                          searchGan(query);
                                          Navigator.pop(context);
                                        });
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Nama Z-A",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "Murah";
                                          init();
                                          searchGan(query);
                                          Navigator.pop(context);
                                        });
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Harga Terendah",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "Mahal";
                                          init();
                                          searchGan(query);
                                          Navigator.pop(context);
                                        });
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Harga Tertinggi",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 20),
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Spesifikasi",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0,
                                      color: Colors.black,
                                      height: 1.5,
                                    ),
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "Big";
                                        });
                                        init();
                                        searchGan(query);
                                        Navigator.pop(context);
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Size Fan Terbesar",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "Small";
                                        });
                                        init();
                                        searchGan(query);
                                        Navigator.pop(context);
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Size Fan Terkecil",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "RGB";
                                        });
                                        init();
                                        searchGan(query);
                                        Navigator.pop(context);
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("RGB",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ));
                },
                style: OutlinedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12), // <-- Radius
                  ),
                  side: BorderSide(
                    width: 1.0,
                    color: Colors.white.withOpacity(0.5),
                  ),
                ),
                child: Image.asset(
                  'assets/img/settings.png',
                  width: 22,
                  height: 22,
                ),
              ),
            ),
            SizedBox(
              height: 7,
            ),
          ]),
          SizedBox(height: 5),
          Expanded(
            child: ListView.builder(
                itemCount: fan.length,
                itemBuilder: (context, index) {
                  final hasil = fan[index];
                  return buildList(hasil, index);
                }),
          )
        ],
      ),
    );
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Cari Nama produk atau merk',
        onChanged: searchGan,
      );

  Widget buildList(Fan hasil, int index) => Card(
        elevation: 6,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: BorderSide(
              width: 2,
              color: Colors.white.withOpacity(0.5),
            )),
        margin: const EdgeInsets.fromLTRB(55, 15, 55, 10),
        child: InkWell(
          onTap: () {
            if (global.compareOrList == 0) {
              Navigator.pushNamed(context, '/part/list/detail');
            } else {
              Navigator.pushNamed(context, '/compare/fan');
            }

            setState(() {
              if (global.compareOrList == 0) {
                global.namaRoute = "fan";
                global.namaPart = "Fan";
                global.idDetail = int.parse(hasil.idFans) - 1;
              } else {
                if (global.idYangDiCompare == 1) {
                  global.idCompare1 = int.parse(hasil.idFans);
                } else if (global.idYangDiCompare == 2) {
                  global.idCompare2 = int.parse(hasil.idFans);
                }
              }
            });
          },
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.45,
            child: ListTile(
              //Text(questions[index])
              title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          child: Image.network(
                            hasil.imageLinks,
                            height: MediaQuery.of(context).size.height * 0.2,
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          hasil.namaFans,
                          style: GoogleFonts.poppins(
                            color: Color(0xff1C1255),
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                          ),
                        ),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              shadowColor: Colors.grey,
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                            ),
                            onPressed: () {},
                            child: Text(
                              "Rp ${formatter
                                      .format(int.parse(hasil.harga.toString()))}",
                              style: GoogleFonts.poppins(
                                color: Color(0xff1C1255),
                                fontWeight: FontWeight.w600,
                                fontSize: 13,
                              ),
                            )),
                        Text(
                          "Size : ${hasil.sizeFans} mm",
                          style: GoogleFonts.poppins(
                            color: Color(0xff1C1255),
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          "Speed : ${hasil.speedFans}",
                          style: GoogleFonts.poppins(
                            color: Color(0xff1C1255),
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              selected: true,
              selectedTileColor: const Color.fromARGB(221, 241, 237, 241),
            ),
          ),
        ),
      );

  Future searchGan(String query) async => debounce(() async {
        final fannn = await FanApi.fetchFanIdNyar(query, request);

        if (!mounted) return;

        setState(() {
          this.query = query;
          this.fan = fannn;
        });
      });
}
