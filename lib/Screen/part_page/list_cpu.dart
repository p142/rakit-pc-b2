// ignore_for_file: prefer_const_constructors

import 'dart:async';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import '../../Api/api_cpu_id_nyar.dart';
import '../../../../../Models/models_cpu.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/widget/search_widget.dart';

class ListCpu extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  ListCpu({Key? key}) : super(key: key);

  @override
  State<ListCpu> createState() => _ListCpuState();
}

class _ListCpuState extends State<ListCpu> {
  var formatter = NumberFormat('#,###,000');
  late Future data;
  List<Cpu> cpu = [];
  String query = '';
  Timer? debouncer;
  String request = 'AZ';

  @override
  void initState() {
    super.initState();

    init();
  }

  @override
  void dispose() {
    debouncer?.cancel();
    super.dispose();
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 1000),
  }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }

    debouncer = Timer(duration, callback);
  }

  Future init() async {
    final cpuu = await CpuApi.fetchCpuIdNyar(query, request);
    setState(() => cpu = cpuu);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff342C4C),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: const Color(0xFF272B40).withOpacity(0.0),
        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              if (global.compareOrList == 0) {
                Navigator.pushNamed(context, '/part');
              } else {
                Navigator.pushNamed(context, '/compare/cpu');
              }
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        title: const Text('Cpu',
            style: TextStyle(
                color: Color(0xffDBD8E3),
                fontFamily: 'Inter',
                fontSize: 20,
                fontWeight: FontWeight.bold)),
      ),
      body: Column(
        children: <Widget>[
          Row(children: <Widget>[
            SizedBox(width: MediaQuery.of(context).size.width * 0.069),
            buildSearch(),
            SizedBox(
              width: 5,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.06,
              child: TextButton(
                onPressed: () {
                  showModalBottomSheet(
                      context: context,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(10))),
                      builder: (context) => Container(
                            padding: EdgeInsets.all(16),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 14),
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Filter",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black,
                                      height: 1.5,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 20),
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Urutkan",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0,
                                      color: Colors.black,
                                      height: 1.5,
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Row(children: <Widget>[
                                        OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              request = "AZ";
                                              init();
                                              searchGan(query);
                                              Navigator.pop(context);
                                            });
                                          },
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0))),
                                          ),
                                          child: const Text("Nama A-Z",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Inter',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              request = "ZA";
                                              init();
                                              searchGan(query);
                                              Navigator.pop(context);
                                            });
                                          },
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0))),
                                          ),
                                          child: const Text("Nama Z-A",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Inter',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              request = "Murah";
                                              init();
                                              searchGan(query);
                                              Navigator.pop(context);
                                            });
                                          },
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0))),
                                          ),
                                          child: const Text("Harga Terendah",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Inter',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                      ])),
                                ),
                                Row(
                                  children: <Widget>[
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "Mahal";
                                          init();
                                          searchGan(query);
                                          Navigator.pop(context);
                                        });
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Harga Tertinggi",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 20),
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Spesifikasi",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0,
                                      color: Colors.black,
                                      height: 1.5,
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Row(children: <Widget>[
                                        OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              request = "Intel";
                                              init();
                                              searchGan(query);
                                              Navigator.pop(context);
                                            });
                                          },
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0))),
                                          ),
                                          child: const Text("Intel",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Inter',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              request = "AMD";
                                              init();
                                              searchGan(query);
                                              Navigator.pop(context);
                                            });
                                          },
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0))),
                                          ),
                                          child: const Text("AMD",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Inter',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              request = "Core";
                                              init();
                                              searchGan(query);
                                              Navigator.pop(context);
                                            });
                                          },
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0))),
                                          ),
                                          child: const Text("Core Terbanyak",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Inter',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        OutlinedButton(
                                          onPressed: () {
                                            setState(() {
                                              request = "Efisien";
                                              init();
                                              searchGan(query);
                                              Navigator.pop(context);
                                            });
                                          },
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0))),
                                          ),
                                          child: const Text("Watt Efisien",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontFamily: 'Inter',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                      ])),
                                ),
                                Row(
                                  children: <Widget>[
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "Boost";
                                          init();
                                          searchGan(query);
                                          Navigator.pop(context);
                                        });
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Boost Clock Tertinggi",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    OutlinedButton(
                                      onPressed: () {
                                        setState(() {
                                          request = "Unlocked";
                                          init();
                                          searchGan(query);
                                          Navigator.pop(context);
                                        });
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        15.0))),
                                      ),
                                      child: const Text("Unlocked Overclock",
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontFamily: 'Inter',
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ));
                },
                style: OutlinedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12), // <-- Radius
                  ),
                  side: BorderSide(
                    width: 1.0,
                    color: Colors.white.withOpacity(0.5),
                  ),
                ),
                child: Image.asset(
                  'assets/img/settings.png',
                  width: 22,
                  height: 22,
                ),
              ),
            ),
            SizedBox(
              height: 7,
            ),
          ]),
          SizedBox(height: 5),
          Expanded(
            child: ListView.builder(
                itemCount: cpu.length,
                itemBuilder: (context, index) {
                  final hasil = cpu[index];
                  return buildList(hasil, index);
                }),
          )
        ],
      ),
    );
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Cari Nama produk atau merk',
        onChanged: searchGan,
      );

  Widget buildList(Cpu hasil, int index) => Card(
        elevation: 6,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: BorderSide(
              width: 2,
              color: Colors.white.withOpacity(0.5),
            )),
        margin: const EdgeInsets.fromLTRB(55, 15, 55, 10),
        child: InkWell(
          onTap: () {
            if (global.compareOrList == 0) {
              Navigator.pushNamed(context, '/part/list/detail');
            } else {
              Navigator.pushNamed(context, '/compare/cpu');
            }
            setState(() {
              if (global.compareOrList == 0) {
                global.namaRoute = "cpu";
                global.idDetail = int.parse(hasil.idCpu.toString()) - 1;
                global.namaPart = "CPU";
              } else {
                if (global.idYangDiCompare == 1) {
                  global.idCompare1 = int.parse(hasil.idCpu);
                } else if (global.idYangDiCompare == 2) {
                  global.idCompare2 = int.parse(hasil.idCpu);
                }
              }
            });
          },
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.45,
            child: ListTile(
              //Text(questions[index])
              title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 5, 10, 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          child: Image.network(
                            hasil.imageLink,
                            height: MediaQuery.of(context).size.height * 0.2,
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          hasil.namaCpu,
                          style: GoogleFonts.poppins(
                            color: Color(0xff1C1255),
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                          ),
                        ),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              shadowColor: Colors.grey,
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                            ),
                            onPressed: () {},
                            child: Text(
                              // var formatter = NumberFormat('#,###,000');
                              "Rp ${formatter
                                      .format(int.parse(hasil.harga.toString()))}",
                              // hasil.harga.toString(),
                              style: GoogleFonts.poppins(
                                fontSize: 13,
                                color: Color(0xff1C1255),
                                fontWeight: FontWeight.w600,
                              ),
                            )),
                        Text(
                          "Max clock : ${hasil.maxClock} Ghz",
                          style: GoogleFonts.poppins(
                            fontSize: 12,
                            color: Color(0xff1C1255),
                          ),
                        ),
                        Text(
                          "Core/thread : ${hasil.coreCount}/${hasil.threadsCount} Cores",
                          style: GoogleFonts.poppins(
                            fontSize: 12,
                            color: Color(0xff1C1255),
                          ),
                        ),
                        Text(
                          "Socket : ${hasil.socket}",
                          style: GoogleFonts.poppins(
                            fontSize: 12,
                            color: Color(0xff1C1255),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              selected: true,
              selectedTileColor: const Color.fromARGB(221, 241, 237, 241),
            ),
          ),
        ),
      );

  Future searchGan(String query) async => debounce(() async {
        final cpuuu = await CpuApi.fetchCpuIdNyar(query, request);

        if (!mounted) return;

        setState(() {
          this.query = query;
          cpu = cpuuu;
        });
      });
}
