// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:url_launcher/url_launcher.dart';

import '../build_begginer/how_it_works.dart';

class CardHome extends StatelessWidget {
  const CardHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      body: ListView(
        children: <Widget>[
          const SizedBox(height: 15.0),
          Container(
              padding: const EdgeInsets.only(right: 0.0),
              width: MediaQuery.of(context).size.width - 30.0,
              height: MediaQuery.of(context).size.height - 100.0,
              child: GridView.count(
                  crossAxisCount: 2,
                  primary: false,
                  crossAxisSpacing: 2.0,
                  mainAxisSpacing: 15.0,
                  childAspectRatio: 0.8,
                  children: <Widget>[
                    _buildCard('Build PC', 'assets/img/build1.png', false,
                        false, context),
                    _buildCard('Part PC', 'assets/img/assemble.png', false,
                        false, context),
                    _buildCard('Compare Part', 'assets/img/comparee.png', false,
                        false, context),
                    _buildCard(
                        'Forum', 'assets/img/forum.png', false, false, context),
                  ])),
          const SizedBox(height: 15.0)
        ],
      ),
    );
  }

  Widget _buildCard(
      String name, String imgPath, bool added, bool isFavorite, context) {
    return Padding(
        padding: const EdgeInsets.only(
            top: 9.0, bottom: 13.0, left: 12.0, right: 12.0),
        child: InkWell(
          onTap: () {
            returnbutton(context, name);
          },
          child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  color: const Color.fromARGB(225, 121, 119, 255)),
              child: Column(children: [
                Padding(
                    padding: const EdgeInsets.all(13),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                    )),
                Hero(
                    tag: imgPath,
                    child: SizedBox(
                      height: 100.0,
                      width: 100.0,
                      child: Image.asset(imgPath),
                    )),
                const SizedBox(height: 23.0),
                Text(
                  name,
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ])),
        ));
  }
}

Future returnbutton(context, name) {
  const url = "https://www.facebook.com/groups/1191013355164961";

  if (name == "Build PC") {
    return Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.bottomToTop, child: HowItWorks()));
  } else if (name == "Part PC") {
    return Navigator.pushNamed(context, '/part');
  } else if (name == "Compare Part") {
    return Navigator.pushNamed(context, '/compare');
  } else {
    return launch(url);
  }
}
