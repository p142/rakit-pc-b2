// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:page_transition/page_transition.dart';

import '../build_begginer/how_it_works.dart';

class FloatingBar extends StatelessWidget {
  const FloatingBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.bottomToTop, child: HowItWorks()));
      },
      backgroundColor: HexColor("#7A77FF"),
      child: const Icon(
        Icons.build,
        size: 30,
      ),
    );
  }
}
