// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        // ignore: prefer_const_constructors
        shape: CircularNotchedRectangle(),
        notchMargin: 10.0,
        color: Colors.transparent,
        elevation: 9.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
            height: 70.0,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0),
                    bottomLeft: Radius.circular(25.0),
                    bottomRight: Radius.circular(25.0)),
                color: Color(0xFFDBD8E3)),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    child: SizedBox(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width / 2 - 40.0,
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(Icons.home,
                              color: Color(0xFF7A77FF), size: 43),
                        ],
                      ),
                    ),
                    onTap: () {
                      if (ModalRoute.of(context)?.settings.name != "/") {
                        // Navigator.pushNamed(context, "/");
                        Navigator.pushReplacementNamed(context, '/');
                      }
                    },
                  ),
                  InkWell(
                    child: SizedBox(
                        height: 50.0,
                        width: MediaQuery.of(context).size.width / 2 - 40.0,
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(Icons.history,
                                color: Color(0xFF7A77FF), size: 46),
                          ],
                        )),
                    onTap: () {
                      if (ModalRoute.of(context)?.settings.name != "/history") {
                        Navigator.pushReplacementNamed(context, '/history');
                      }
                    },
                  )
                ])));
  }
}
