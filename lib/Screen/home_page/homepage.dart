// ignore_for_file: prefer_const_constructors

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quickalert/quickalert.dart';
import 'package:rakit_pc/Login/Screens/Welcome/welcome_screen.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:rakit_pc/Screen/home_page/card_homepage.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/service/google_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rakit_pc/Models/model_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'floating_button.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  // User user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel(email: '', name: '', uid: '');

  late SharedPreferences logindata;
  void checkLogin() async {
    logindata = await SharedPreferences.getInstance();
    var email = logindata.getString('email');
    if (email != null) {
      setState(() {
        global.currentState = const HomePage();
      });
    } else {
      setState(() {
        global.currentState = const WelcomeScreen();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 1, vsync: this);
    global.dariSimpan = false;
    global.idBuilds = 0;
    global.idCaseAdv = 0;
    global.idVgaAdv = 0;
    global.idCpuAdv = 0;
    global.compatible = "All parts compatible";
    global.idCpuCoolerAdv = 0;
    global.idFanAdv = 0;
    global.idFan2Adv = 0;
    global.idFan3Adv = 0;
    global.idMoboAdv = 0;
    global.idPart = 0;
    global.socketMobo = "";
    global.socketCpu = "";
    global.idPsuAdv = 0;
    global.idRamAdv = 0;
    global.idRam2Adv = 0;
    global.idStorageAdv = 0;
    global.idStorage2Adv = 0;
    global.idRamAdv = 0;
    global.namaPart = "";
    global.sengDiganti = 0;
    global.hargaCase = 0;
    global.hargaCooler = 0;
    global.hargaCpu = 0;
    global.hargaVga = 0;
    global.hargaFan1 = 0;
    global.hargaFan2 = 0;
    global.hargaFan3 = 0;
    global.hargaMobo = 0;
    global.hargaPsu = 0;
    global.hargaRam1 = 0;
    global.hargaRam2 = 0;
    global.hargaStorage1 = 0;
    global.hargaStorage2 = 0;
    global.hargaHarga = 0;
    global.wattTotal = 0;
    global.wattCpu = 0;
    global.wattVga = 0;
    global.wattCooler = 0;
    global.wattFan1 = 0;
    global.wattFan2 = 0;
    global.wattFan3 = 0;
    global.wattRam1 = 0;
    global.wattRam2 = 0;
    global.wattStorage1 = 0;
    global.wattStorage2 = 0;
    global.idCompare1 = 0;
    global.idCompare2 = 0;
    global.namaCpu = "";
    global.namaRam = "";
    global.namaMobo = "";
    global.namaCase = "";
    global.namaVga = "";
    global.namaPsu = "";
    global.namaStorage = "";
    global.namaFan = "";
    global.namaCpuCooler = "";
    global.moboSupportNvme = false;
    global.storageNvme = false;
    global.compareOrList = 0;

    checkLogin();

    //get the data from firestore
    FirebaseFirestore.instance
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser?.uid)
        .get()
        .then((value) {
      loggedInUser = UserModel.fromMap(value.data());
      setState(() {
        global.uidUser = loggedInUser.uid;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        title: Text(
          "HomePage",
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.exit_to_app_rounded, color: Colors.white),
            onPressed: () {
              QuickAlert.show(
                  context: context,
                  type: QuickAlertType.warning,
                  title: "Warning",
                  text: '\nApakah Anda Yakin Ingin Logout',
                  confirmBtnText: 'Iya',
                  showCancelBtn: true,
                  cancelBtnText: 'Tidak',
                  confirmBtnColor: Colors.green,
                  onConfirmBtnTap: () async {
                    Navigator.pop(context);
                    logout(context, global.action);
                  },
                  onCancelBtnTap: () {
                    Navigator.pop(context);
                  });
            },
          ),
        ],
      ),
      body: ListView(
        shrinkWrap: false,
        children: <Widget>[
          Container(
            //color: Colors.blueGrey,
            padding: const EdgeInsets.fromLTRB(15, 2, 0, 1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  greetingMessage(),
                  style: GoogleFonts.poppins(
                    color: Color.fromARGB(255, 168, 164, 164),
                    fontSize: 17,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Text(
                  loggedInUser.name,
                  style: GoogleFonts.poppins(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
              height: MediaQuery.of(context).size.height - 60.0,
              width: double.infinity,
              child: TabBarView(
                  controller: _tabController, children: const [CardHome()]))
        ],
      ),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: const BottomBar(),
    );
  }

  //this methode for logout
  Future<void> logout(BuildContext context, String action) async {
    if (global.action == action) {
      await FirebaseServices().googleSignOut();
    } else {
      await FirebaseAuth.instance.signOut();
    }

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => const WelcomeScreen()),
        (Route<dynamic> route) => false);
    logindata = await SharedPreferences.getInstance();
    setState(() {
      logindata.remove('email');
      global.currentState = const WelcomeScreen();
    });
  }

  //Methode for greeting based on time
  String greetingMessage() {
    var timeNow = DateTime.now().hour;

    if (timeNow <= 11) {
      return 'Selamat Pagi, ';
    } else if (timeNow > 11 && timeNow <= 16) {
      return 'Selamat Siang, ';
    } else if (timeNow > 16 && timeNow < 20) {
      return 'Selamat Sore, ';
    } else {
      return 'Selamat Malam, ';
    }
  }
}
