// ignore_for_file: unused_import, prefer_const_constructors_in_immutables, prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/Models/models_storage.dart';
import '../../Api/api_storage.dart';
import '../home_page/floating_button.dart';

class CompareStorage extends StatefulWidget {
  CompareStorage({Key? key}) : super(key: key);
  @override
  State<CompareStorage> createState() => _CompareStorageState();
}

class _CompareStorageState extends State<CompareStorage> {
  var formatter = NumberFormat('#,###,000');
  late Future data;
  List<Storage> data3 = [];

  @override
  void initState() {
    data = fetchStorage();
    data.then((value) => setState(() {
          data3 = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,

        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/compare/list');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        //backgroundColor: Color.fromARGB(240, 143, 5, 131),
        title: Text(
          "Compare Storage",
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: result(),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget result() {
    if (global.idCompare1 != 0 && global.idCompare2 != 0) {
      return ListView(
        children: [
          SizedBox(
            child: adaDua(),
          ),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          if (global.idCompare1 == 0 && global.idCompare2 == 0) ...[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.47,
              height: MediaQuery.of(context).size.height * 0.76,
              child: kosongkiri(),
            ),
            const SizedBox(
              width: 20,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.47,
                height: MediaQuery.of(context).size.height * 0.76,
                child: kosongkanan()),
          ] else if (global.idCompare1 != 0 && global.idCompare2 == 0) ...[
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKiri(),
              ),
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkanan()),
          ] else if (global.idCompare1 == 0 && global.idCompare2 != 0) ...[
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkiri()),
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKanan(),
              ),
            ),
          ]
        ],
      );
    }
  }

  Widget adaDua() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare1 - 1].imageLink))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/storage/list');
                    setState(() {
                      global.idYangDiCompare = 1;
                    });
                  },
                ),
                SizedBox(
                  width: 10,
                ),
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare2 - 1].imageLink))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/storage/list');
                    setState(() {
                      global.idYangDiCompare = 2;
                    });
                  },
                ),
              ]),
              DataTable(
                dataRowHeight: MediaQuery.of(context).size.height * 0.1,
                columnSpacing: MediaQuery.of(context).size.width * 0.05,
                border: TableBorder(
                    horizontalInside: BorderSide(color: Colors.white, width: 2),
                    top: BorderSide.none),
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                  DataColumn(
                    label: Text(""),
                  ),
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
                rows: <DataRow>[
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Container(
                          width: double.infinity,
                          child: const Text(
                            'Nama Storage',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Container(
                            width: 80,
                            child: Text(
                                data3[global.idCompare1 - 1].namaStorage,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Container(
                            width: 80,
                            child:
                                Text(data3[global.idCompare2 - 1].namaStorage,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7),
                                    )),
                          ),
                        ),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Merek Storage',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].merkStorage,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].merkStorage,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Harga Storage',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare1 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare2 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Type Storage',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (data3[global.idCompare2 - 1].typeStorage == "HDD" &&
                          data3[global.idCompare1 - 1].typeStorage !=
                              "HDD") ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].typeStorage,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].typeStorage,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (data3[global.idCompare2 - 1].typeStorage !=
                              "HDD" &&
                          data3[global.idCompare1 - 1].typeStorage ==
                              "HDD") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].typeStorage,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].typeStorage,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].typeStorage,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].typeStorage,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Storage Capacity',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(
                              data3[global.idCompare1 - 1].storageCapacity) >
                          int.parse(data3[global.idCompare2 - 1]
                              .storageCapacity)) ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1]
                                            .storageCapacity}GB",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(
                              "${data3[global.idCompare2 - 1].storageCapacity}GB",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].storageCapacity) <
                          int.parse(data3[global.idCompare2 - 1]
                              .storageCapacity)) ...[
                        DataCell(
                          Text(
                              "${data3[global.idCompare1 - 1].storageCapacity}GB",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1]
                                            .storageCapacity}GB",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare1 - 1]
                                            .storageCapacity}GB",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare2 - 1]
                                            .storageCapacity}GB",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Storage Interface',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (data3[global.idCompare2 - 1]
                                  .storageInterface
                                  .substring(0, 3) ==
                              "SAT" &&
                          data3[global.idCompare1 - 1]
                                  .storageInterface
                                  .substring(0, 3) !=
                              "SAT") ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1]
                                        .storageInterface,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].storageInterface,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (data3[global.idCompare1 - 1]
                                  .storageInterface
                                  .substring(0, 3) ==
                              "SAT" &&
                          data3[global.idCompare2 - 1]
                                  .storageInterface
                                  .substring(0, 3) !=
                              "SAT") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].storageInterface,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1]
                                        .storageInterface,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1]
                                        .storageInterface,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1]
                                        .storageInterface,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Read Speed',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].readSpeed) >
                          int.parse(
                              data3[global.idCompare2 - 1].readSpeed)) ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].readSpeed}MB/s",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text("${data3[global.idCompare2 - 1].readSpeed}MB/s",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].readSpeed) <
                          int.parse(
                              data3[global.idCompare2 - 1].readSpeed)) ...[
                        DataCell(
                          Text("${data3[global.idCompare1 - 1].readSpeed}MB/s",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].readSpeed}MB/s",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].readSpeed}MB/s",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].readSpeed}MB/s",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Write Speed',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].writeSpeed) >
                          int.parse(
                              data3[global.idCompare2 - 1].writeSpeed)) ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].writeSpeed}MB/s",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text("${data3[global.idCompare2 - 1].writeSpeed}MB/s",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].writeSpeed) <
                          int.parse(
                              data3[global.idCompare2 - 1].writeSpeed)) ...[
                        DataCell(
                          Text("${data3[global.idCompare1 - 1].writeSpeed}MB/s",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].writeSpeed}MB/s",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].writeSpeed}MB/s",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].writeSpeed}MB/s",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Cache',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (data3[global.idCompare1 - 1].cache != "No" &&
                          data3[global.idCompare2 - 1].cache == "No") ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(data3[global.idCompare1 - 1].cache,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].cache,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (data3[global.idCompare2 - 1].cache != "No" &&
                          data3[global.idCompare1 - 1].cache == "No") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].cache,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(data3[global.idCompare2 - 1].cache,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(data3[global.idCompare1 - 1].cache,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(data3[global.idCompare2 - 1].cache,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Storage Watt',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].storageWatt) <
                          int.parse(
                              data3[global.idCompare2 - 1].storageWatt)) ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].storageWatt} Watt",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(
                              "${data3[global.idCompare2 - 1].storageWatt} Watt",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].storageWatt) >
                          int.parse(
                              data3[global.idCompare2 - 1].storageWatt)) ...[
                        DataCell(
                          Text(
                              "${data3[global.idCompare1 - 1].storageWatt} Watt",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].storageWatt} Watt",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Text(
                              "${data3[global.idCompare1 - 1].storageWatt} Watt",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(
                              "${data3[global.idCompare2 - 1].storageWatt} Watt",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Form Factor',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].formFactor,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].formFactor,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Rpm',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].rpm,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].rpm,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
  }

  Widget kosongkiri() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(left: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah Storage",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/storage/list');
        setState(() {
          global.idYangDiCompare = 1;
        });
      },
    );
  }

  Widget kosongkanan() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(right: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah Storage",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/storage/list');
        setState(() {
          global.idYangDiCompare = 2;
        });
      },
    );
  }

  Widget keisiKiriCompare() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/storage/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merk Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                if (data3[global.idCompare2 - 1].typeStorage == "HDD" &&
                    data3[global.idCompare1 - 1].typeStorage != "HDD") ...[
                  Text("Type Storage",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].typeStorage}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare2 - 1].typeStorage != "HDD" &&
                    data3[global.idCompare1 - 1].typeStorage == "HDD") ...[
                  Text("Type Storage",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].typeStorage}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Type Storage",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].typeStorage}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].storageCapacity) >
                    int.parse(
                        data3[global.idCompare2 - 1].storageCapacity)) ...[
                  Text("Storage Capacity",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare1 - 1].storageCapacity}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].storageCapacity) <
                    int.parse(
                        data3[global.idCompare2 - 1].storageCapacity)) ...[
                  Text("Storage Capacity",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare1 - 1].storageCapacity}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Storage Capacity",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare1 - 1].storageCapacity}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (data3[global.idCompare2 - 1]
                            .storageInterface
                            .substring(0, 3) ==
                        "SAT" &&
                    data3[global.idCompare1 - 1]
                            .storageInterface
                            .substring(0, 3) !=
                        "SAT") ...[
                  Text("Storage Interface",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].storageInterface}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare1 - 1]
                            .storageInterface
                            .substring(0, 3) ==
                        "SAT" &&
                    data3[global.idCompare2 - 1]
                            .storageInterface
                            .substring(0, 3) !=
                        "SAT") ...[
                  Text("Storage Interface",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].storageInterface}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Storage Interface",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].storageInterface}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].readSpeed) >
                    int.parse(data3[global.idCompare2 - 1].readSpeed)) ...[
                  Text("Read Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].readSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].readSpeed) <
                    int.parse(data3[global.idCompare2 - 1].readSpeed)) ...[
                  Text("Read Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].readSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Read Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].readSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].writeSpeed) >
                    int.parse(data3[global.idCompare2 - 1].writeSpeed)) ...[
                  Text("Write Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].writeSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].writeSpeed) <
                    int.parse(data3[global.idCompare2 - 1].writeSpeed)) ...[
                  Text("Write Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].writeSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Write Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].writeSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (data3[global.idCompare1 - 1].cache != "No" &&
                    data3[global.idCompare2 - 1].cache == "No") ...[
                  Text("Cache",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].cache}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare2 - 1].cache != "No" &&
                    data3[global.idCompare1 - 1].cache == "No") ...[
                  Text("Cache",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].cache}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Cache",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].cache}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].storageWatt) <
                    int.parse(data3[global.idCompare2 - 1].storageWatt)) ...[
                  Text("Storage Watt",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare1 - 1].storageWatt} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].storageWatt) >
                    int.parse(data3[global.idCompare2 - 1].storageWatt)) ...[
                  Text("Storage Watt",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare1 - 1].storageWatt} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Storage Watt",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare1 - 1].storageWatt} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Rpm",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].rpm}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKananCompare() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/storage/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merk Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                if (data3[global.idCompare1 - 1].typeStorage == "HDD" &&
                    data3[global.idCompare2 - 1].typeStorage != "HDD") ...[
                  Text("Type Storage",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].typeStorage}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare1 - 1].typeStorage != "HDD" &&
                    data3[global.idCompare2 - 1].typeStorage == "HDD") ...[
                  Text("Type Storage",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].typeStorage}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Type Storage",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].typeStorage}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].storageCapacity) <
                    int.parse(
                        data3[global.idCompare2 - 1].storageCapacity)) ...[
                  Text("Storage Capacity",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare2 - 1].storageCapacity}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].storageCapacity) >
                    int.parse(
                        data3[global.idCompare2 - 1].storageCapacity)) ...[
                  Text("Storage Capacity",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare2 - 1].storageCapacity}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Storage Capacity",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare2 - 1].storageCapacity}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (data3[global.idCompare1 - 1]
                            .storageInterface
                            .substring(0, 3) ==
                        "SAT" &&
                    data3[global.idCompare2 - 1]
                            .storageInterface
                            .substring(0, 3) !=
                        "SAT") ...[
                  Text("Storage Interface",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].storageInterface}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare2 - 1]
                            .storageInterface
                            .substring(0, 3) ==
                        "SAT" &&
                    data3[global.idCompare1 - 1]
                            .storageInterface
                            .substring(0, 3) !=
                        "SAT") ...[
                  Text("Storage Interface",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].storageInterface}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Storage Interface",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].storageInterface}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].readSpeed) <
                    int.parse(data3[global.idCompare2 - 1].readSpeed)) ...[
                  Text("Read Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].readSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].readSpeed) >
                    int.parse(data3[global.idCompare2 - 1].readSpeed)) ...[
                  Text("Read Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].readSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Read Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].readSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].writeSpeed) <
                    int.parse(data3[global.idCompare2 - 1].writeSpeed)) ...[
                  Text("Write Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].writeSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].writeSpeed) >
                    int.parse(data3[global.idCompare2 - 1].writeSpeed)) ...[
                  Text("Write Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].writeSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Write Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].writeSpeed}MB/s\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (data3[global.idCompare2 - 1].cache != "No" &&
                    data3[global.idCompare1 - 1].cache == "No") ...[
                  Text("Cache",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].cache}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare1 - 1].cache != "No" &&
                    data3[global.idCompare2 - 1].cache == "No") ...[
                  Text("Cache",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].cache}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Cache",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].cache}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].storageWatt) >
                    int.parse(data3[global.idCompare2 - 1].storageWatt)) ...[
                  Text("Storage Watt",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare2 - 1].storageWatt} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].storageWatt) <
                    int.parse(data3[global.idCompare2 - 1].storageWatt)) ...[
                  Text("Storage Watt",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare2 - 1].storageWatt} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Storage Watt",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare2 - 1].storageWatt} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Rpm",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].rpm}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKanan() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/storage/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Type Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].typeStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merk Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Storage Capacity",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].storageCapacity}GB\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Storage Interface",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].storageInterface}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Read Speed",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].readSpeed}MB/s\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Write Speed",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].writeSpeed}MB/s\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Cache",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].cache}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Storage Watt",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].storageWatt} Watt\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Rpm",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].rpm}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKiri() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/storage/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Type Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].typeStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merk Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkStorage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga Storage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Storage Capacity",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].storageCapacity}GB\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Storage Interface",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].storageInterface}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Read Speed",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].readSpeed}MB/s\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Write Speed",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].writeSpeed}MB/s\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Cache",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].cache}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Storage Watt",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].storageWatt} Watt\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Rpm",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].rpm}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }
}
