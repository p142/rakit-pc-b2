// ignore_for_file: prefer_const_constructors, unnecessary_const, unused_import, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:rakit_pc/Screen/home_page/floating_button.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/Models/models_ram.dart';
import '../../Api/api_ram.dart';
import '../build_begginer/how_it_works.dart';

class CompareRam extends StatefulWidget {
  CompareRam({Key? key}) : super(key: key);
  @override
  State<CompareRam> createState() => _CompareRamState();
}

class _CompareRamState extends State<CompareRam> {
  var formatter = NumberFormat('#,###,000');
  late Future data;
  List<Ram> data3 = [];

  @override
  void initState() {
    data = fetchRam();
    data.then((value) => setState(() {
          data3 = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,

        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/compare/list');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        //backgroundColor: Color.fromARGB(240, 143, 5, 131),
        title: Text(
          "Compare RAM",
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: result(),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget result() {
    if (global.idCompare1 != 0 && global.idCompare2 != 0) {
      return ListView(
        children: [
          SizedBox(
            child: adaDua(),
          ),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          if (global.idCompare1 == 0 && global.idCompare2 == 0) ...[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.47,
              height: MediaQuery.of(context).size.height * 0.76,
              child: kosongkiri(),
            ),
            const SizedBox(
              width: 20,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.47,
                height: MediaQuery.of(context).size.height * 0.76,
                child: kosongkanan()),
          ] else if (global.idCompare1 != 0 && global.idCompare2 == 0) ...[
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKiri(),
              ),
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkanan()),
          ] else if (global.idCompare1 == 0 && global.idCompare2 != 0) ...[
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkiri()),
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKanan(),
              ),
            ),
          ]
        ],
      );
    }
  }

  Widget adaDua() {
    return data3.isEmpty
        ? const Center(
            child: const CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare1 - 1].imageLink))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/ram/list');
                    setState(() {
                      global.idYangDiCompare = 1;
                    });
                  },
                ),
                SizedBox(
                  width: 10,
                ),
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare2 - 1].imageLink))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/ram/list');
                    setState(() {
                      global.idYangDiCompare = 2;
                    });
                  },
                ),
              ]),
              DataTable(
                dataRowHeight: MediaQuery.of(context).size.height * 0.1,
                columnSpacing: MediaQuery.of(context).size.width * 0.05,
                border: TableBorder(
                    horizontalInside: BorderSide(color: Colors.white, width: 2),
                    top: BorderSide.none),
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                  DataColumn(
                    label: Text(""),
                  ),
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
                rows: <DataRow>[
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        SizedBox(
                          width: double.infinity,
                          child: const Text(
                            'Nama RAM',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      DataCell(
                        SizedBox(
                          width: 100,
                          child: Text(data3[global.idCompare1 - 1].namaRam,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].namaRam,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7),
                            )),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Merek RAM',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].merkRam,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].merkRam,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Harga RAM',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare1 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare2 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Memory Type',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1]
                              .memoryType
                              .substring(3, 4)) >
                          int.parse(data3[global.idCompare2 - 1]
                              .memoryType
                              .substring(3, 4))) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].memoryType,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(data3[global.idCompare1 - 1]
                              .memoryType
                              .substring(3, 4)) <
                          int.parse(data3[global.idCompare2 - 1]
                              .memoryType
                              .substring(3, 4))) ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryType,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Memory Size',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].memorySize) >
                          int.parse(
                              data3[global.idCompare2 - 1].memorySize)) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].memorySize}GB",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text("${data3[global.idCompare2 - 1].memorySize}GB",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].memorySize) <
                          int.parse(
                              data3[global.idCompare2 - 1].memorySize)) ...[
                        DataCell(
                          Text("${data3[global.idCompare1 - 1].memorySize}GB",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].memorySize}GB",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].memorySize}GB",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].memorySize}GB",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Memory Speed',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].memorySpeed) >
                          int.parse(
                              data3[global.idCompare2 - 1].memorySpeed)) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].memorySpeed}Mhz",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text("${data3[global.idCompare2 - 1].memorySpeed}Mhz",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].memorySpeed) <
                          int.parse(
                              data3[global.idCompare2 - 1].memorySpeed)) ...[
                        DataCell(
                          Text("${data3[global.idCompare1 - 1].memorySpeed}Mhz",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].memorySpeed}Mhz",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].memorySpeed}Mhz",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].memorySpeed}Mhz",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Latency RAM',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].latencyCl) <
                          int.parse(
                              data3[global.idCompare2 - 1].latencyCl)) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].latencyCl,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].latencyCl,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].latencyCl) >
                          int.parse(
                              data3[global.idCompare2 - 1].latencyCl)) ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].latencyCl,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].latencyCl,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].latencyCl,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].latencyCl,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Voltage',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text("${data3[global.idCompare1 - 1].voltage}V",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text("${data3[global.idCompare2 - 1].voltage}V",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
  }

  Widget kosongkiri() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(left: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah Ram",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/ram/list');
        setState(() {
          global.idYangDiCompare = 1;
        });
      },
    );
  }

  Widget kosongkanan() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(right: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah Ram",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/ram/list');
        setState(() {
          global.idYangDiCompare = 2;
        });
      },
    );
  }

  Widget keisiKiriCompare() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/ram/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaRam}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkRam}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(3, 4)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(3, 4))) ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(3, 4)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(3, 4))) ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1].memorySize) <
                    int.parse(data3[global.idCompare2 - 1].memorySize)) ...[
                  Text("Memory Size",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].memorySize}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else if (int.parse(data3[global.idCompare1 - 1].memorySize) >
                    int.parse(data3[global.idCompare2 - 1].memorySize)) ...[
                  Text("Memory Size",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].memorySize}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else ...[
                  Text("Memory Size",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memorySize}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1].memorySpeed) <
                    int.parse(data3[global.idCompare2 - 1].memorySpeed)) ...[
                  Text("Memory Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].memorySpeed}Mhz\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else if (int.parse(data3[global.idCompare1 - 1].memorySpeed) >
                    int.parse(data3[global.idCompare2 - 1].memorySpeed)) ...[
                  Text("Memory Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].memorySpeed}Mhz\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else ...[
                  Text("Memory Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memorySpeed}Mhz\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1].latencyCl) >
                    int.parse(data3[global.idCompare2 - 1].latencyCl)) ...[
                  Text("Latency RAM",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("CL${data3[global.idCompare1 - 1].latencyCl}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else if (int.parse(data3[global.idCompare1 - 1].latencyCl) <
                    int.parse(data3[global.idCompare2 - 1].latencyCl)) ...[
                  Text("Latency RAM",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("CL${data3[global.idCompare1 - 1].latencyCl}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else ...[
                  Text("Latency RAM",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("CL${data3[global.idCompare1 - 1].latencyCl}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                Text("Voltage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].voltage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKananCompare() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/ram/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaRam}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkRam}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(3, 4)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(3, 4))) ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(3, 4)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(3, 4))) ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1].memorySize) <
                    int.parse(data3[global.idCompare2 - 1].memorySize)) ...[
                  Text("Memory Size",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].memorySize}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].memorySize) >
                    int.parse(data3[global.idCompare2 - 1].memorySize)) ...[
                  Text("Memory Size",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].memorySize}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Memory Size",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].memorySize}GB\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1].memorySpeed) >
                    int.parse(data3[global.idCompare2 - 1].memorySpeed)) ...[
                  Text("Memory Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].memorySpeed}Mhz\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else if (int.parse(data3[global.idCompare1 - 1].memorySpeed) <
                    int.parse(data3[global.idCompare2 - 1].memorySpeed)) ...[
                  Text("Memory Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].memorySpeed}Mhz\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else ...[
                  Text("Memory Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].memorySpeed}Mhz\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1].latencyCl) <
                    int.parse(data3[global.idCompare2 - 1].latencyCl)) ...[
                  Text("Latency RAM",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("CL${data3[global.idCompare2 - 1].latencyCl}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else if (int.parse(data3[global.idCompare1 - 1].latencyCl) >
                    int.parse(data3[global.idCompare2 - 1].latencyCl)) ...[
                  Text("Latency RAM",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("CL${data3[global.idCompare2 - 1].latencyCl}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else ...[
                  Text("Latency RAM",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("CL${data3[global.idCompare2 - 1].latencyCl}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                Text("Voltage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].voltage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKanan() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/ram/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaRam}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkRam}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Memory Type",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].memoryType}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Memory Size",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].memorySize}GB\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Memory Speed",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].memorySpeed}Mhz\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Latency RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("CL${data3[global.idCompare2 - 1].latencyCl}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Voltage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].voltage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKiri() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/ram/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaRam}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkRam}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga RAM",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Memory Type",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].memoryType}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Memory Size",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].memorySize}GB\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Memory Speed",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].memorySpeed}Mhz\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Latency ram",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("CL${data3[global.idCompare1 - 1].latencyCl}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Voltage",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].voltage}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }
}
