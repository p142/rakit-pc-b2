// ignore_for_file: prefer_const_constructors, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:intl/intl.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/Models/models_motherboard.dart';
import '../../Api/api_motherboard.dart';
import '../home_page/floating_button.dart';

class CompareMobo extends StatefulWidget {
  CompareMobo({Key? key}) : super(key: key);
  @override
  State<CompareMobo> createState() => _CompareMoboState();
}

class _CompareMoboState extends State<CompareMobo> {
  var formatter = NumberFormat('#,###,000');
  late Future data;
  List<Motherboard> data3 = [];

  @override
  void initState() {
    data = fetchMotherboard();
    data.then((value) => setState(() {
          data3 = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,

        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/compare/list');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        //backgroundColor: Color.fromARGB(240, 143, 5, 141),
        title: Text(
          "Compare Motherboard",
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: result(),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget result() {
    if (global.idCompare1 != 0 && global.idCompare2 != 0) {
      return ListView(
        children: [
          SizedBox(
            child: adaDua(),
          ),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          if (global.idCompare1 == 0 && global.idCompare2 == 0) ...[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.47,
              height: MediaQuery.of(context).size.height * 0.76,
              child: kosongkiri(),
            ),
            const SizedBox(
              width: 20,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.47,
                height: MediaQuery.of(context).size.height * 0.76,
                child: kosongkanan()),
          ] else if (global.idCompare1 != 0 && global.idCompare2 == 0) ...[
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKiri(),
              ),
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkanan()),
          ] else if (global.idCompare1 == 0 && global.idCompare2 != 0) ...[
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkiri()),
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKanan(),
              ),
            ),
          ]
        ],
      );
    }
  }

  Widget adaDua() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              GestureDetector(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.42,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white24.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(14),
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                              data3[global.idCompare1 - 1].imageLink))),
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/compare/motherboard/list');
                  setState(() {
                    global.idYangDiCompare = 1;
                  });
                },
              ),
              SizedBox(
                width: 20,
              ),
              GestureDetector(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.42,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white24.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(14),
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                              data3[global.idCompare2 - 1].imageLink))),
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/compare/motherboard/list');
                  setState(() {
                    global.idYangDiCompare = 2;
                  });
                },
              ),
            ]),
            DataTable(
              dataRowHeight: MediaQuery.of(context).size.height * 0.1,
              columnSpacing: MediaQuery.of(context).size.width * 0.05,
              border: TableBorder(
                  horizontalInside:
                      BorderSide(color: Colors.white, width: 2),
                  top: BorderSide.none),
              columns: const <DataColumn>[
                DataColumn(
                  label: Text(
                    '',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                DataColumn(
                  label: Text(""),
                ),
                DataColumn(
                  label: Text(
                    '',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
              ],
              rows: <DataRow>[
                DataRow(
                  cells: <DataCell>[
                    DataCell(
                      SizedBox(
                        width: double.infinity,
                        child: const Text(
                          'Nama Motherboard',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    DataCell(
                      SizedBox(
                        width: 100,
                        child: Text(data3[global.idCompare1 - 1].namaMobo,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ),
                    DataCell(
                      Text(data3[global.idCompare2 - 1].namaMobo,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                            fontSize: 13,
                            fontWeight: FontWeight.normal,
                            height: 1.5,
                            color: const Color(0xffD7D7D7),
                          )),
                    ),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(
                      'Merek Motherboard',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    DataCell(
                      Text(data3[global.idCompare1 - 1].merkMobo,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                    DataCell(
                      Text(data3[global.idCompare2 - 1].merkMobo,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(
                      'Harga Motherboard',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    DataCell(
                      Text(
                          "Rp ${formatter
                                  .format(int.parse(
                                      data3[global.idCompare1 - 1]
                                          .harga
                                          .toString()))}",
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                    DataCell(
                      Text(
                          "Rp ${formatter
                                  .format(int.parse(
                                      data3[global.idCompare2 - 1]
                                          .harga
                                          .toString()))}",
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(
                      'Motherboard Socket',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    DataCell(
                      Text(data3[global.idCompare1 - 1].socketMobo,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                    DataCell(
                      Text(data3[global.idCompare2 - 1].socketMobo,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(
                      'Chipset',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    DataCell(
                      Text(data3[global.idCompare1 - 1].chipsetMobo,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                    DataCell(
                      Text(data3[global.idCompare2 - 1].chipsetMobo,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(
                      'Form Factor',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    DataCell(
                      Text(data3[global.idCompare1 - 1].formFactor,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                    DataCell(
                      Text(data3[global.idCompare2 - 1].formFactor,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(
                      Text(
                        'Memory Slot',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    if (int.parse(data3[global.idCompare1 - 1]
                            .slotMemory
                            .substring(0, 1)) >
                        int.parse(data3[global.idCompare2 - 1]
                            .slotMemory
                            .substring(0, 1))) ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare1 - 1].slotMemory,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].slotMemory,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ] else if (int.parse(data3[global.idCompare1 - 1]
                            .slotMemory
                            .substring(0, 1)) <
                        int.parse(data3[global.idCompare2 - 1]
                            .slotMemory
                            .substring(0, 1))) ...[
                      DataCell(
                        Text(data3[global.idCompare1 - 1].slotMemory,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare2 - 1].slotMemory,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ] else ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare1 - 1].slotMemory,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare2 - 1].slotMemory,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ]
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(
                      Text(
                        'Supported Memory',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    if (int.parse(data3[global.idCompare1 - 1]
                            .memoryType
                            .substring(5, 7)) >
                        int.parse(data3[global.idCompare2 - 1]
                            .memoryType
                            .substring(5, 7))) ...[
                      DataCell(
                        Row(
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: SizedBox(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(
                              data3[global.idCompare2 - 1].memoryType,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                    ] else if (int.parse(data3[global.idCompare1 - 1]
                            .memoryType
                            .substring(5, 7)) <
                        int.parse(data3[global.idCompare2 - 1]
                            .memoryType
                            .substring(5, 7))) ...[
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(
                              data3[global.idCompare1 - 1].memoryType,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: SizedBox(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ] else ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: SingleChildScrollView(
                                child: Text(
                                    data3[global.idCompare1 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: SingleChildScrollView(
                                child: Text(
                                    data3[global.idCompare2 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ]
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(
                      Text(
                        'PCI-E',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    if (int.parse(data3[global.idCompare1 - 1]
                            .pcIgen
                            .substring(4, 5)) >
                        int.parse(data3[global.idCompare2 - 1]
                            .pcIgen
                            .substring(4, 5))) ...[
                      DataCell(
                        Row(
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: SizedBox(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].pcie}/ ${data3[global.idCompare1 - 1].pcIgen}",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(
                              "${data3[global.idCompare2 - 1].pcie}/ ${data3[global.idCompare2 - 1].pcIgen}",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                    ] else if (int.parse(data3[global.idCompare1 - 1]
                            .pcIgen
                            .substring(4, 5)) <
                        int.parse(data3[global.idCompare2 - 1]
                            .pcIgen
                            .substring(4, 5))) ...[
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(
                              "${data3[global.idCompare1 - 1].pcie}/ ${data3[global.idCompare1 - 1].pcIgen}",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: SizedBox(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].pcie}/ ${data3[global.idCompare2 - 1].pcIgen}",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ] else ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: SingleChildScrollView(
                                child: Text(
                                    "${data3[global.idCompare1 - 1].pcie}/ ${data3[global.idCompare1 - 1].pcIgen}",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: SingleChildScrollView(
                                child: Text(
                                    "${data3[global.idCompare2 - 1].pcie}/ ${data3[global.idCompare2 - 1].pcIgen}",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ]
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(
                      Text(
                        'M.2 Slot',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    if (data3[global.idCompare2 - 1].m2Slot == "No" &&
                        data3[global.idCompare1 - 1].m2Slot != "No") ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare1 - 1].m2Slot,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].m2Slot,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ] else if (data3[global.idCompare1 - 1].m2Slot ==
                            "No" &&
                        data3[global.idCompare2 - 1].m2Slot != "No") ...[
                      DataCell(
                        Text(data3[global.idCompare1 - 1].m2Slot,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare2 - 1].m2Slot,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ] else ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare1 - 1].m2Slot,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare2 - 1].m2Slot,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ]
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(
                      Text(
                        'Sata Slot',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    if (int.parse(data3[global.idCompare1 - 1]
                            .sataSlot
                            .substring(0, 1)) >
                        int.parse(data3[global.idCompare2 - 1]
                            .sataSlot
                            .substring(0, 1))) ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare1 - 1].sataSlot,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].sataSlot,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ] else if (int.parse(data3[global.idCompare1 - 1]
                            .sataSlot
                            .substring(0, 1)) <
                        int.parse(data3[global.idCompare2 - 1]
                            .sataSlot
                            .substring(0, 1))) ...[
                      DataCell(
                        Text(data3[global.idCompare1 - 1].sataSlot,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare2 - 1].sataSlot,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ] else ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare1 - 1].sataSlot,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                  data3[global.idCompare2 - 1].sataSlot,
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7))),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ]
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(
                      Text(
                        'USB Port',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    if (int.parse(data3[global.idCompare1 - 1]
                            .usbPort
                            .substring(0, 1)) >
                        int.parse(data3[global.idCompare2 - 1]
                            .usbPort
                            .substring(0, 1))) ...[
                      DataCell(
                        Row(
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: SizedBox(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].usbPort,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(data3[global.idCompare2 - 1].usbPort,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                    ] else if (int.parse(data3[global.idCompare1 - 1]
                            .usbPort
                            .substring(0, 1)) <
                        int.parse(data3[global.idCompare2 - 1]
                            .usbPort
                            .substring(0, 1))) ...[
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(data3[global.idCompare1 - 1].usbPort,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: SizedBox(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].usbPort,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/Centang.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ] else ...[
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: SingleChildScrollView(
                                child: Text(
                                    data3[global.idCompare1 - 1].usbPort,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                      DataCell(
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: SingleChildScrollView(
                                child: Text(
                                    data3[global.idCompare2 - 1].usbPort,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                            ),
                            Image.asset(
                              'assets/img/CentangKuning.png',
                              width: 25,
                              height: 25,
                            )
                          ],
                        ),
                      ),
                    ]
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(
                      'Audio Port',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    DataCell(
                      Text(data3[global.idCompare1 - 1].audioPort,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                    DataCell(
                      Text(data3[global.idCompare2 - 1].audioPort,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7))),
                    ),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(
                      'Display Output',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    DataCell(
                      SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Text(
                            data3[global.idCompare1 - 1].displayOutput,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ),
                    DataCell(
                      SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Text(
                            data3[global.idCompare2 - 1].displayOutput,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text(
                      'LAN Port',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                    DataCell(
                      Text(data3[global.idCompare1 - 1].lanPort,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              color: const Color(0xffD7D7D7))),
                    ),
                    DataCell(
                      Text(data3[global.idCompare2 - 1].lanPort,
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              color: const Color(0xffD7D7D7))),
                    ),
                  ],
                ),
              ],
            ),
          ],
        );
  }

  Widget kosongkiri() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(left: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah Mobo",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/motherboard/list');
        setState(() {
          global.idYangDiCompare = 1;
        });
      },
    );
  }

  Widget kosongkanan() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(right: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah Mobo",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/motherboard/list');
        setState(() {
          global.idYangDiCompare = 2;
        });
      },
    );
  }

  Widget keisiKiriCompare() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/motherboard/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Motherboard",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merk Motherboard",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Motherboard Socket",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].socketMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Chipset",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].chipsetMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare1 - 1]
                        .slotMemory
                        .substring(0, 1)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .slotMemory
                        .substring(0, 1))) ...[
                  Text("Memory Slot ",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].slotMemory}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1]
                        .slotMemory
                        .substring(0, 1)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .slotMemory
                        .substring(0, 1))) ...[
                  Text("Memory Slot ",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].slotMemory}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Memory Slot ",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].slotMemory}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(5, 7)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(5, 7))) ...[
                  Text("Supported Memory",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(5, 7)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(5, 7))) ...[
                  Text("Supported Memory",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Supported Memory",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (int.parse(
                        data3[global.idCompare1 - 1].pcIgen.substring(4, 5)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .pcIgen
                        .substring(4, 5))) ...[
                  Text("PCI-E",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare1 - 1].pcie}/ ${data3[global.idCompare1 - 1].pcIgen}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].pcIgen.substring(4, 5)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .pcIgen
                        .substring(4, 5))) ...[
                  Text("PCI-E",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare1 - 1].pcie}/ ${data3[global.idCompare1 - 1].pcIgen}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("PCI-E",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare1 - 1].pcie}/ ${data3[global.idCompare1 - 1].pcIgen}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (data3[global.idCompare2 - 1].m2Slot == "No" &&
                    data3[global.idCompare1 - 1].m2Slot != "No") ...[
                  Text("M.2 Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].m2Slot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare1 - 1].m2Slot == "No" &&
                    data3[global.idCompare2 - 1].m2Slot != "No") ...[
                  Text("M.2 Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].m2Slot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("M.2 Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].m2Slot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (int.parse(
                        data3[global.idCompare1 - 1].sataSlot.substring(0, 1)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .sataSlot
                        .substring(0, 1))) ...[
                  Text("Sata Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].sataSlot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].sataSlot.substring(0, 1)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .sataSlot
                        .substring(0, 1))) ...[
                  Text("Sata Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].sataSlot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Sata Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].sataSlot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (int.parse(
                        data3[global.idCompare1 - 1].usbPort.substring(0, 1)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .usbPort
                        .substring(0, 1))) ...[
                  Text("USB Port",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare1 - 1].usbPort}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffD7D7D7))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].usbPort.substring(0, 1)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .usbPort
                        .substring(0, 1))) ...[
                  Text("USB Port",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].usbPort}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("USB Port",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare1 - 1].usbPort}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                Text("Audio Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].audioPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Display Output",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].displayOutput}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("LAN Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].lanPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKananCompare() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/motherboard/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Motherboard",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merk Motherboard",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Motherboard Socket",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].socketMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Chipset",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].chipsetMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare1 - 1]
                        .slotMemory
                        .substring(0, 1)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .slotMemory
                        .substring(0, 1))) ...[
                  Text("Memory Slot ",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].slotMemory}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1]
                        .slotMemory
                        .substring(0, 1)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .slotMemory
                        .substring(0, 1))) ...[
                  Text("Memory Slot ",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].slotMemory}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Memory Slot ",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].slotMemory}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(5, 7)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(5, 7))) ...[
                  Text("Supported Memory",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(5, 7)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(5, 7))) ...[
                  Text("Supported Memory",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Supported Memory",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (int.parse(
                        data3[global.idCompare1 - 1].pcIgen.substring(4, 5)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .pcIgen
                        .substring(4, 5))) ...[
                  Text("PCI-E",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare2 - 1].pcie}/ ${data3[global.idCompare2 - 1].pcIgen}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].pcIgen.substring(4, 5)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .pcIgen
                        .substring(4, 5))) ...[
                  Text("PCI-E",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare2 - 1].pcie}/ ${data3[global.idCompare2 - 1].pcIgen}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("PCI-E",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare2 - 1].pcie}/ ${data3[global.idCompare2 - 1].pcIgen}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (data3[global.idCompare1 - 1].m2Slot == "No" &&
                    data3[global.idCompare2 - 1].m2Slot != "No") ...[
                  Text("M.2 Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].m2Slot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare2 - 1].m2Slot == "No" &&
                    data3[global.idCompare1 - 1].m2Slot != "No") ...[
                  Text("M.2 Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].m2Slot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("M.2 Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].m2Slot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (int.parse(
                        data3[global.idCompare1 - 1].sataSlot.substring(0, 1)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .sataSlot
                        .substring(0, 1))) ...[
                  Text("Sata Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].sataSlot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].sataSlot.substring(0, 1)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .sataSlot
                        .substring(0, 1))) ...[
                  Text("Sata Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].sataSlot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Sata Slot",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].sataSlot}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (int.parse(
                        data3[global.idCompare1 - 1].usbPort.substring(0, 1)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .usbPort
                        .substring(0, 1))) ...[
                  Text("USB Port",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].usbPort}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].usbPort.substring(0, 1)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .usbPort
                        .substring(0, 1))) ...[
                  Text("USB Port",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].usbPort}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("USB Port",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].usbPort}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                Text("Audio Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].audioPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Display Output",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].displayOutput}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("LAN Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].lanPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKiri() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/motherboard/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Motherboard",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merk Motherboard",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Motherboard Socket",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].socketMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Chipset",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].chipsetMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Memory Slot ",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].slotMemory}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Supported Memory",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].memoryType}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("PCI-E",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "${data3[global.idCompare1 - 1].pcie}/${data3[global.idCompare1 - 1].pcIgen}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("M.2 Slot",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].m2Slot}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Sata Slot",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].sataSlot}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("USB Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].usbPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Audio Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].audioPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Display Output",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].displayOutput}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("LAN Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].lanPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKanan() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/motherboard/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Motherboard",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merk Motherboard",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Motherboard Socket",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].socketMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Chipset",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].chipsetMobo}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Memory Slot ",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].slotMemory}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Supported Memory",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].memoryType}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("PCI-E",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "${data3[global.idCompare2 - 1].pcie}/${data3[global.idCompare2 - 1].pcIgen}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("M.2 Slot",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].m2Slot}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Sata Slot",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].sataSlot}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("USB Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].usbPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Audio Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].audioPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Display Output",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].displayOutput}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("LAN Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].lanPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }
}
