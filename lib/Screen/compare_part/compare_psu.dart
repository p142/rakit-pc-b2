// ignore_for_file: avoid_unnecessary_containers, prefer_const_constructors, sized_box_for_whitespace, unnecessary_const, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:intl/intl.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/Models/models_psu.dart';
import '../../Api/api_psu.dart';
import '../home_page/floating_button.dart';

class ComparePsu extends StatefulWidget {
  ComparePsu({Key? key}) : super(key: key);
  @override
  State<ComparePsu> createState() => _ComparePsuState();
}

class _ComparePsuState extends State<ComparePsu> {
  var formatter = NumberFormat('#,###,000');
  late Future data;
  List<Psu> data3 = [];

  @override
  void initState() {
    data = fetchPsu();
    data.then((value) => setState(() {
          data3 = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,

        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/compare/list');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        //backgroundColor: Color.fromARGB(240, 143, 5, 131),
        title: Text(
          "Compare Power Supply",
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: result(),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget result() {
    if (global.idCompare1 != 0 && global.idCompare2 != 0) {
      return ListView(
        children: [
          SizedBox(
            child: adaDua(),
          ),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          if (global.idCompare1 == 0 && global.idCompare2 == 0) ...[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.47,
              height: MediaQuery.of(context).size.height * 0.76,
              child: kosongkiri(),
            ),
            const SizedBox(
              width: 20,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.47,
                height: MediaQuery.of(context).size.height * 0.76,
                child: kosongkanan()),
          ] else if (global.idCompare1 != 0 && global.idCompare2 == 0) ...[
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKiri(),
              ),
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkanan()),
          ] else if (global.idCompare1 == 0 && global.idCompare2 != 0) ...[
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkiri()),
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKanan(),
              ),
            ),
          ]
        ],
      );
    }
  }

  Widget adaDua() {
    return data3.isEmpty
        ? const Center(
            child: const CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  GestureDetector(
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.2,
                      width: MediaQuery.of(context).size.width * 0.42,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white24.withOpacity(0.2),
                          borderRadius: BorderRadius.circular(14),
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(
                                  data3[global.idCompare1 - 1].imageLink))),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/compare/psu/list');
                      setState(() {
                        global.idYangDiCompare = 1;
                      });
                    },
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.2,
                      width: MediaQuery.of(context).size.width * 0.42,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white24.withOpacity(0.2),
                          borderRadius: BorderRadius.circular(14),
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(
                                  data3[global.idCompare2 - 1].imageLink))),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/compare/psu/list');
                      setState(() {
                        global.idYangDiCompare = 2;
                      });
                    },
                  ),
                ]),
                DataTable(
                  dataRowHeight: MediaQuery.of(context).size.height * 0.1,
                  columnSpacing: MediaQuery.of(context).size.width * 0.04,
                  border: TableBorder(
                      horizontalInside:
                          BorderSide(color: Colors.white, width: 2),
                      top: BorderSide.none),
                  columns: const <DataColumn>[
                    DataColumn(
                      label: Text(
                        '',
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                    DataColumn(
                      label: Text(""),
                    ),
                    DataColumn(
                      label: Text(
                        '',
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    ),
                  ],
                  rows: <DataRow>[
                    DataRow(
                      cells: <DataCell>[
                        DataCell(
                          Container(
                            width: double.infinity,
                            child: const Text(
                              'Nama PSU',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        DataCell(
                          Container(
                            width: 100,
                            child: Text(data3[global.idCompare1 - 1].namaPsu,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ),
                        DataCell(
                          Container(
                            // width: 100,
                            child: Text(data3[global.idCompare2 - 1].namaPsu,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7),
                                )),
                          ),
                        ),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(
                          'Merek PSU',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                        DataCell(
                          Text(data3[global.idCompare1 - 1].merkPsu,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].merkPsu,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(
                          'Harga PSU',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                        DataCell(
                          Text(
                              "Rp ${formatter
                                      .format(int.parse(
                                          data3[global.idCompare1 - 1]
                                              .harga
                                              .toString()))}",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(
                              "Rp ${formatter
                                      .format(int.parse(
                                          data3[global.idCompare2 - 1]
                                              .harga
                                              .toString()))}",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(
                          Text(
                            'Watt PSU',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        if (int.parse(data3[global.idCompare1 - 1].wattPsu) >
                            int.parse(
                                data3[global.idCompare2 - 1].wattPsu)) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      "${data3[global.idCompare1 - 1].wattPsu} Watt",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text("${data3[global.idCompare2 - 1].wattPsu} Watt",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (int.parse(
                                data3[global.idCompare1 - 1].wattPsu) <
                            int.parse(
                                data3[global.idCompare2 - 1].wattPsu)) ...[
                          DataCell(
                            Text("${data3[global.idCompare1 - 1].wattPsu} Watt",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      "${data3[global.idCompare2 - 1].wattPsu} Watt",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      "${data3[global.idCompare1 - 1].wattPsu} Watt",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      "${data3[global.idCompare2 - 1].wattPsu} Watt",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(
                          Text(
                            'Certification',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        if (((data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Gol") &&
                                (data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Whi" ||
                                    data3[global.idCompare2 - 1]
                                            .the80PlusEfficient
                                            .substring(8, 11) ==
                                        "Bro")) ||
                            ((data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Bro") &&
                                (data3[global.idCompare2 - 1]
                                        .the80PlusEfficient
                                        .substring(8, 11) ==
                                    "Whi"))) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare1 - 1]
                                          .the80PlusEfficient,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(
                                data3[global.idCompare2 - 1].the80PlusEfficient,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (((data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Gol") &&
                                (data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Whi" ||
                                    data3[global.idCompare1 - 1]
                                            .the80PlusEfficient
                                            .substring(8, 11) ==
                                        "Bro")) ||
                            ((data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Bro") &&
                                (data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Whi"))) ...[
                          DataCell(
                            Text(
                                data3[global.idCompare1 - 1].the80PlusEfficient,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare2 - 1]
                                          .the80PlusEfficient,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1]
                                          .the80PlusEfficient,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1]
                                          .the80PlusEfficient,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(
                          Text(
                            'Modular',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        if (data3[global.idCompare1 - 1].modular == "Modular" &&
                            data3[global.idCompare2 - 1].modular == "No") ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1].modular,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(data3[global.idCompare2 - 1].modular,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (data3[global.idCompare2 - 1].modular ==
                                "Modular" &&
                            data3[global.idCompare1 - 1].modular == "No") ...[
                          DataCell(
                            Text(data3[global.idCompare1 - 1].modular,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1].modular,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1].modular,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1].modular,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(
                          Text(
                            'Silent Mode',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        if (data3[global.idCompare1 - 1].silentMode != "No" &&
                            data3[global.idCompare2 - 1].silentMode ==
                                "No") ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1].silentMode,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(data3[global.idCompare2 - 1].silentMode,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (data3[global.idCompare2 - 1].silentMode !=
                                "No" &&
                            data3[global.idCompare1 - 1].silentMode ==
                                "No") ...[
                          DataCell(
                            Text(data3[global.idCompare1 - 1].silentMode,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1].silentMode,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1].silentMode,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1].silentMode,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(
                          Text(
                            'ATX Connector',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        if (int.parse(
                                data3[global.idCompare1 - 1].atxConnector) >
                            int.parse(
                                data3[global.idCompare2 - 1].atxConnector)) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      "${data3[global.idCompare1 - 1]
                                              .atxConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(
                                "${data3[global.idCompare2 - 1].atxConnector}x",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (int.parse(
                                data3[global.idCompare1 - 1].atxConnector) <
                            int.parse(
                                data3[global.idCompare2 - 1].atxConnector)) ...[
                          DataCell(
                            Text(
                                "${data3[global.idCompare1 - 1].atxConnector}x",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      "${data3[global.idCompare2 - 1]
                                              .atxConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      "${data3[global.idCompare1 - 1]
                                              .atxConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      "${data3[global.idCompare2 - 1]
                                              .atxConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(
                          Text(
                            'PCI-E Connector',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        if (int.parse(
                                data3[global.idCompare1 - 1].pcieConnector) >
                            int.parse(data3[global.idCompare2 - 1]
                                .pcieConnector)) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      "${data3[global.idCompare1 - 1]
                                              .pcieConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(
                                "${data3[global.idCompare2 - 1].pcieConnector}x",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (int.parse(
                                data3[global.idCompare1 - 1].pcieConnector) <
                            int.parse(data3[global.idCompare2 - 1]
                                .pcieConnector)) ...[
                          DataCell(
                            Text(
                                "${data3[global.idCompare1 - 1].pcieConnector}x",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      "${data3[global.idCompare2 - 1]
                                              .pcieConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      "${data3[global.idCompare1 - 1].pcieConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                            DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      "${data3[global.idCompare2 - 1].pcieConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(
                          Text(
                            'Sata Connector',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        if (int.parse(
                                data3[global.idCompare1 - 1].sataConnector) >
                            int.parse(data3[global.idCompare2 - 1]
                                .sataConnector)) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      "${data3[global.idCompare1 - 1]
                                              .sataConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(
                                "${data3[global.idCompare2 - 1].sataConnector}x",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (int.parse(
                                data3[global.idCompare1 - 1].sataConnector) <
                            int.parse(data3[global.idCompare2 - 1]
                                .sataConnector)) ...[
                          DataCell(
                            Text(
                                "${data3[global.idCompare1 - 1].sataConnector}x",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      "${data3[global.idCompare2 - 1]
                                              .sataConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      "${data3[global.idCompare1 - 1].sataConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                            DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      "${data3[global.idCompare2 - 1].sataConnector}x",
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(
                          'Form Factor',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                        DataCell(
                          Text(data3[global.idCompare1 - 1].formFactor,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].formFactor,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(
                          'RGB',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                        DataCell(
                          Text(data3[global.idCompare1 - 1].rgb,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].rgb,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ],
                    ),
                    DataRow(
                      cells: <DataCell>[
                        DataCell(Text(
                          'Fan Size',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                        DataCell(
                          Text(data3[global.idCompare1 - 1].fanSize,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].fanSize,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          );
  }

  Widget kosongkiri() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(const Radius.circular(10))),
          margin: const EdgeInsets.only(left: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah PSU",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/psu/list');
        setState(() {
          global.idYangDiCompare = 1;
        });
      },
    );
  }

  Widget kosongkanan() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(const Radius.circular(10))),
          margin: const EdgeInsets.only(right: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah PSU",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/psu/list');
        setState(() {
          global.idYangDiCompare = 2;
        });
      },
    );
  }

  Widget keisiKiriCompare() {
    return data3.isEmpty
        ? const Center(
            child: const CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/psu/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaPsu}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkPsu}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare1 - 1].wattPsu) >
                    int.parse(data3[global.idCompare2 - 1].wattPsu)) ...[
                  Text("Watt PSU",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].wattPsu} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].wattPsu) <
                    int.parse(data3[global.idCompare2 - 1].wattPsu)) ...[
                  Text("Watt PSU",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].wattPsu} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Watt PSU",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].wattPsu} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (((data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Gol") &&
                        (data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Whi" ||
                            data3[global.idCompare2 - 1]
                                    .the80PlusEfficient
                                    .substring(8, 11) ==
                                "Bro")) ||
                    ((data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Bro") &&
                        (data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) ==
                            "Whi"))) ...[
                  Text("Certification",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].the80PlusEfficient}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (((data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Gol") &&
                        (data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Whi" ||
                            data3[global.idCompare1 - 1]
                                    .the80PlusEfficient
                                    .substring(8, 11) ==
                                "Bro")) ||
                    ((data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Bro") &&
                        (data3[global.idCompare1 - 1]
                                .the80PlusEfficient
                                .substring(8, 11) ==
                            "Whi"))) ...[
                  Text("Certification",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].the80PlusEfficient}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Certification",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].the80PlusEfficient}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (data3[global.idCompare1 - 1].modular.substring(0, 2) !=
                        "No" &&
                    data3[global.idCompare2 - 1].modular.substring(0, 2) ==
                        "No") ...[
                  Text("Modular",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].modular}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare2 - 1]
                            .modular
                            .substring(0, 2) !=
                        "No" &&
                    data3[global.idCompare1 - 1].modular.substring(0, 2) ==
                        "No") ...[
                  Text("Modular",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].modular}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Modular",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].modular}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (data3[global.idCompare1 - 1].silentMode != "No" &&
                    data3[global.idCompare2 - 1].silentMode == "No") ...[
                  Text("Silent Mode",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].silentMode}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare2 - 1].silentMode != "No" &&
                    data3[global.idCompare1 - 1].silentMode == "No") ...[
                  Text("Silent Mode",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].silentMode}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ] else ...{
                  Text("Silent Mode",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].silentMode}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                Text("ATX Connector",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffFFB200))),
                Text("${data3[global.idCompare1 - 1].atxConnector}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffFFB200))),
                if (int.parse(data3[global.idCompare1 - 1].pcieConnector) >
                    int.parse(data3[global.idCompare2 - 1].pcieConnector)) ...[
                  Text("PCI-E Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].pcieConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].pcieConnector) <
                    int.parse(data3[global.idCompare2 - 1].pcieConnector)) ...[
                  Text("PCI-E Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].pcieConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("PCI-E Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].pcieConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].sataConnector) >
                    int.parse(data3[global.idCompare2 - 1].sataConnector)) ...[
                  Text("Sata Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].sataConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].sataConnector) <
                    int.parse(data3[global.idCompare2 - 1].sataConnector)) ...[
                  Text("Sata Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].sataConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Sata Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].sataConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("RGB ",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].rgb}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Fan Size",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].fanSize} mm\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKananCompare() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/psu/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaPsu}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkPsu}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare2 - 1].wattPsu) >
                    int.parse(data3[global.idCompare1 - 1].wattPsu)) ...[
                  Text("Watt PSU",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].wattPsu} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].wattPsu) <
                    int.parse(data3[global.idCompare1 - 1].wattPsu)) ...[
                  Text("Watt PSU",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].wattPsu} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Watt PSU",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].wattPsu} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (((data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Gol") &&
                        (data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Whi" ||
                            data3[global.idCompare1 - 1]
                                    .the80PlusEfficient
                                    .substring(8, 11) ==
                                "Bro")) ||
                    ((data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Bro") &&
                        (data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) ==
                            "Whi"))) ...[
                  Text("Certification",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].the80PlusEfficient}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (((data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Gol") &&
                        (data3[global.idCompare2 - 1].the80PlusEfficient.substring(8, 11) == "Whi" ||
                            data3[global.idCompare2 - 1]
                                    .the80PlusEfficient
                                    .substring(8, 11) ==
                                "Bro")) ||
                    ((data3[global.idCompare1 - 1].the80PlusEfficient.substring(8, 11) == "Bro") &&
                        (data3[global.idCompare2 - 1]
                                .the80PlusEfficient
                                .substring(8, 11) ==
                            "Whi"))) ...[
                  Text("Certification",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].the80PlusEfficient}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Certification",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare2 - 1]
                              .the80PlusEfficient
                              .substring(8, 11)}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (data3[global.idCompare2 - 1].modular.substring(0, 2) !=
                        "No" &&
                    data3[global.idCompare1 - 1].modular.substring(0, 2) ==
                        "No") ...[
                  Text("Modular",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].modular}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare1 - 1]
                            .modular
                            .substring(0, 2) !=
                        "No" &&
                    data3[global.idCompare2 - 1].modular.substring(0, 2) ==
                        "No") ...[
                  Text("Modular",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].modular}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Modular",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].modular}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (data3[global.idCompare2 - 1].silentMode != "No" &&
                    data3[global.idCompare1 - 1].silentMode == "No") ...[
                  Text("Silent Mode",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].silentMode}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare1 - 1].silentMode != "No" &&
                    data3[global.idCompare2 - 1].silentMode == "No") ...[
                  Text("Silent Mode",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].silentMode}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Silent Mode",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].silentMode}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                Text("ATX Connector",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffFFB200))),
                Text("${data3[global.idCompare2 - 1].atxConnector}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffFFB200))),
                if (int.parse(data3[global.idCompare2 - 1].pcieConnector) >
                    int.parse(data3[global.idCompare1 - 1].pcieConnector)) ...[
                  Text("PCI-E Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].pcieConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare2 - 1].pcieConnector) <
                    int.parse(data3[global.idCompare1 - 1].pcieConnector)) ...[
                  Text("PCI-E Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].pcieConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("PCI-E Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].pcieConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare2 - 1].sataConnector) >
                    int.parse(data3[global.idCompare1 - 1].sataConnector)) ...[
                  Text("Sata Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].sataConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare2 - 1].sataConnector) <
                    int.parse(data3[global.idCompare1 - 1].sataConnector)) ...[
                  Text("Sata Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].sataConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...{
                  Text("Sata Connector",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].sataConnector}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                },
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("RGB ",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].rgb}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Fan Size",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].fanSize} mm\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKanan() {
    return data3.isEmpty
        ? const Center(
            child: const CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/psu/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaPsu}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkPsu}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Watt PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].wattPsu} Watt\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Certification",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].the80PlusEfficient}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Modular",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].modular}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Silent Mode",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].silentMode}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("ATX Connector",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].atxConnector}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("PCI-E Connector",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].pcieConnector}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Sata Connector",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].sataConnector}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("RGB ",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].rgb}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Fan Size",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].fanSize} mm\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKiri() {
    return data3.isEmpty
        ? const Center(
            child: const CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/psu/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaPsu}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkPsu}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Watt PSU",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].wattPsu} Watt\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Certification",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].the80PlusEfficient}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Modular",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].modular}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Silent Mode",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].silentMode}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("ATX Connector",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].atxConnector}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("PCI-E Connector",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].pcieConnector}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Sata Connector",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].sataConnector}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Form Factor",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].formFactor}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("RGB ",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].rgb}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Fan Size",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].fanSize} mm\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }
}
