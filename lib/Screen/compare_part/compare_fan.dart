// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import '../../Models/models_fan.dart';
import '../../Api/api_fan.dart';
import 'package:intl/intl.dart';
import 'package:rakit_pc/global.dart' as global;

import '../home_page/floating_button.dart';

class CompareFan extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  CompareFan({Key? key}) : super(key: key);
  @override
  State<CompareFan> createState() => _CompareFanState();
}

class _CompareFanState extends State<CompareFan> {
  var formatter = NumberFormat('#,###,000');
  late Future data;
  List<Fan> data3 = [];

  @override
  void initState() {
    data = fetchFan();
    data.then((value) => setState(() {
          data3 = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,

        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/compare/list');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        //backgroundColor: Color.fromARGB(240, 143, 5, 131),
        title: Text(
          "Compare Fan",
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: result(),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget result() {
    if (global.idCompare1 != 0 && global.idCompare2 != 0) {
      return ListView(
        children: [
          SizedBox(
            child: adaDua(),
          ),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          if (global.idCompare1 == 0 && global.idCompare2 == 0) ...[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.47,
              height: MediaQuery.of(context).size.height * 0.76,
              child: kosongkiri(),
            ),
            const SizedBox(
              width: 20,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.47,
                height: MediaQuery.of(context).size.height * 0.76,
                child: kosongkanan()),
          ] else if (global.idCompare1 != 0 && global.idCompare2 == 0) ...[
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKiri(),
              ),
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkanan()),
          ] else if (global.idCompare1 == 0 && global.idCompare2 != 0) ...[
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkiri()),
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKanan(),
              ),
            ),
          ]
        ],
      );
    }
  }

  Widget adaDua() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare1 - 1].imageLinks))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/fan/list');
                    setState(() {
                      global.idYangDiCompare = 1;
                    });
                  },
                ),
                SizedBox(
                  width: 20,
                ),
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare2 - 1].imageLinks))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/fan/list');
                    setState(() {
                      global.idYangDiCompare = 2;
                    });
                  },
                ),
              ]),
              DataTable(
                dataRowHeight: MediaQuery.of(context).size.height * 0.1,
                columnSpacing: MediaQuery.of(context).size.width * 0.05,
                border: TableBorder(
                    horizontalInside: BorderSide(color: Colors.white, width: 2),
                    top: BorderSide.none),
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                  DataColumn(
                    label: Text(""),
                  ),
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
                rows: <DataRow>[
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        const Text(
                          'Nama Fan',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: SizedBox(
                            width: 80,
                            child: Text(data3[global.idCompare1 - 1].namaFans,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: SizedBox(
                            width: 80,
                            child: Text(data3[global.idCompare2 - 1].namaFans,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Merek Fan',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].merkFans,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].merkFans,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Harga Fan',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare1 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare2 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Size Fan',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare2 - 1].sizeFans) >
                          int.parse(data3[global.idCompare1 - 1].sizeFans)) ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].sizeFans,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].sizeFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare2 - 1].sizeFans) <
                          int.parse(data3[global.idCompare1 - 1].sizeFans)) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].sizeFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 20,
                                height: 20,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].sizeFans,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].sizeFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].sizeFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Speed Fan',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      if (int.parse(data3[global.idCompare2 - 1]
                              .speedFans
                              .substring(0, 2)) >
                          int.parse(data3[global.idCompare1 - 1]
                              .speedFans
                              .substring(0, 2))) ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].speedFans,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].speedFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 20,
                                height: 20,
                              )
                            ],
                          ),
                        ),
                      ] else if (int.parse(data3[global.idCompare2 - 1]
                              .speedFans
                              .substring(0, 2)) <
                          int.parse(data3[global.idCompare1 - 1]
                              .speedFans
                              .substring(0, 2))) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].speedFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 20,
                                height: 20,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].speedFans,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].speedFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].speedFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                      //
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Power Usage Fan',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      if (int.parse(data3[global.idCompare2 - 1].powerFans) <
                          int.parse(
                              data3[global.idCompare1 - 1].powerFans)) ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].powerFans,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].powerFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 20,
                                height: 20,
                              )
                            ],
                          ),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare2 - 1].powerFans) >
                          int.parse(
                              data3[global.idCompare1 - 1].powerFans)) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].powerFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 20,
                                height: 20,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].powerFans,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].powerFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].powerFans,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Power Connector',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].powerConnector,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].powerConnector,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Voltage Fan',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].voltageFans,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].voltageFans,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Color Fan',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].colorFans,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].colorFans,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
  }

  Widget kosongkiri() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(left: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah Fan",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/fan/list');
        setState(() {
          global.idYangDiCompare = 1;
        });
      },
    );
  }

  Widget kosongkanan() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(right: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah fan",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/fan/list');
        setState(() {
          global.idYangDiCompare = 2;
        });
      },
    );
  }

  Widget keisiKanan() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/fan/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLinks,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Size Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].sizeFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Speed Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].speedFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Power Connector",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].powerConnector}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Power Usage Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].powerFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Voltage Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].voltageFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Color Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].colorFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKiri() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/fan/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLinks,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Size Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].sizeFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Speed Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].speedFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Power Connector",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].powerConnector}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Power Usage Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].powerFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Voltage Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].voltageFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Color Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].colorFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKananCompare() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/fan/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLinks,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare2 - 1].sizeFans) >
                    int.parse(data3[global.idCompare1 - 1].sizeFans)) ...[
                  Text("Size Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].sizeFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].sizeFans) <
                    int.parse(data3[global.idCompare1 - 1].sizeFans)) ...[
                  Text("Size Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].sizeFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Size Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].sizeFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare2 - 1]
                        .speedFans
                        .substring(0, 2)) >
                    int.parse(data3[global.idCompare1 - 1]
                        .speedFans
                        .substring(0, 2))) ...[
                  Text("Speed Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].speedFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1]
                        .speedFans
                        .substring(0, 2)) <
                    int.parse(data3[global.idCompare1 - 1]
                        .speedFans
                        .substring(0, 2))) ...[
                  Text("Speed Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].speedFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Speed Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].speedFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare2 - 1].powerFans) <
                    int.parse(data3[global.idCompare1 - 1].powerFans)) ...[
                  Text("Power Usage Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].powerFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].powerFans) >
                    int.parse(data3[global.idCompare1 - 1].powerFans)) ...[
                  Text("Power Usage Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].powerFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Power Usage Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].powerFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                Text("Power Connector",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].powerConnector}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Voltage Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].voltageFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Color Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].colorFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKiriCompare() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/fan/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLinks,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Merek Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Harga Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare2 - 1].sizeFans) <
                    int.parse(data3[global.idCompare1 - 1].sizeFans)) ...[
                  Text("Size Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].sizeFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].sizeFans) >
                    int.parse(data3[global.idCompare1 - 1].sizeFans)) ...[
                  Text("Size Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].sizeFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Size Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].sizeFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare2 - 1]
                        .speedFans
                        .substring(0, 2)) <
                    int.parse(data3[global.idCompare1 - 1]
                        .speedFans
                        .substring(0, 2))) ...[
                  Text("Speed Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].speedFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1]
                        .speedFans
                        .substring(0, 2)) >
                    int.parse(data3[global.idCompare1 - 1]
                        .speedFans
                        .substring(0, 2))) ...[
                  Text("Speed Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].speedFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Speed Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].speedFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare2 - 1].powerFans) >
                    int.parse(data3[global.idCompare1 - 1].powerFans)) ...[
                  Text("Power Usage Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].powerFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].powerFans) <
                    int.parse(data3[global.idCompare1 - 1].powerFans)) ...[
                  Text("Power Usage Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].powerFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffEB1D36))),
                ] else ...[
                  Text("Power Usage Fan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: const Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].powerFans}\n",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: const Color(0xffFFB200))),
                ],
                Text("Power Connector",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].powerConnector}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Voltage Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].voltageFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
                Text("Color Fan",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: const Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].colorFans}\n",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: const Color(0xffD7D7D7))),
              ],
            ));
  }
}
