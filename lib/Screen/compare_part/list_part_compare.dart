// ignore_for_file: prefer_const_constructors, unused_import

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:rakit_pc/global.dart' as global;

import '../home_page/floating_button.dart';

class ListCompare extends StatefulWidget {
  const ListCompare({Key? key}) : super(key: key);
  @override
  State<ListCompare> createState() => _ListCompareState();
}

class _ListCompareState extends State<ListCompare> {
  @override
  void initState() {
    super.initState();
    global.compareOrList = 1;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,

        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/compare');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        //backgroundColor: Color.fromARGB(240, 143, 5, 131),
        title: Text(
          'List Compare Part',
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: ListView(
        children: [
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Power Supply',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 5),
                            Image.asset(
                              "assets/img/psu.png",
                              width: 33,
                              height: 33,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Pensuplai Kelistrikan Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      setState(() {
                        global.idCompare1 = 0;
                        global.idCompare2 = 0;
                      });
                      Navigator.pushNamed(context, '/compare/psu');
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'CPU',
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                'assets/img/cpu.png',
                                height: 35,
                                width: 35,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Otak Dari Sebuah Komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        setState(() {
                          global.namaPart = "Casing";
                          global.idCompare1 = 0;
                          global.idCompare2 = 0;
                        });
                        Navigator.pushNamed(context, '/compare/cpu');
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'CPU Cooler',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 10),
                            Image.asset(
                              "assets/img/cooler.png",
                              width: 35,
                              height: 35,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Kipas Pendingin\nCPU Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      setState(() {
                        global.idCompare1 = 0;
                        global.idCompare2 = 0;
                      });
                      Navigator.pushNamed(context, '/compare/cpucooler');
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'VGA',
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                'assets/img/vga.png',
                                height: 35,
                                width: 35,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Penerjemah tampilan komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        setState(() {
                          global.idCompare1 = 0;
                          global.idCompare2 = 0;
                        });
                        Navigator.pushNamed(context, '/compare/vga');
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Motherboard',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 5),
                            Image.asset(
                              "assets/img/motherboard.png",
                              width: 35,
                              height: 35,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Dasar Penyambung Part Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      setState(() {
                        global.idCompare1 = 0;
                        global.idCompare2 = 0;
                      });
                      Navigator.pushNamed(context, '/compare/motherboard');
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "RAM",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                "assets/img/ram.png",
                                height: 35,
                                width: 35,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Memori Sementara Komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        setState(() {
                          global.idCompare1 = 0;
                          global.idCompare2 = 0;
                        });
                        Navigator.pushNamed(context, '/compare/ram');
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                margin: EdgeInsets.only(left: 2),
                height: MediaQuery.of(context).size.height * 0.15,
                child: Card(
                  color: Colors.transparent,
                  child: ListTile(
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Fan',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(width: 5),
                            Image.asset(
                              "assets/img/cooling-fan.png",
                              width: 33,
                              height: 33,
                              color: Colors.white70,
                            )
                          ],
                        ),
                        const SizedBox(height: 3),
                        Text(
                          'Kipas Pendingin Sistem Komputer',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            color: Colors.white70,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                    selected: true,
                    selectedTileColor: const Color(0xFF7A77FF),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onTap: () {
                      setState(() {
                        global.idCompare1 = 0;
                        global.idCompare2 = 0;
                      });
                      Navigator.pushNamed(context, '/compare/fan');
                    },
                  ),
                ),
              )),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 2),
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Card(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Storage",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(width: 10),
                              Image.asset(
                                "assets/img/ssd.png",
                                height: 35,
                                width: 35,
                                color: Colors.white70,
                              )
                            ],
                          ),
                          const SizedBox(height: 3),
                          Text(
                            'Penyimpanan Storage Komputer',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                              color: Colors.white70,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      selected: true,
                      selectedTileColor: const Color(0xFF7A77FF),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      onTap: () {
                        setState(() {
                          global.idCompare1 = 0;
                          global.idCompare2 = 0;
                        });
                        Navigator.pushNamed(context, '/compare/storage');
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }
}
