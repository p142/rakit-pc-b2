// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:rakit_pc/Screen/home_page/floating_button.dart';

class HowItWorksCompare extends StatefulWidget {
  const HowItWorksCompare({Key? key}) : super(key: key);

  @override
  State<HowItWorksCompare> createState() => _HowItWorksCompareState();
}

class _HowItWorksCompareState extends State<HowItWorksCompare> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          "How it Works",
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
      ),
      body: Center(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  const SizedBox(
                    height: 10,
                    width: 40,
                  ),
                  Text(
                    "Langkah 1 :",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                      fontSize: 14.7,
                      color: Colors.white,
                      height: 1.5,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    width: MediaQuery.of(context).size.width * 0.61,
                    child: Text(
                      "Pilih jenis komponen komputer yang akan di compare",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                        fontSize: 14.7,
                        color: Colors.white,
                        height: 1.5,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  const SizedBox(
                    height: 10,
                    width: 40,
                  ),
                  Text(
                    "Langkah 2 :",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                      fontSize: 14.7,
                      color: Colors.white,
                      height: 1.5,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Text(
                      "Pilih masing masing kedua part yang akan dicompare",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                        fontSize: 14.7,
                        color: Colors.white,
                        height: 1.5,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 40,
                  ),
                  Text(
                    "Langkah 3 :",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                      fontSize: 14.7,
                      color: Colors.white,
                      height: 1.5,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 40),
                    width: MediaQuery.of(context).size.width * 0.59,
                    child: Text(
                      "Bandingkan kekurangan dan kelebihan masing masing part berdasarkan checklist",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                        fontSize: 14.7,
                        color: Colors.white,
                        height: 1.5,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.04,
              ),
              Center(
                child: Text(
                  "Catatan : ",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                    fontSize: 14.7,
                    color: Colors.white,
                    height: 1.5,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.01,
              ),
              Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.2,
                  ),
                  Image.asset(
                    'assets/img/Centang.png',
                    width: 25,
                    height: 25,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.03,
                  ),
                  Text(
                    "Spesifikasi part lebih Tinggi",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                      fontSize: 14.7,
                      color: Colors.white,
                      height: 1.5,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.2,
                  ),
                  Image.asset(
                    'assets/img/CentangKuning.png',
                    width: 25,
                    height: 25,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.03,
                  ),
                  Text(
                    "Spesifikasi part Sebanding",
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                      fontSize: 14.7,
                      color: Colors.white,
                      height: 1.5,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.06,
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/compare/list');
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    backgroundColor: HexColor("#7A77FF"),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 25, vertical: 10),
                    textStyle: GoogleFonts.poppins(
                      fontSize: 16.0,
                    )),
                child: const Text('Next'),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }
}
