// ignore_for_file: prefer_const_constructors, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:intl/intl.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/Models/models_cpu_cooler.dart';

import '../../Api/api_cpu_cooler.dart';
import '../home_page/floating_button.dart';

class CompareCpuCooler extends StatefulWidget {
  CompareCpuCooler({Key? key}) : super(key: key);
  @override
  State<CompareCpuCooler> createState() => _CompareCpuCoolerState();
}

class _CompareCpuCoolerState extends State<CompareCpuCooler> {
  var formatter = NumberFormat('#,###,000');
  late Future data;
  List<CpuCooler> data3 = [];

  @override
  void initState() {
    data = fetchCpuCooler();
    data.then((value) => setState(() {
          data3 = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,

        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/compare/list');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        //backgroundColor: Color.fromARGB(240, 143, 5, 131),
        title: Text(
          "Compare CPU Cooler",
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: result(),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget result() {
    if (global.idCompare1 != 0 && global.idCompare2 != 0) {
      return ListView(
        children: [
          SizedBox(
            child: adaDua(),
          ),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          if (global.idCompare1 == 0 && global.idCompare2 == 0) ...[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.47,
              height: MediaQuery.of(context).size.height * 0.76,
              child: kosongkiri(),
            ),
            const SizedBox(
              width: 20,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.47,
                height: MediaQuery.of(context).size.height * 0.76,
                child: kosongkanan()),
          ] else if (global.idCompare1 != 0 && global.idCompare2 == 0) ...[
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKiri(),
              ),
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkanan()),
          ] else if (global.idCompare1 == 0 && global.idCompare2 != 0) ...[
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkiri()),
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKanan(),
              ),
            ),
          ]
        ],
      );
    }
  }

  Widget adaDua() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.white,
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare1 - 1].imageLink))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/cpucooler/list');
                    setState(() {
                      global.idYangDiCompare = 1;
                    });
                  },
                ),
                SizedBox(
                  width: 20,
                ),
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare2 - 1].imageLink))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/cpucooler/list');
                    setState(() {
                      global.idYangDiCompare = 2;
                    });
                  },
                ),
              ]),
              DataTable(
                dataRowHeight: MediaQuery.of(context).size.height * 0.1,
                columnSpacing: MediaQuery.of(context).size.width * 0.04,
                border: TableBorder(
                    horizontalInside: BorderSide(color: Colors.white, width: 2),
                    top: BorderSide.none),
                columns: const <DataColumn>[
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                  DataColumn(
                    label: Text(""),
                  ),
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
                rows: <DataRow>[
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        SizedBox(
                          width: double.infinity,
                          child: const Text(
                            'Nama Cooler',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      DataCell(
                        SizedBox(
                          width: 100,
                          child: Text(data3[global.idCompare1 - 1].namaCooler,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                      DataCell(
                        SizedBox(
                          width: 100,
                          child: Text(data3[global.idCompare2 - 1].namaCooler,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7),
                              )),
                        ),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Merek Cooler',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].merkCooler,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].merkCooler,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Harga Cooler',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare1 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare2 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Type CPU Cooler',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (data3[global.idCompare1 - 1].typeCooler ==
                              "Liquid Cooler" &&
                          data3[global.idCompare2 - 1].typeCooler ==
                              "Air Cooler") ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].typeCooler,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].typeCooler,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (data3[global.idCompare2 - 1].typeCooler ==
                              "Liquid Cooler" &&
                          data3[global.idCompare1 - 1].typeCooler ==
                              "Air Cooler") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].typeCooler,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].typeCooler,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].typeCooler,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].typeCooler,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Fan Quantity',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].fanQuantity) >
                          int.parse(
                              data3[global.idCompare2 - 1].fanQuantity)) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].fanQuantity}x",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].fanQuantity,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].fanQuantity) <
                          int.parse(
                              data3[global.idCompare2 - 1].fanQuantity)) ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].fanQuantity,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].fanQuantity,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].fanQuantity,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].fanQuantity,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Fan Speed',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1]
                              .fanSpeed
                              .substring(0, 3)) >
                          int.parse(data3[global.idCompare2 - 1]
                              .fanSpeed
                              .substring(0, 3))) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].fanSpeed,
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].fanSpeed,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(data3[global.idCompare1 - 1]
                              .fanSpeed
                              .substring(0, 3)) <
                          int.parse(data3[global.idCompare2 - 1]
                              .fanSpeed
                              .substring(0, 3))) ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].fanSpeed,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].fanSpeed,
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1].fanSpeed,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1].fanSpeed,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Power CPU Cooler',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].powerCooler) >
                          int.parse(
                              data3[global.idCompare2 - 1].powerCooler)) ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].powerCooler} watt",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(
                              "${data3[global.idCompare2 - 1].powerCooler} watt",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].powerCooler) <
                          int.parse(
                              data3[global.idCompare2 - 1].powerCooler)) ...[
                        DataCell(
                          Text(
                              "${data3[global.idCompare1 - 1].powerCooler} watt",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].powerCooler} watt",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].powerCooler} watt",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              SizedBox(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].powerCooler} watt",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        SizedBox(
                          width: double.infinity,
                          child: const Text(
                            'Socket Cooler',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: SizedBox(
                            width: 100,
                            child: Text(
                                data3[global.idCompare1 - 1].socketCooler,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: SizedBox(
                            width: 100,
                            child:
                                Text(data3[global.idCompare2 - 1].socketCooler,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal,
                                      height: 1.5,
                                      color: const Color(0xffD7D7D7),
                                    )),
                          ),
                        ),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Dimension Cooler',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].dimensionCooler,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].dimensionCooler,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
  }

  Widget kosongkiri() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(left: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah CPU Cooler",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/cpucooler/list');
        setState(() {
          global.idYangDiCompare = 1;
        });
      },
    );
  }

  Widget kosongkanan() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(right: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah CPU Cooler",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/cpucooler/list');
        setState(() {
          global.idYangDiCompare = 2;
        });
      },
    );
  }

  Widget keisiKiriCompare() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/cpucooler/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merek CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                if (data3[global.idCompare1 - 1].typeCooler ==
                        "Liquid Cooler" &&
                    data3[global.idCompare2 - 1].typeCooler ==
                        "Air Cooler") ...[
                  Text("Type CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].typeCooler}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare2 - 1].typeCooler ==
                        "Liquid Cooler" &&
                    data3[global.idCompare1 - 1].typeCooler ==
                        "Air Cooler") ...[
                  Text("Type CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].typeCooler}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Type CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].typeCooler}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1].fanQuantity) >
                    int.parse(data3[global.idCompare2 - 1].fanQuantity)) ...[
                  Text("Fan Quantitiy",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].fanQuantity}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].fanQuantity) <
                    int.parse(data3[global.idCompare2 - 1].fanQuantity)) ...[
                  Text("Fan Quantitiy",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].fanQuantity}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Fan Quantitiy",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].fanQuantity}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (int.parse(
                        data3[global.idCompare1 - 1].fanSpeed.substring(0, 3)) >
                    int.parse(data3[global.idCompare2 - 1]
                        .fanSpeed
                        .substring(0, 3))) ...[
                  Text("Fan Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].fanSpeed}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].fanSpeed.substring(0, 3)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .fanSpeed
                        .substring(0, 3))) ...[
                  Text("Fan Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].fanSpeed}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Fan Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].fanSpeed}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare1 - 1].powerCooler) >
                    int.parse(data3[global.idCompare2 - 1].powerCooler)) ...[
                  Text("Power CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare1 - 1].powerCooler} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].powerCooler) <
                    int.parse(data3[global.idCompare2 - 1].powerCooler)) ...[
                  Text("Power CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare1 - 1].powerCooler} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Power CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare1 - 1].powerCooler} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                Text("Socket CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].socketCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Dimension CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].dimensionCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKananCompare() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/cpucooler/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merek CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                if (data3[global.idCompare2 - 1].typeCooler ==
                        "Liquid Cooler" &&
                    data3[global.idCompare1 - 1].typeCooler ==
                        "Air Cooler") ...[
                  Text("Type CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].typeCooler}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (data3[global.idCompare1 - 1].typeCooler ==
                        "Liquid Cooler" &&
                    data3[global.idCompare2 - 1].typeCooler ==
                        "Air Cooler") ...[
                  Text("Type CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].typeCooler}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Type CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].typeCooler}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare2 - 1].fanQuantity) >
                    int.parse(data3[global.idCompare1 - 1].fanQuantity)) ...[
                  Text("Fan Quantitiy",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].fanQuantity}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].fanQuantity) <
                    int.parse(data3[global.idCompare1 - 1].fanQuantity)) ...[
                  Text("Fan Quantitiy",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].fanQuantity}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Fan Quantitiy",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].fanQuantity}x\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (int.parse(
                        data3[global.idCompare2 - 1].fanSpeed.substring(0, 3)) >
                    int.parse(data3[global.idCompare1 - 1]
                        .fanSpeed
                        .substring(0, 3))) ...[
                  Text("Fan Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].fanSpeed}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(
                        data3[global.idCompare2 - 1].fanSpeed.substring(0, 3)) <
                    int.parse(data3[global.idCompare1 - 1]
                        .fanSpeed
                        .substring(0, 3))) ...[
                  Text("Fan Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].fanSpeed}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Fan Speed",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].fanSpeed}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (int.parse(data3[global.idCompare2 - 1].powerCooler) >
                    int.parse(data3[global.idCompare1 - 1].powerCooler)) ...[
                  Text("Power CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare2 - 1].powerCooler} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].powerCooler) <
                    int.parse(data3[global.idCompare1 - 1].powerCooler)) ...[
                  Text("Power CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare2 - 1].powerCooler} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Power CPU Cooler",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare2 - 1].powerCooler} Watt\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                Text("Socket CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].socketCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Dimension CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].dimensionCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKanan() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/cpucooler/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merek CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Type CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].typeCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Fan Quantitiy",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].fanQuantity}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Fan Speed",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].fanSpeed}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Power CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].powerCooler} Watt\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Socket CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].socketCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Dimension CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].dimensionCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKiri() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/cpucooler/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merek CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Type CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].typeCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Fan Quantitiy",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].fanQuantity}x\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Fan Speed",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].fanSpeed}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Power CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].powerCooler} Watt\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Socket CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].socketCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Dimension CPU Cooler",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].dimensionCooler}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }
}
