// ignore_for_file: unused_import, prefer_const_constructors_in_immutables, prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:rakit_pc/Models/models_vga.dart';
import '../../Api/api_vga.dart';
import '../home_page/floating_button.dart';

class CompareVga extends StatefulWidget {
  CompareVga({Key? key}) : super(key: key);
  @override
  State<CompareVga> createState() => _CompareVgaState();
}

class _CompareVgaState extends State<CompareVga> {
  var formatter = NumberFormat('#,###,000');
  late Future data;
  List<Vga> data3 = [];

  @override
  void initState() {
    data = fetchVga();
    data.then((value) => setState(() {
          data3 = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        backgroundColor: const Color(0x44000000).withOpacity(0),
        elevation: 0,

        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/compare/list');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        //backgroundColor: Color.fromARGB(240, 143, 5, 131),
        title: Text(
          "Compare VGA",
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: result(),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget result() {
    if (global.idCompare1 != 0 && global.idCompare2 != 0) {
      return ListView(
        children: [
          SizedBox(
            child: adaDua(),
          ),
        ],
      );
    } else {
      return Row(
        children: <Widget>[
          if (global.idCompare1 == 0 && global.idCompare2 == 0) ...[
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.47,
              height: MediaQuery.of(context).size.height * 0.76,
              child: kosongkiri(),
            ),
            const SizedBox(
              width: 20,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.47,
                height: MediaQuery.of(context).size.height * 0.76,
                child: kosongkanan()),
          ] else if (global.idCompare1 != 0 && global.idCompare2 == 0) ...[
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKiri(),
              ),
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkanan()),
          ] else if (global.idCompare1 == 0 && global.idCompare2 != 0) ...[
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                // height: MediaQuery.of(context).size.height * 0.72,
                child: kosongkiri()),
            SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: keisiKanan(),
              ),
            ),
          ]
        ],
      );
    }
  }

  Widget adaDua() {
    return data3.isEmpty
        ? const Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare1 - 1].imageLink))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/vga/list');
                    setState(() {
                      global.idYangDiCompare = 1;
                    });
                  },
                ),
                SizedBox(
                  width: 20,
                ),
                GestureDetector(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.42,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white24.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(14),
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                data3[global.idCompare2 - 1].imageLink))),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/compare/vga/list');
                    setState(() {
                      global.idYangDiCompare = 2;
                    });
                  },
                ),
              ]),
              DataTable(
                dataRowHeight: MediaQuery.of(context).size.height * 0.1,
                columnSpacing: MediaQuery.of(context).size.width * 0.05,
                border: TableBorder(
                    horizontalInside:
                        BorderSide(color: Colors.white, width: 1.4),
                    top: BorderSide.none),
                columns: const <DataColumn>[
                  DataColumn(
                    label: Expanded(
                        child: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    )),
                  ),
                  DataColumn(
                    label: Text(""),
                  ),
                  DataColumn(
                    label: Text(
                      '',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
                rows: <DataRow>[
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Container(
                          width: double.infinity,
                          child: const Text(
                            'Nama VGA',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      DataCell(
                        Container(
                          width: 100,
                          child: Text(data3[global.idCompare1 - 1].namaVga,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].namaVga,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                              fontSize: 13,
                              fontWeight: FontWeight.normal,
                              height: 1.5,
                              color: const Color(0xffD7D7D7),
                            )),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Merek VGA',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].merkVga,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].merkVga,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Release Date',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].releaseDate,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].releaseDate,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Generation',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].generation,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].generation,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Harga VGA',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare1 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        data3[global.idCompare2 - 1]
                                            .harga
                                            .toString()))}",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Base/Boost Clocks',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(data3[global.idCompare1 - 1].boostClock) >
                          int.parse(
                              data3[global.idCompare2 - 1].boostClock)) ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].baseClocks}/${data3[global.idCompare1 - 1].boostClock}",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(
                              "${data3[global.idCompare2 - 1].baseClocks}/${data3[global.idCompare2 - 1].boostClock}",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].boostClock) <
                          int.parse(
                              data3[global.idCompare2 - 1].boostClock)) ...[
                        DataCell(
                          Text(
                              "${data3[global.idCompare1 - 1].baseClocks}/${data3[global.idCompare1 - 1].boostClock}",
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].baseClocks}/${data3[global.idCompare2 - 1].boostClock}",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare1 - 1].baseClocks}/${data3[global.idCompare1 - 1].boostClock}",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    "${data3[global.idCompare2 - 1].baseClocks}/${data3[global.idCompare2 - 1].boostClock}",
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Memory VGA',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (data3[global.idCompare1 - 1].memoryVga ==
                              "System Shared" &&
                          data3[global.idCompare2 - 1].memoryVga ==
                              "System Shared") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryVga,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryVga,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (data3[global.idCompare2 - 1].memoryVga !=
                              "System Shared" &&
                          data3[global.idCompare1 - 1].memoryVga ==
                              "System Shared") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryVga,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].memoryVga,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else if (data3[global.idCompare1 - 1].memoryVga !=
                              "System Shared" &&
                          data3[global.idCompare2 - 1].memoryVga ==
                              "System Shared") ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].memoryVga,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].memoryVga,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else ...[
                        if (int.parse(data3[global.idCompare1 - 1].memoryVga) >
                            int.parse(
                                data3[global.idCompare2 - 1].memoryVga)) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1].memoryVga,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(data3[global.idCompare2 - 1].memoryVga,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (int.parse(
                                data3[global.idCompare1 - 1].memoryVga) <
                            int.parse(
                                data3[global.idCompare2 - 1].memoryVga)) ...[
                          DataCell(
                            Text(data3[global.idCompare1 - 1].memoryVga,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1].memoryVga,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare1 - 1].memoryVga,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare2 - 1].memoryVga,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Memory Type',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (data3[global.idCompare1 - 1].memoryClock ==
                              "System Shared" &&
                          data3[global.idCompare2 - 1].memoryClock ==
                              "System Shared") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryType,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryType,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (data3[global.idCompare2 - 1].memoryClock !=
                              "System Shared" &&
                          data3[global.idCompare1 - 1].memoryClock ==
                              "System Shared") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryType,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else if (data3[global.idCompare1 - 1].memoryClock !=
                              "System Shared" &&
                          data3[global.idCompare2 - 1].memoryClock ==
                              "System Shared") ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].memoryType,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].memoryType,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else ...[
                        if (int.parse(data3[global.idCompare1 - 1]
                                .memoryType
                                .substring(4, 5)) >
                            int.parse(data3[global.idCompare2 - 1]
                                .memoryType
                                .substring(4, 5))) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1].memoryType,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(data3[global.idCompare2 - 1].memoryType,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (int.parse(data3[global.idCompare1 - 1]
                                .memoryType
                                .substring(4, 5)) <
                            int.parse(data3[global.idCompare2 - 1]
                                .memoryType
                                .substring(4, 5))) ...[
                          DataCell(
                            Text(data3[global.idCompare1 - 1].memoryType,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1].memoryType,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare1 - 1].memoryType,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare2 - 1].memoryType,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Memory Clock',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (data3[global.idCompare1 - 1].memoryClock ==
                              "System Shared" &&
                          data3[global.idCompare2 - 1].memoryClock ==
                              "System Shared") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryClock,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryClock,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (data3[global.idCompare2 - 1].memoryClock !=
                              "System Shared" &&
                          data3[global.idCompare1 - 1].memoryClock ==
                              "System Shared") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryClock,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].memoryClock,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else if (data3[global.idCompare1 - 1].memoryClock !=
                              "System Shared" &&
                          data3[global.idCompare2 - 1].memoryClock ==
                              "System Shared") ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].memoryClock,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].memoryClock,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else ...[
                        if (int.parse(
                                data3[global.idCompare1 - 1].memoryClock) >
                            int.parse(
                                data3[global.idCompare2 - 1].memoryClock)) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1].memoryClock,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(data3[global.idCompare2 - 1].memoryClock,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (int.parse(
                                data3[global.idCompare1 - 1].memoryClock) <
                            int.parse(
                                data3[global.idCompare2 - 1].memoryClock)) ...[
                          DataCell(
                            Text(data3[global.idCompare1 - 1].memoryBus,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1].memoryClock,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare1 - 1].memoryClock,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare2 - 1].memoryClock,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Memory Bus',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (data3[global.idCompare1 - 1].memoryBus ==
                              "System Shared" &&
                          data3[global.idCompare2 - 1].memoryBus ==
                              "System Shared") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryBus,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryBus,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (data3[global.idCompare2 - 1].memoryBus !=
                              "System Shared" &&
                          data3[global.idCompare1 - 1].memoryBus ==
                              "System Shared") ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].memoryBus,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1].memoryBus,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else if (data3[global.idCompare1 - 1].memoryBus !=
                              "System Shared" &&
                          data3[global.idCompare2 - 1].memoryBus ==
                              "System Shared") ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1].memoryBus,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].memoryBus,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else ...[
                        if (int.parse(data3[global.idCompare1 - 1].memoryBus) >
                            int.parse(
                                data3[global.idCompare2 - 1].memoryBus)) ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare1 - 1].memoryBus,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Text(data3[global.idCompare2 - 1].memoryBus,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                        ] else if (int.parse(
                                data3[global.idCompare1 - 1].memoryBus) <
                            int.parse(
                                data3[global.idCompare2 - 1].memoryBus)) ...[
                          DataCell(
                            Text(data3[global.idCompare1 - 1].memoryBus,
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize: 13,
                                    fontWeight: FontWeight.normal,
                                    height: 1.5,
                                    color: const Color(0xffD7D7D7))),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 60,
                                  child: Text(
                                      data3[global.idCompare2 - 1].memoryBus,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/Centang.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ] else ...[
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare1 - 1].memoryBus,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                          DataCell(
                            Row(
                              children: [
                                Container(
                                  width: 50,
                                  child: Text(
                                      data3[global.idCompare2 - 1].memoryBus,
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.normal,
                                          height: 1.5,
                                          color: const Color(0xffD7D7D7))),
                                ),
                                Image.asset(
                                  'assets/img/CentangKuning.png',
                                  width: 25,
                                  height: 25,
                                )
                              ],
                            ),
                          ),
                        ]
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(
                        Text(
                          'Power Consumption',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      if (int.parse(
                              data3[global.idCompare1 - 1].powerConsumption) <
                          int.parse(data3[global.idCompare2 - 1]
                              .powerConsumption)) ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare1 - 1]
                                        .powerConsumption,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Text(data3[global.idCompare2 - 1].powerConsumption,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ] else if (int.parse(
                              data3[global.idCompare1 - 1].powerConsumption) >
                          int.parse(data3[global.idCompare2 - 1]
                              .powerConsumption)) ...[
                        DataCell(
                          Text(data3[global.idCompare1 - 1].powerConsumption,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 60,
                                child: Text(
                                    data3[global.idCompare2 - 1]
                                        .powerConsumption,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/Centang.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ] else ...[
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare1 - 1]
                                        .powerConsumption,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                        DataCell(
                          Row(
                            children: [
                              Container(
                                width: 50,
                                child: Text(
                                    data3[global.idCompare2 - 1]
                                        .powerConsumption,
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.poppins(
                                        fontSize: 13,
                                        fontWeight: FontWeight.normal,
                                        height: 1.5,
                                        color: const Color(0xffD7D7D7))),
                              ),
                              Image.asset(
                                'assets/img/CentangKuning.png',
                                width: 25,
                                height: 25,
                              )
                            ],
                          ),
                        ),
                      ]
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Raytracing',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text("${data3[global.idCompare1 - 1].rTcores}RT Cores",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text("${data3[global.idCompare2 - 1].rTcores}RT Cores",
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Architecture',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].architecture,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].architecture,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'VGA Interface',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].vgaInterface,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].vgaInterface,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Output Port',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(data3[global.idCompare1 - 1].outputPort,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(data3[global.idCompare2 - 1].outputPort,
                              textAlign: TextAlign.center,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Power Connection',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].powerConnection,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].powerConnection,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Graphic API',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(data3[global.idCompare1 - 1].graphicApi,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                      DataCell(
                        SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Text(data3[global.idCompare2 - 1].graphicApi,
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                  fontSize: 13,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5,
                                  color: const Color(0xffD7D7D7))),
                        ),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Dimension VGA',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].dimensionVga,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].dimensionVga,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                  DataRow(
                    cells: <DataCell>[
                      DataCell(Text(
                        'Display Technology',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      DataCell(
                        Text(data3[global.idCompare1 - 1].displayTechnology,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                      DataCell(
                        Text(data3[global.idCompare2 - 1].displayTechnology,
                            textAlign: TextAlign.left,
                            style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                                height: 1.5,
                                color: const Color(0xffD7D7D7))),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
  }

  Widget kosongkiri() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(left: 5),
          height: MediaQuery.of(context).size.height * 0.4,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah VGA",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/vga/list');
        setState(() {
          global.idYangDiCompare = 1;
        });
      },
    );
  }

  Widget kosongkanan() {
    return InkWell(
      child: Container(
          decoration: BoxDecoration(
              color: const Color(0xffDBD8E3).withOpacity(0.3),
              borderRadius: const BorderRadius.all(Radius.circular(10))),
          margin: const EdgeInsets.only(right: 5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width * 0.46,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              const Icon(
                Icons.add_box_rounded,
                color: Colors.white,
                size: 90.0,
              ),
              Text(
                "Tambah VGA",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 19.0,
                  color: Colors.white,
                ),
              ),
            ],
          )),
      onTap: () {
        Navigator.pushNamed(context, '/compare/vga/list');
        setState(() {
          global.idYangDiCompare = 2;
        });
      },
    );
  }

  Widget keisiKiriCompare() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/vga/list');
              setState(() {
                global.idYangDiCompare = 1;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merek VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Release Date",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].releaseDate}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Generation",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].generation}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].harga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare1 - 1].boostClock) >
                    int.parse(data3[global.idCompare2 - 1].boostClock)) ...[
                  Text("Base/Boost Clocks",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare1 - 1].baseClocks}/${data3[global.idCompare1 - 1].boostClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].boostClock) <
                    int.parse(data3[global.idCompare2 - 1].boostClock)) ...[
                  Text("Base/Boost Clocks",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare1 - 1].baseClocks}/${data3[global.idCompare1 - 1].boostClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Base/Boost Clocks",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare1 - 1].baseClocks}/${data3[global.idCompare1 - 1].boostClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (data3[global.idCompare1 - 1].memoryVga ==
                    "System Shared") ...[
                  Text("Memory VGA",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare1 - 1].memoryVga}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else if (data3[global.idCompare2 - 1].memoryVga ==
                        "System Shared" ||
                    (int.parse(data3[global.idCompare1 - 1].memoryVga) >
                        int.parse(data3[global.idCompare2 - 1].memoryVga))) ...[
                  Text("Memory VGA",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].memoryVga}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].memoryVga) <
                    int.parse(data3[global.idCompare2 - 1].memoryVga)) ...[
                  Text("Memory VGA",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].memoryVga}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Memory VGA",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memoryVga}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (data3[global.idCompare1 - 1].memoryType ==
                    "System Shared") ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else if (data3[global.idCompare2 - 1].memoryType ==
                        "System Shared" ||
                    (int.parse(data3[global.idCompare1 - 1]
                            .memoryType
                            .substring(4, 5)) >
                        int.parse(data3[global.idCompare2 - 1]
                            .memoryType
                            .substring(4, 5)))) ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(4, 5)) <
                    int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(4, 5))) ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (data3[global.idCompare1 - 1].memoryClock ==
                    "System Shared") ...[
                  Text("Memory Clock",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare1 - 1].memoryClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else if (data3[global.idCompare2 - 1].memoryClock ==
                        "System Shared" ||
                    int.parse(data3[global.idCompare1 - 1].memoryClock) >
                        int.parse(
                            data3[global.idCompare2 - 1].memoryClock)) ...[
                  Text("Memory Clock",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].memoryClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].memoryClock) <
                    int.parse(data3[global.idCompare2 - 1].memoryClock)) ...[
                  Text("Memory Clock",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].memoryClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Memory Clock",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memoryClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (data3[global.idCompare1 - 1].memoryBus ==
                    "System Shared") ...[
                  Text("Memory Bus",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare1 - 1].memoryBus}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else if (data3[global.idCompare2 - 1].memoryBus ==
                        "System Shared" ||
                    (int.parse(data3[global.idCompare1 - 1].memoryBus) >
                        int.parse(data3[global.idCompare2 - 1].memoryBus))) ...[
                  Text("Memory Bus",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].memoryBus}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare1 - 1].memoryBus) <
                    int.parse(data3[global.idCompare2 - 1].memoryBus)) ...[
                  Text("Memory Bus",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].memoryBus}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Memory Bus",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memoryBus}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare1 - 1].powerConsumption) >
                    int.parse(
                        data3[global.idCompare2 - 1].powerConsumption)) ...[
                  Text("Power Consumption",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare1 - 1].powerConsumption}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else if (int.parse(
                        data3[global.idCompare1 - 1].powerConsumption) <
                    int.parse(
                        data3[global.idCompare2 - 1].powerConsumption)) ...[
                  Text("Power Consumption",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare1 - 1].powerConsumption}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else ...{
                  Text("Power Consumption",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].powerConsumption}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                Text("Raytracing",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                if (data3[global.idCompare1 - 1].rTcores != "No") ...[
                  Text(
                      "Support with ${data3[global.idCompare1 - 1].rTcores}RT Cores\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else ...[
                  Text("${data3[global.idCompare1 - 1].rTcores}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ],
                Text("Architecture",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].architecture}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("VGA Interface",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].vgaInterface}\n",
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Output Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].outputPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Power Connection",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].powerConnection}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Graphic Api",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].graphicApi}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Dimension VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].dimensionVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Display Technology",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].displayTechnology}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKananCompare() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/vga/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merek VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Release Date",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].releaseDate}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Generation",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].generation}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].harga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                if (int.parse(data3[global.idCompare2 - 1].boostClock) >
                    int.parse(data3[global.idCompare1 - 1].boostClock)) ...[
                  Text("Base/Boost Clocks",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text(
                      "${data3[global.idCompare2 - 1].baseClocks}/${data3[global.idCompare2 - 1].boostClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].boostClock) <
                    int.parse(data3[global.idCompare1 - 1].boostClock)) ...[
                  Text("Base/Boost Clocks",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text(
                      "${data3[global.idCompare2 - 1].baseClocks}/${data3[global.idCompare2 - 1].boostClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Base/Boost Clocks",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text(
                      "${data3[global.idCompare2 - 1].baseClocks}/${data3[global.idCompare2 - 1].boostClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (data3[global.idCompare2 - 1].memoryVga ==
                    "System Shared") ...[
                  Text("Memory VGA",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare2 - 1].memoryVga}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else if (data3[global.idCompare1 - 1].memoryVga ==
                        "System Shared" ||
                    int.parse(data3[global.idCompare2 - 1].memoryVga) >
                        int.parse(data3[global.idCompare1 - 1].memoryVga)) ...[
                  Text("Memory VGA",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].memoryVga}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].memoryVga) <
                    int.parse(data3[global.idCompare1 - 1].memoryVga)) ...[
                  Text("Memory VGA",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].memoryVga}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...[
                  Text("Memory VGA",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare1 - 1].memoryVga}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                ],
                if (data3[global.idCompare2 - 1].memoryType ==
                    "System Shared") ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else if (data3[global.idCompare1 - 1].memoryType ==
                        "System Shared" ||
                    int.parse(data3[global.idCompare2 - 1]
                            .memoryType
                            .substring(4, 5)) >
                        int.parse(data3[global.idCompare1 - 1]
                            .memoryType
                            .substring(4, 5))) ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1]
                        .memoryType
                        .substring(4, 5)) <
                    int.parse(data3[global.idCompare1 - 1]
                        .memoryType
                        .substring(4, 5))) ...[
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Memory Type",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].memoryType}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (data3[global.idCompare2 - 1].memoryClock ==
                    "System Shared") ...[
                  Text("Memory Clock",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare2 - 1].memoryClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else if (data3[global.idCompare1 - 1].memoryClock ==
                        "System Shared" ||
                    int.parse(data3[global.idCompare2 - 1].memoryClock) >
                        int.parse(
                            data3[global.idCompare1 - 1].memoryClock)) ...[
                  Text("Memory Clock",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].memoryClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].memoryClock) <
                    int.parse(data3[global.idCompare1 - 1].memoryClock)) ...[
                  Text("Memory Clock",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].memoryClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Memory Clock",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].memoryClock}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (data3[global.idCompare2 - 1].memoryBus ==
                    "System Shared") ...[
                  Text("Memory Bus",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffD7D7D7))),
                  Text("${data3[global.idCompare2 - 1].memoryBus}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else if (data3[global.idCompare1 - 1].memoryBus ==
                        "System Shared" ||
                    int.parse(data3[global.idCompare2 - 1].memoryBus) >
                        int.parse(data3[global.idCompare1 - 1].memoryBus)) ...[
                  Text("Memory Bus",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].memoryBus}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else if (int.parse(data3[global.idCompare2 - 1].memoryBus) <
                    int.parse(data3[global.idCompare1 - 1].memoryBus)) ...[
                  Text("Memory Bus",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].memoryBus}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else ...{
                  Text("Memory Bus",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].memoryBus}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                if (int.parse(data3[global.idCompare2 - 1].powerConsumption) >
                    int.parse(
                        data3[global.idCompare1 - 1].powerConsumption)) ...[
                  Text("Power Consumption",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffEB1D36))),
                  Text("${data3[global.idCompare2 - 1].powerConsumption}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffEB1D36))),
                ] else if (int.parse(
                        data3[global.idCompare2 - 1].powerConsumption) <
                    int.parse(
                        data3[global.idCompare1 - 1].powerConsumption)) ...[
                  Text("Power Consumption",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xff3CCF4E))),
                  Text("${data3[global.idCompare2 - 1].powerConsumption}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xff3CCF4E))),
                ] else ...{
                  Text("Power Consumption",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Color(0xffFFB200))),
                  Text("${data3[global.idCompare2 - 1].powerConsumption}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffFFB200))),
                },
                Text("Raytracing",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                if (data3[global.idCompare2 - 1].rTcores != "No") ...[
                  Text(
                      "Support with ${data3[global.idCompare2 - 1].rTcores}RT Cores\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else ...[
                  Text("${data3[global.idCompare2 - 1].rTcores}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ],
                Text("Architecture",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].architecture}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("VGA Interface",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].vgaInterface}\n",
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Output Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].outputPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Power Connection",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].powerConnection}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Graphic Api",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].graphicApi}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Dimension VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].dimensionVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Display Technology",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].displayTechnology}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKiri() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/vga/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare1 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].namaVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merek VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].merkVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Release Date",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].releaseDate}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Generation",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].generation}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare1 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Base Clocks",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].baseClocks}GHz\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Boost Clocks",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].boostClock}GHz\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Memory VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].memoryVga}GB\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Memory Type",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].memoryType}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Memory Clock",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].memoryClock}GHz\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Memory Bus",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].memoryBus}bit\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Power Consumption",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "${data3[global.idCompare1 - 1].powerConsumption}Watt\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Architecture",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].architecture}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Raytracing",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                if (data3[global.idCompare1 - 1].rTcores != "No") ...[
                  Text(
                      "Support with ${data3[global.idCompare1 - 1].rTcores}RT Cores\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else ...[
                  Text("${data3[global.idCompare1 - 1].rTcores}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ],
                Text("VGA Interface",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].vgaInterface}\n",
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Output Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].outputPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Power Connection",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].powerConnection}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Graphic Api",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].graphicApi}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Dimension VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].dimensionVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Display Technology",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare1 - 1].displayTechnology}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }

  Widget keisiKanan() {
    return data3.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              color: Colors.black,
            ),
          )
        : InkWell(
            onTap: () {
              Navigator.pushNamed(context, '/compare/vga/list');
              setState(() {
                global.idYangDiCompare = 2;
              });
            },
            child: Column(
              children: [
                Image.network(
                  data3[global.idCompare2 - 1].imageLink,
                  width: MediaQuery.of(context).size.width * 0.3,
                  height: MediaQuery.of(context).size.height * 0.2,
                ),
                Text("Nama VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].namaVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Merek VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].merkVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Release Date",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].releaseDate}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Generation",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].generation}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Harga",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "Rp ${formatter
                            .format(int.parse(
                                data3[global.idCompare2 - 1].harga.toString()))}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Base Clocks",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].baseClocks}GHz\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Boost Clocks",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].boostClock}GHz\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Memory VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].memoryVga}GB\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Memory Type",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].memoryType}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Memory Clock",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].memoryClock}GHz\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Memory Bus",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].memoryBus}bit\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Power Consumption",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text(
                    "${data3[global.idCompare2 - 1].powerConsumption}Watt\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Architecture",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].architecture}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Raytracing",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                if (data3[global.idCompare2 - 1].rTcores != "No") ...[
                  Text(
                      "Support with ${data3[global.idCompare2 - 1].rTcores}RT Cores\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ] else ...[
                  Text("${data3[global.idCompare2 - 1].rTcores}\n",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          height: 1.5,
                          color: Color(0xffD7D7D7))),
                ],
                Text("VGA Interface",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].vgaInterface}\n",
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Output Port",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].outputPort}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Power Connection",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].powerConnection}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Graphic Api",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].graphicApi}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Dimension VGA",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].dimensionVga}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
                Text("Display Technology",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        color: Color(0xffD7D7D7))),
                Text("${data3[global.idCompare2 - 1].displayTechnology}\n",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                        color: Color(0xffD7D7D7))),
              ],
            ));
  }
}
