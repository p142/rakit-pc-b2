// ignore_for_file: avoid_unnecessary_containers, unnecessary_null_comparison, unnecessary_const, prefer_const_constructors
import 'package:google_fonts/google_fonts.dart';
import 'package:rakit_pc/Api/api_simpan_upload.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:rakit_pc/Api/api_casing_id.dart';
import 'package:rakit_pc/Api/api_cpu_cooler_id.dart';
import 'package:rakit_pc/Api/api_cpu_id.dart';
import 'package:rakit_pc/Api/api_motherboard_id.dart';
import 'package:rakit_pc/Api/api_psu_id.dart';
import 'package:rakit_pc/Api/api_ram_id.dart';
import 'package:rakit_pc/Api/api_storage_id.dart';
import 'package:rakit_pc/Api/api_vga_id.dart';
import 'package:rakit_pc/Api/api_fan_id.dart';
import 'package:rakit_pc/Models/models_casing.dart';
import 'package:rakit_pc/Models/models_cpu.dart';
import 'package:rakit_pc/Models/models_cpu_cooler.dart';
import 'package:rakit_pc/Models/models_motherboard.dart';
import 'package:rakit_pc/Models/models_psu.dart';
import 'package:rakit_pc/Models/models_ram.dart';
import 'package:rakit_pc/Models/models_storage.dart';
import 'package:rakit_pc/Models/models_vga.dart';
import 'package:rakit_pc/Models/models_fan.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter/services.dart';
import 'package:quickalert/quickalert.dart';

import '../home_page/floating_button.dart';

class BuildAdvanced extends StatefulWidget {
  const BuildAdvanced({Key? key}) : super(key: key);

  @override
  State<BuildAdvanced> createState() => _BuildAdvancedState();
}

class _BuildAdvancedState extends State<BuildAdvanced> {
  String cpuSocket = "";
  String moboSocket = "";
  String messageCompability = "";
  int compatibleInt = 0;
  String moboChecker = "";
  var formatter = NumberFormat('#,###,000');
  bool wesCpuMobo = false;
  bool wesMoboNvme = false;
  bool wesCoolerMobo = false;
  bool wesRam = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    setState(() {
      global.compareOrList = 0;
      global.hargaHarga = global.hargaCase +
          global.hargaCooler +
          global.hargaCpu +
          global.hargaVga +
          global.hargaFan1 +
          global.hargaFan2 +
          global.hargaFan3 +
          global.hargaMobo +
          global.hargaPsu +
          global.hargaRam1 +
          global.hargaRam2 +
          global.hargaStorage1 +
          global.hargaStorage2;
      global.wattTotal = global.wattCooler +
          global.wattCpu +
          global.wattVga +
          global.wattCooler +
          global.wattFan1 +
          global.wattFan2 +
          global.wattFan3 +
          global.wattRam1 +
          global.wattRam2 +
          global.wattStorage1 +
          global.wattStorage2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff342C4C),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        title: Text(
          'Build Advanced ',
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          kotakCompability(),
          const SizedBox(
            width: 30,
            height: 30,
          ),
          Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Total Harga : ',
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ]),
          Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Align(
              alignment: Alignment.center,
              child: Text(
                  "Rp ${formatter.format(int.parse(global.hargaHarga.toString()))}",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                    fontSize: 36,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  )),
            ),
          ]),
          if (global.idCpuAdv != 0 &&
              global.idRamAdv != 0 &&
              global.idMoboAdv != 0 &&
              global.idVgaAdv != 0 &&
              global.idStorageAdv != 0 &&
              global.idPsuAdv != 0 &&
              global.idFanAdv != 0 &&
              global.idCpuCoolerAdv != 0) ...[
            Container(
              margin: const EdgeInsets.only(top: 10),
              padding: const EdgeInsets.symmetric(horizontal: 70),
              height: MediaQuery.of(context).size.height * 0.05,
              child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.white.withOpacity(0.2),
                  ),
                  onPressed: () {
                    Clipboard.setData(ClipboardData(
                        text: "Rakitan Build tanggal ${DateTime.now().year}/${DateTime.now().month}/${DateTime.now().day}\n\nCPU : ${global.namaCpu}\nHarga : Rp ${formatter
                                .format(int.parse(global.hargaCpu.toString()))}\n\nVGA : ${global.namaVga}\nHarga : Rp ${formatter
                                .format(int.parse(global.hargaVga.toString()))}\n\nRAM : ${global.namaRam}\nHarga : Rp ${formatter
                                .format(int.parse(global.hargaRam1.toString()))}\n\nStorage : ${global.namaStorage}\nHarga : Rp ${formatter
                                .format(
                                    int.parse(global.hargaStorage1.toString()))}\n\nPower Supply : ${global.namaPsu}\nHarga : Rp ${formatter
                                .format(int.parse(global.hargaPsu.toString()))}\n\nCPU Cooler : ${global.namaCpuCooler}\nHarga : Rp ${formatter
                                .format(
                                    int.parse(global.hargaCooler.toString()))}\n\nFan : ${global.namaFan}\nHarga : Rp ${formatter
                                .format(int.parse(global.hargaFan1.toString()))}\n\nCasing : ${global.namaCase}\nHarga : Rp ${formatter
                                .format(int.parse(global.hargaCase.toString()))}\n\nHarga Total : Rp ${formatter
                                .format(int.parse(global.hargaHarga.toString()))}"));
                    Fluttertoast.showToast(
                        msg: "Build Berhasil di Copy ke Clipboard !");
                  },
                  icon: const Icon(Icons.copy),
                  label: const Text("Copy to Clipboard")),
            ),
          ] else ...[
            Container(
              margin: const EdgeInsets.only(top: 10),
              padding: const EdgeInsets.symmetric(horizontal: 70),
              height: MediaQuery.of(context).size.height * 0.05,
              child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.white.withOpacity(0.2),
                  ),
                  onPressed: null,
                  icon: const Icon(Icons.copy),
                  label: const Text("Copy to Clipboard")),
            ),
          ],
          const SizedBox(
            width: 30,
            height: 30,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Casing',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: const Divider(
                        color: Colors.white,
                        height: 25,
                        thickness: 3,
                      ),
                    ),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.idCaseAdv = 0;
                        global.hargaCase = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [getCasing()],
            ),
          ),
          const SizedBox(
            width: 30,
            height: 20,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'CPU',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        child: const Divider(
                          color: Colors.white,
                          height: 25,
                          thickness: 3,
                        )),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.idCpuAdv = 0;
                        global.hargaCpu = 0;
                        global.wattCpu = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [getCpu()],
          ),
          const SizedBox(
            width: 30,
            height: 20,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'CPU Cooler',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        child: const Divider(
                          color: Colors.white,
                          height: 25,
                          thickness: 3,
                        )),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.idCpuCoolerAdv = 0;
                        global.hargaCooler = 0;
                        global.wattCooler = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [getCpuCooler()],
            ),
          ),
          const SizedBox(
            width: 30,
            height: 20,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Motherboard',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        child: const Divider(
                          color: Colors.white,
                          height: 25,
                          thickness: 3,
                        )),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.idMoboAdv = 0;
                        global.hargaMobo = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [getMobo()],
            ),
          ),
          const SizedBox(
            width: 30,
            height: 20,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Power Supply',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        child: const Divider(
                          color: Colors.white,
                          height: 25,
                          thickness: 3,
                        )),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.hargaPsu = 0;
                        global.idPsuAdv = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [getPowerSupply()],
            ),
          ),
          const SizedBox(
            width: 30,
            height: 20,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'RAM',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        child: const Divider(
                          color: Colors.white,
                          height: 25,
                          thickness: 3,
                        )),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.idRamAdv = 0;
                        global.idRam2Adv = 0;
                        global.hargaRam1 = 0;
                        global.hargaRam2 = 0;

                        global.wattRam1 = 0;
                        global.wattRam2 = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [getRam(), getRamAgain()],
            ),
          ),
          const SizedBox(
            width: 30,
            height: 20,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Storage',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        child: const Divider(
                          color: Colors.white,
                          height: 25,
                          thickness: 3,
                        )),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.idStorageAdv = 0;
                        global.idStorage2Adv = 0;
                        global.hargaStorage1 = 0;
                        global.hargaStorage2 = 0;
                        global.wattStorage1 = 0;
                        global.wattStorage2 = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [getStorage(), getStorageAgain()],
            ),
          ),
          const SizedBox(
            width: 30,
            height: 20,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'VGA',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        child: const Divider(
                          color: Colors.white,
                          height: 25,
                          thickness: 3,
                        )),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.idVgaAdv = 0;
                        global.hargaVga = 0;
                        global.wattVga = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [getVga()],
            ),
          ),
          const SizedBox(
            width: 30,
            height: 20,
          ),
          Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Fan',
                    style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        child: const Divider(
                          color: Colors.white,
                          height: 25,
                          thickness: 3,
                        )),
                  ),
                  InkWell(
                    child: const Icon(
                      Icons.close,
                      color: Colors.redAccent,
                      size: 30.0,
                    ),
                    onTap: () {
                      setState(() {
                        global.idFanAdv = 0;
                        global.idFan2Adv = 0;
                        global.idFan3Adv = 0;
                        global.hargaFan1 = 0;
                        global.hargaFan2 = 0;
                        global.hargaFan3 = 0;
                        global.wattFan1 = 0;
                        global.wattFan2 = 0;
                        global.wattFan3 = 0;

                        init();
                      });
                    },
                  )
                ],
              )),
          Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [getFan(), getFanAgain(), getFanAgainAgain()]),
          ),
          const SizedBox(
            width: 30,
            height: 40,
          ),
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.07,
                  width: MediaQuery.of(context).size.width * 0.50,
                  child: ElevatedButton(
                    onPressed: () async {
                      setState(() {
                        if (global.compatible == "All parts compatible") {
                          compatibleInt = 1;
                        } else {
                          compatibleInt = 0;
                        }
                      });
                      try {
                        bool response = await postData(
                          global.uidUser,
                          compatibleInt.toString(),
                          global.hargaHarga.toString(),
                          global.idCaseAdv.toString(),
                          global.hargaCase.toString(),
                          global.idCpuAdv.toString(),
                          global.hargaCpu.toString(),
                          global.idCpuCoolerAdv.toString(),
                          global.hargaCooler.toString(),
                          global.idMoboAdv.toString(),
                          global.hargaMobo.toString(),
                          global.idPsuAdv.toString(),
                          global.hargaPsu.toString(),
                          global.idRamAdv.toString(),
                          global.hargaRam1.toString(),
                          global.idRamAdv.toString(),
                          global.hargaRam2.toString(),
                          global.idStorageAdv.toString(),
                          global.hargaStorage1.toString(),
                          global.idStorage2Adv.toString(),
                          global.hargaStorage2.toString(),
                          global.idVgaAdv.toString(),
                          global.hargaVga.toString(),
                          global.idFanAdv.toString(),
                          global.hargaFan1.toString(),
                          global.idFan2Adv.toString(),
                          global.hargaFan2.toString(),
                          global.idFan3Adv.toString(),
                          global.hargaFan3.toString(),
                        );
                        if (response) {
                          QuickAlert.show(
                              context: context,
                              type: QuickAlertType.success,
                              text: 'Build Berhasil Disimpan!',
                              confirmBtnText: 'Oke',
                              confirmBtnColor: HexColor("#12c06a"),
                              onConfirmBtnTap: () {
                                Navigator.pushNamed(context, '/history');
                              });
                        } else {
                          QuickAlert.show(
                            context: context,
                            type: QuickAlertType.error,
                            title: 'Error',
                            text: 'Ada Kesalahan, Coba Lagi Nanti',
                          );
                        }
                      } catch (e) {
                        QuickAlert.show(
                          context: context,
                          type: QuickAlertType.error,
                          title: 'Error',
                          text: 'Ada Kesalahan, Coba Lagi Nanti',
                        );
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      // primary: Colors.white,
                      // padding: EdgeInsets.symmetric(vertical: 15, horizontal: 70),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14.89),
                      ),
                    ),
                    child: Text('Simpan Build',
                        style: GoogleFonts.poppins(
                            color: HexColor("#9997FF"),
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget getCasing() {
    if (global.idCaseAdv != 0) {
      return FutureBuilder(
          future: fetchCasingId(global.idCaseAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, index) {
                    Casing casingGan = snapshot.data[index];
                    global.namaCase = casingGan.namaCasing;

                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/part/list/casing');
                              setState(
                                () {
                                  global.namaPart = "Casing";
                                },
                              );
                            },
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  selected: true,
                                  selectedTileColor: const Color(0xFFffffff),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  leading: ClipRRect(
                                    child: Image.network(
                                      casingGan.imageLink,
                                    ),
                                  ),
                                  title: Text(
                                    casingGan.namaCasing,
                                    style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  subtitle: Text(
                                      "Rp ${formatter
                                              .format(
                                                  int.parse(casingGan.harga))}",
                                      style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: const Color(0xff52BB6F),
                                      )),
                                ),
                              ],
                            ),
                          )),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
      //
    } else {
      return ElevatedButton.icon(
          // ignore: prefer__ructors
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                const Color.fromARGB(255, 121, 119, 255)),
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/part/list/casing');
            setState(
              () {
                global.namaPart = "Casing";
              },
            );
          },
          icon: const Icon(Icons.add),
          label: Text('Pilih Casing',
              style: GoogleFonts.poppins(
                  fontSize: 17, fontWeight: FontWeight.w600)));
    }
  }

  Widget kotakCompability() {
    setState(() {
      global.compatible = "All parts compatible";
    });

    if (global.socketMobo != "") {
      if (global.socketMobo.substring(0, 3).toLowerCase() == "lga") {
        moboChecker = global.socketMobo.substring(4, 8);
      } else {
        moboChecker = global.socketMobo;
      }
    }

    if ((global.socketCpu.toLowerCase() != global.socketMobo.toLowerCase()) ||
        (global.idRamAdv != global.idRam2Adv &&
            global.idRamAdv != 0 &&
            global.idRam2Adv != 0) ||
        (global.socketCooler
                .toLowerCase()
                .contains(moboChecker.toLowerCase()) ==
            false) ||
        (global.moboSupportNvme == false && global.storageNvme == true)) {
      setState(() {
        global.compatible = "Some part Incompatible";
      });
      if ((global.socketCooler
                  .toLowerCase()
                  .contains(moboChecker.toLowerCase()) ==
              false) &&
          (wesCoolerMobo == false)) {
        messageCompability +=
            "- Cpu Cooler tidak kompatible dengan Motherboard\n";
        wesCoolerMobo = true;
      }
      if ((global.moboSupportNvme == false && global.storageNvme == true) &&
          (wesMoboNvme == false)) {
        messageCompability += "- Motberboard tidak support M.2 NVME Storage\n";
        wesMoboNvme = true;
      }
      if ((global.socketCpu.toLowerCase() != global.socketMobo.toLowerCase()) &&
          (wesCpuMobo == false)) {
        messageCompability += "- CPU tidak kompatible dengan motherboard\n";
        wesCpuMobo = true;
      }
      if (((global.idRamAdv != 0 && global.idRam2Adv != 0) &&
              (global.idRamAdv != global.idRam2Adv)) &&
          (wesRam == false)) {
        messageCompability += "- Ram yang berbeda bisa menimbulkan error\n";
        wesRam = true;
      }

      return Column(
        children: <Widget>[
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
            margin: const EdgeInsets.all(0.0),
            color: HexColor("#d62828"),
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  dense: true,
                  leading: const Icon(
                    Icons.clear_rounded,
                    color: Colors.white,
                    size: 32,
                  ),
                  title: Text('Compatiblity : ',
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w600)),
                  subtitle: Text(messageCompability,
                      style: GoogleFonts.poppins(
                          color: const Color(0xffffba08),
                          fontSize: 14,
                          fontWeight: FontWeight.w700)),
                ),
              ],
            ),
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
            margin: const EdgeInsets.all(0.0),
            color: HexColor("#2C85C5"),
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: const Icon(
                    Icons.offline_bolt_outlined,
                    color: Colors.white,
                    size: 32,
                  ),
                  title: Text(
                      'Perkiraan Watt : ' + global.wattTotal.toString() + "W",
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w600)),
                ),
              ],
            ),
          ),
        ],
      );
    }
    return Column(
      children: <Widget>[
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
          ),
          margin: const EdgeInsets.all(0.0),
          color: HexColor("#00B16A"),
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              ListTile(
                dense: true,
                leading: const Icon(
                  Icons.check_circle_outline_rounded,
                  color: Colors.white,
                ),
                title: Text('Compatiblity : ',
                    style: GoogleFonts.poppins(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w600)),
                subtitle: Text('Tidak ada isu atau masalah ditemukan.',
                    style: GoogleFonts.poppins(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w400)),
              ),
            ],
          ),
        ),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
          ),
          margin: const EdgeInsets.all(0.0),
          color: HexColor("#2C85C5"),
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              ListTile(
                leading: const Icon(
                  Icons.offline_bolt_outlined,
                  color: Colors.white,
                ),
                title: Text(
                    'Perkiraan Watt : ' + global.wattTotal.toString() + "W",
                    style: GoogleFonts.poppins(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w600)),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget fanWidget(fanGan) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          selected: true,
          selectedTileColor: const Color(0xFFffffff),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 10.0),
          leading: ClipRRect(
            borderRadius: const BorderRadius.all(const Radius.circular(4.0)),
            child: Image.network(
              fanGan.imageLinks,
            ),
          ),
          title: Text(
            fanGan.namaFans,
            style: GoogleFonts.poppins(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          subtitle:
              Text("Rp ${formatter.format(int.parse(fanGan.harga.toString()))}",
                  style: GoogleFonts.poppins(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: const Color(0xff52BB6F),
                  )),
        ),
      ],
    );
  }

  Widget elseFan(int num) {
    return ElevatedButton.icon(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
              const Color.fromARGB(255, 121, 119, 255)),
        ),
        onPressed: () {
          Navigator.pushNamed(context, '/part/list/fan');
          setState(
            () {
              global.sengDiganti = num;
              global.namaPart = "Fan";
            },
          );
        },
        icon: const Icon(Icons.add),
        label: Text('Pilih Fan',
            style: GoogleFonts.poppins(
                fontSize: 17, fontWeight: FontWeight.w600)));
  }

  Widget getFan() {
    if (global.idFanAdv != 0) {
      return FutureBuilder(
          future: fetchFanId(global.idFanAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Fan fanGan = snapshot.data[index];
                    global.namaFan = fanGan.namaFans;
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/part/list/fan');
                                setState(
                                  () {
                                    global.sengDiganti = 1;
                                    global.namaPart = "Fan";
                                    global.hargaFan1 = int.parse(fanGan.harga);
                                  },
                                );
                              },
                              child: fanWidget(fanGan))),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
    } else {
      return elseFan(1);
    }
  }

  Widget getFanAgain() {
    if (global.idFan2Adv != 0) {
      return FutureBuilder(
          future: fetchFanId(global.idFan2Adv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Fan fanGan = snapshot.data[index];
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/part/list/fan');
                                setState(
                                  () {
                                    global.sengDiganti = 2;
                                    global.namaPart = "Fan";
                                    global.hargaFan1 = int.parse(fanGan.harga);
                                  },
                                );
                              },
                              child: fanWidget(fanGan))),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
    } else {
      return elseFan(2);
    }
  }

  Widget getFanAgainAgain() {
    if (global.idFan3Adv != 0) {
      return FutureBuilder(
          future: fetchFanId(global.idFan3Adv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Fan fanGan = snapshot.data[index];
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/part/list/fan');
                                setState(
                                  () {
                                    global.sengDiganti = 3;
                                    global.namaPart = "Fan";
                                    global.hargaFan3 = int.parse(fanGan.harga);
                                  },
                                );
                              },
                              child: fanWidget(fanGan))),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
    } else {
      return elseFan(3);
    }
  }

  Widget getCpu() {
    if (global.idCpuAdv != 0) {
      return FutureBuilder(
          future: fetchCpuId(global.idCpuAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Cpu cpuGan = snapshot.data[index];
                    global.namaCpu = cpuGan.namaCpu;
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 10,
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/part/list/cpu');
                              setState(
                                () {
                                  global.namaPart = "CPU";
                                  global.hargaCpu =
                                      int.parse(cpuGan.harga.toString());
                                },
                              );
                            },
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                ListTile(
                                  selected: true,
                                  selectedTileColor: const Color(0xFFffffff),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  leading: ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(4.0)),
                                    child: Image.network(
                                      cpuGan.imageLink,
                                    ),
                                  ),
                                  title: Text(
                                    cpuGan.namaCpu,
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  subtitle: Text(
                                      "Rp ${formatter.format(int.parse(
                                              cpuGan.harga.toString()))}",
                                      style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: const Color(0xff52BB6F),
                                      )),
                                ),
                              ],
                            ),
                          )),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
      //
    } else {
      return ElevatedButton.icon(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                const Color.fromARGB(255, 121, 119, 255)),
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/part/list/cpu');
            setState(
              () {
                global.namaPart = "CPU";
              },
            );
          },
          icon: const Icon(Icons.add),
          label: Text('Pilih CPU',
              style: GoogleFonts.poppins(
                  fontSize: 17, fontWeight: FontWeight.w600)));
    }
  }

  Widget getCpuCooler() {
    if (global.idCpuCoolerAdv != 0) {
      return FutureBuilder(
          future: fetchCpuCoolerId(global.idCpuCoolerAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    CpuCooler cpuCoolerGan = snapshot.data[index];
                    global.namaCpuCooler = cpuCoolerGan.namaCooler;
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/part/list/cooler');
                              setState(
                                () {
                                  global.namaPart = "CPU Cooler";
                                  global.hargaCooler =
                                      int.parse(cpuCoolerGan.harga);
                                },
                              );
                            },
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                ListTile(
                                  selected: true,
                                  selectedTileColor: const Color(0xFFffffff),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  leading: ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(4.0)),
                                    child: Image.network(
                                      cpuCoolerGan.imageLink,
                                    ),
                                  ),
                                  title: Text(
                                    cpuCoolerGan.namaCooler,
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  subtitle: Text(
                                      "Rp ${formatter.format(int.parse(
                                              cpuCoolerGan.harga.toString()))}",
                                      style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: const Color(0xff52BB6F),
                                      )),
                                ),
                              ],
                            ),
                          )),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
      //
    } else {
      return ElevatedButton.icon(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                const Color.fromARGB(255, 121, 119, 255)),
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/part/list/cooler');
            setState(
              () {
                global.namaPart = "CPU Cooler";
              },
            );
          },
          icon: const Icon(Icons.add),
          label: Text('Pilih CPU Cooler',
              style: GoogleFonts.poppins(
                  fontSize: 17, fontWeight: FontWeight.w600)));
    }
  }

  Widget getMobo() {
    if (global.idMoboAdv != 0) {
      return FutureBuilder(
          future: fetchMotherboardId(global.idMoboAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Motherboard moboGan = snapshot.data[index];
                    global.namaMobo = moboGan.namaMobo;
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, '/part/list/motherboard');
                              setState(
                                () {
                                  global.hargaMobo = int.parse(moboGan.harga);
                                  global.namaPart = "Motherboard";
                                },
                              );
                            },
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                ListTile(
                                  selected: true,
                                  selectedTileColor: const Color(0xFFffffff),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  leading: ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(4.0)),
                                    child: Image.network(
                                      moboGan.imageLink,
                                    ),
                                  ),
                                  title: Text(
                                    moboGan.namaMobo,
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  subtitle: Text(
                                      "Rp ${formatter.format(int.parse(
                                              moboGan.harga.toString()))}",
                                      style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: const Color(0xff52BB6F),
                                      )),
                                ),
                              ],
                            ),
                          )),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
      //
    } else {
      return ElevatedButton.icon(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                const Color.fromARGB(255, 121, 119, 255)),
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/part/list/motherboard');
            setState(
              () {
                global.namaPart = "Motherboard";
              },
            );
          },
          icon: const Icon(Icons.add),
          label: Text('Pilih Motherboard',
              style: GoogleFonts.poppins(
                  fontSize: 17, fontWeight: FontWeight.w600)));
    }
  }

  Widget getPowerSupply() {
    if (global.idPsuAdv != 0) {
      return FutureBuilder(
          future: fetchPsuId(global.idPsuAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Psu psuGan = snapshot.data[index];
                    global.namaPsu = psuGan.namaPsu;
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/part/list/psu');
                              setState(
                                () {
                                  global.namaPart = "PSU";
                                  global.hargaPsu = int.parse(psuGan.harga);
                                },
                              );
                            },
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                ListTile(
                                  // leading: Image.network(psuGan.imageLink),
                                  selected: true,
                                  selectedTileColor: const Color(0xFFffffff),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  leading: ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(4.0)),
                                    child: Image.network(
                                      psuGan.imageLink,
                                    ),
                                  ),
                                  title: Text(
                                    psuGan.namaPsu,
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  subtitle: Text(
                                      "Rp ${formatter.format(int.parse(
                                              psuGan.harga.toString()))}",
                                      style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: const Color(0xff52BB6F),
                                      )),
                                ),
                              ],
                            ),
                          )),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
      //
    } else {
      return ElevatedButton.icon(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                const Color.fromARGB(255, 121, 119, 255)),
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/part/list/psu');
            setState(
              () {
                global.namaPart = "PSU";
              },
            );
          },
          icon: const Icon(Icons.add),
          label: Text('Pilih Power Supply',
              style: GoogleFonts.poppins(
                  fontSize: 17, fontWeight: FontWeight.w600)));
    }
  }

  Widget ramWidget(ramGan) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          selected: true,
          selectedTileColor: const Color(0xFFffffff),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 10.0),
          leading: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(4.0)),
            child: Image.network(
              ramGan.imageLink,
            ),
          ),
          title: Text(
            ramGan.namaRam,
            style: GoogleFonts.poppins(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          subtitle:
              Text("Rp ${formatter.format(int.parse(ramGan.harga.toString()))}",
                  style: GoogleFonts.poppins(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: const Color(0xff52BB6F),
                  )),
        ),
      ],
    );
  }

  Widget elseRam(int num) {
    return ElevatedButton.icon(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
              const Color.fromARGB(255, 121, 119, 255)),
        ),
        onPressed: () {
          Navigator.pushNamed(context, '/part/list/ram');
          setState(
            () {
              global.sengDiganti = num;
              global.namaPart = "Ram";
            },
          );
        },
        icon: const Icon(Icons.add),
        label: Text('Pilih RAM',
            style: GoogleFonts.poppins(
                fontSize: 17, fontWeight: FontWeight.w600)));
  }

  Widget getRam() {
    if (global.idRamAdv != 0) {
      return FutureBuilder(
          future: fetchRamId(global.idRamAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Ram ramGan = snapshot.data[index];
                    global.namaRam = ramGan.namaRam;
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/part/list/ram');
                                setState(
                                  () {
                                    global.sengDiganti = 1;
                                    global.namaPart = "Ram";
                                    global.hargaRam1 = int.parse(ramGan.harga);
                                  },
                                );
                              },
                              child: ramWidget(ramGan))),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
    } else {
      return elseRam(1);
    }
  }

  Widget getRamAgain() {
    if (global.idRam2Adv != 0) {
      return FutureBuilder(
          future: fetchRamId(global.idRam2Adv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Ram ramGan = snapshot.data[index];
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/part/list/ram');
                                setState(
                                  () {
                                    global.sengDiganti = 2;
                                    global.namaPart = "Ram";
                                    global.hargaRam2 = int.parse(ramGan.harga);
                                  },
                                );
                              },
                              child: ramWidget(ramGan))),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
    } else {
      return elseRam(2);
    }
  }

  Widget storateWidget(storageGan) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          selected: true,
          selectedTileColor: const Color(0xFFffffff),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 10.0),
          leading: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(4.0)),
            child: Image.network(
              storageGan.imageLink,
            ),
          ),
          title: Text(
            storageGan.namaStorage,
            style: GoogleFonts.poppins(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          subtitle: Text(
              "Rp ${formatter.format(int.parse(storageGan.harga.toString()))}",
              style: GoogleFonts.poppins(
                fontSize: 15,
                fontWeight: FontWeight.bold,
                color: const Color(0xff52BB6F),
              )),
        ),
      ],
    );
  }

  Widget elseStorage(int num) {
    return ElevatedButton.icon(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
              const Color.fromARGB(255, 121, 119, 255)),
        ),
        onPressed: () {
          Navigator.pushNamed(context, '/part/list/storage');
          setState(
            () {
              global.sengDiganti = num;
              global.namaPart = "Storage";
            },
          );
        },
        icon: const Icon(Icons.add),
        label: Text('Pilih Storage',
            style: GoogleFonts.poppins(
                fontSize: 17, fontWeight: FontWeight.w600)));
  }

  Widget getStorage() {
    if (global.idStorageAdv != 0) {
      return FutureBuilder(
          future: fetchStorageId(global.idStorageAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Storage storageGan = snapshot.data[index];
                    global.namaStorage = storageGan.namaStorage;
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, '/part/list/storage');
                                setState(
                                  () {
                                    global.sengDiganti = 1;
                                    global.namaPart = "Storage";
                                    global.hargaStorage1 =
                                        int.parse(storageGan.harga);
                                  },
                                );
                              },
                              child: storateWidget(storageGan))),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
    } else {
      return elseStorage(1);
    }
  }

  Widget getStorageAgain() {
    if (global.idStorage2Adv != 0) {
      return FutureBuilder(
          future: fetchStorageId(global.idStorage2Adv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Storage storageGan = snapshot.data[index];
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, '/part/list/storage');
                                setState(
                                  () {
                                    global.sengDiganti = 2;
                                    global.namaPart = "Storage";
                                    global.hargaStorage2 =
                                        int.parse(storageGan.harga);
                                  },
                                );
                              },
                              child: storateWidget(storageGan))),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
      //
    } else {
      return elseStorage(2);
    }
  }

  Widget getVga() {
    if (global.idVgaAdv != 0) {
      return FutureBuilder(
          future: fetchVgaId(global.idVgaAdv),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Vga vgaGan = snapshot.data[index];
                    global.namaVga = vgaGan.namaVga;
                    return SizedBox(
                      width: 200,
                      height: 90,
                      child: Card(
                          color: Colors.transparent,
                          elevation: 00,
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/part/list/vga');
                              setState(
                                () {
                                  global.namaPart = "VGA";
                                  global.hargaVga = 69;
                                },
                              );
                            },
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                ListTile(
                                  // leading: Image.network(vgaGan.imageLink),
                                  selected: true,
                                  selectedTileColor: const Color(0xFFffffff),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  leading: ClipRRect(
                                    borderRadius: const BorderRadius.all(
                                        const Radius.circular(4.0)),
                                    child: Image.network(
                                      vgaGan.imageLink,
                                    ),
                                  ),
                                  title: Text(
                                    vgaGan.namaVga,
                                    style: GoogleFonts.poppins(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  subtitle: Text(
                                      "Rp ${formatter.format(int.parse(
                                              vgaGan.harga.toString()))}",
                                      style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: const Color(0xff52BB6F),
                                      )),
                                ),
                              ],
                            ),
                          )),
                    );
                  });
            }
            return const CircularProgressIndicator();
          });
      //
    } else {
      return ElevatedButton.icon(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                const Color.fromARGB(255, 121, 119, 255)),
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/part/list/vga');
            setState(
              () {
                global.namaPart = "VGA";
              },
            );
          },
          icon: const Icon(Icons.add),
          label: Text('Pilih VGA',
              style: GoogleFonts.poppins(
                  fontSize: 17, fontWeight: FontWeight.w600)));
    }
  }
}
