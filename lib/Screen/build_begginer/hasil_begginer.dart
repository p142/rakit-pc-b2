// ignore_for_file: prefer_const_constructors
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:quickalert/quickalert.dart';
import 'package:rakit_pc/Api/api_builds_id.dart';
import 'package:rakit_pc/Models/models_builds.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:rakit_pc/global.dart' as global;

import 'package:percent_indicator/percent_indicator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../../Api/api_simpan_upload.dart';

class RekomendasiPc extends StatefulWidget {
  const RekomendasiPc({Key? key}) : super(key: key);

  @override
  State<RekomendasiPc> createState() => _RekomendasiPCState();
}

class _RekomendasiPCState extends State<RekomendasiPc> {
  var formatter = NumberFormat('#,###,000');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff342C4C),
      body: FutureBuilder<List>(
          future: BuildsApi.fetchBuildsId(),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    Builds build = snapshot.data[index];

                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.1,
                            width: MediaQuery.of(context).size.width * 0.90,
                            child: LinearPercentIndicator(
                              width: MediaQuery.of(context).size.width * 0.90,
                              lineHeight: 30,
                              percent: 100 / 100,
                              animation: true,
                              animationDuration: 500,
                              animateFromLastPercent: true,
                              barRadius: Radius.circular(20),
                              backgroundColor: HexColor("#C9C9C9"),
                              progressColor: HexColor("#EE5584"),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(const Radius.circular(10.0)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Align(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Rekomendasi Sistem',
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(
                                            fontSize: 24,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ]),
                                Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Align(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "Rp ${formatter
                                                  .format(int.parse(build
                                                      .hargaBuilds
                                                      .toString()))}",
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.poppins(
                                            fontSize: 36,
                                            color: Color(0xff52BB6F),
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ]),
                                const SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 10),
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 90),
                                  height:
                                      MediaQuery.of(context).size.height * 0.05,
                                  child: ElevatedButton.icon(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor:
                                            Colors.white.withOpacity(0.2),
                                      ),
                                      onPressed: () {
                                        Clipboard.setData(ClipboardData(
                                            text: "Rakitan Build tanggal ${DateTime.now().year}/${DateTime.now()
                                                    .month}/${DateTime.now().day}\n\nCPU : ${build.namaCpu}\nHarga : Rp ${formatter
                                                    .format(int.parse(build
                                                        .hargaCpu
                                                        .toString()))}\n\nVGA : ${build.namaVga}\nHarga : Rp ${formatter
                                                    .format(int.parse(build
                                                        .hargaVga
                                                        .toString()))}\n\nRAM : ${build.namaRam}\nHarga : Rp ${formatter
                                                    .format(int.parse(build
                                                        .hargaRam
                                                        .toString()))}\n\nStorage : ${build.namaStorage}\nHarga : Rp ${formatter
                                                    .format(int.parse(
                                                        build.hargaStorage.toString()))}\n\nPower Supply : ${build.namaPsu}\nHarga : Rp ${formatter.format(int.parse(build.hargaPsu.toString()))}\n\nCPU Cooler : ${build.namaCpuCooler}\nHarga : Rp ${formatter.format(int.parse(build.hargaCpuCooler.toString()))}\n\nFan : ${build.namaFans}\nHarga : Rp ${formatter.format(int.parse(build.hargaFans.toString()))}\n\nCasing : ${build.namaCasing}\nHarga : Rp ${formatter.format(int.parse(build.hargaCasing.toString()))}\n\nHarga Total : Rp ${formatter.format(int.parse(build.hargaBuilds.toString()))}"));
                                        Fluttertoast.showToast(
                                            msg:
                                                "Build Berhasil di Copy ke Clipboard !");
                                      },
                                      icon: Icon(Icons.copy),
                                      label: Text("Copy to Clipboard")),
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                printCpu(build),
                                const SizedBox(
                                  height: 10,
                                ),
                                printMobo(build),
                                const SizedBox(
                                  height: 10,
                                ),
                                printRam(build),
                                const SizedBox(
                                  height: 10,
                                ),
                                printVga(build),
                                const SizedBox(
                                  height: 10,
                                ),
                                printStorage(build.storage, build.imgStorage,
                                    build.namaStorage, build.hargaStorage),
                                const SizedBox(
                                  height: 10,
                                ),
                                if (int.parse(build.storage2) != 0) ...[
                                  printStorage(
                                      build.storage2,
                                      build.imgStorage2,
                                      build.namaStorage2,
                                      build.hargaStorage2),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ],
                                printPsu(build),
                                const SizedBox(
                                  height: 10,
                                ),
                                printCpuCooler(build),
                                const SizedBox(
                                  height: 10,
                                ),
                                printFanCooler(build),
                                const SizedBox(
                                  height: 10,
                                ),
                                printCase(build),
                                const SizedBox(
                                  height: 5,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            height: 50,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.white),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12),
                                  ))),
                              child: Text(
                                "Kustomisasi Build",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                  color: HexColor("#9997FF"),
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: () {
                                setState(() {
                                  global.dariSimpan = true;
                                  global.idCaseAdv = int.parse(build.casing);
                                  global.hargaCase =
                                      int.parse(build.hargaCasing);
                                  global.idCpuAdv = int.parse(build.cpu);
                                  global.hargaCpu = int.parse(build.hargaCpu);
                                  global.idMoboAdv =
                                      int.parse(build.motherboard);
                                  global.hargaMobo = int.parse(build.hargaMobo);
                                  global.idVgaAdv = int.parse(build.vga);
                                  if (build.hargaVga.toLowerCase() != "no") {
                                    global.hargaVga = int.parse(build.hargaVga);
                                  } else {
                                    global.hargaVga = 0;
                                  }
                                  global.idCpuCoolerAdv =
                                      int.parse(build.cpuCooler);
                                  global.hargaCooler =
                                      int.parse(build.hargaCpuCooler);
                                  global.idFanAdv = int.parse(build.fans);
                                  global.hargaFan1 = int.parse(build.hargaFans);
                                  global.idFan2Adv = int.parse(build.fans);
                                  global.hargaFan2 = int.parse(build.hargaFans);
                                  global.idFan3Adv = int.parse(build.fans);
                                  global.hargaFan3 = int.parse(build.hargaFans);
                                  global.idPsuAdv = int.parse(build.psu);
                                  global.hargaPsu = int.parse(build.hargaPsu);
                                  global.idStorageAdv =
                                      int.parse(build.storage);
                                  global.hargaStorage1 =
                                      int.parse(build.hargaStorage);
                                  global.idStorage2Adv =
                                      int.parse(build.storage2);
                                  if (build.hargaStorage2.toLowerCase() !=
                                      "no") {
                                    global.hargaStorage2 =
                                        int.parse(build.hargaStorage2);
                                  } else {
                                    global.hargaStorage2 = 0;
                                  }
                                  global.idRamAdv = int.parse(build.ram);
                                  global.hargaRam1 = int.parse(build.hargaRam);
                                  global.idRam2Adv = 0;
                                });
                                Navigator.pushNamed(context, '/build/advanced');
                              },
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                width: 140,
                                height: 54,
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.white),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(14),
                                      ))),
                                  child: Text(
                                    "Beli Build",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                      color: HexColor("#9997FF"),
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  onPressed: () async {
                                    final url = build.links;

                                    if (await canLaunchUrlString(url)) {
                                      await launchUrlString(url);
                                    }
                                  },
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              SizedBox(
                                width: 140,
                                height: 54,
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.white),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(14),
                                      ))),
                                  child: Text(
                                    "Simpan Build",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.poppins(
                                      color: HexColor("#9997FF"),
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold,
                                      height: 1.2,
                                    ),
                                  ),
                                  onPressed: () async {
                                    bool response = await postData(
                                      global.uidUser,
                                      "1",
                                      build.hargaBuilds,
                                      build.casing,
                                      build.hargaCasing,
                                      build.cpu,
                                      build.hargaCpu,
                                      build.cpuCooler,
                                      build.hargaCpuCooler,
                                      build.motherboard,
                                      build.hargaMobo,
                                      build.psu,
                                      build.hargaPsu,
                                      build.ram,
                                      build.hargaRam,
                                      "0",
                                      "0",
                                      build.storage,
                                      build.hargaStorage,
                                      build.storage2,
                                      build.hargaStorage2,
                                      build.vga,
                                      build.hargaVga,
                                      build.fans,
                                      build.hargaFans,
                                      build.fans,
                                      build.hargaFans,
                                      build.fans,
                                      build.hargaFans,
                                    );

                                    if (response) {
                                      QuickAlert.show(
                                          context: context,
                                          type: QuickAlertType.success,
                                          text: 'Build Berhasil Disimpan!',
                                          confirmBtnText: 'Oke',
                                          confirmBtnColor: HexColor("#12c06a"),
                                          onConfirmBtnTap: () {
                                            Navigator.pushNamed(
                                                context, '/history');
                                          });
                                    } else {
                                      QuickAlert.show(
                                        context: context,
                                        type: QuickAlertType.error,
                                        title: 'Error',
                                        text: 'Ada Kesalahan, Coba Lagi Nanti',
                                      );
                                    }
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 100.0,
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  });
            } else {
              return SizedBox(
                height: MediaQuery.of(context).size.height / 1.3,
                child: const Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                  ),
                ),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/build');
        },
        backgroundColor: HexColor("#7A77FF"),
        child: const Icon(
          Icons.build,
          size: 30,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(),
    );
  }

  Widget printCpu(build) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(build.cpu) - 1;
          global.namaPart = "CPU";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(build.imgCpu),
                        fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      build.namaCpu,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      "Rp ${formatter
                              .format(int.parse(build.hargaCpu.toString()))}",
                      style: GoogleFonts.poppins(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff52BB6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printMobo(build) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(build.motherboard) - 1;
          global.namaPart = "Motherboard";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(build.imgMobo),
                        fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      build.namaMobo,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      "Rp ${formatter
                              .format(int.parse(build.hargaMobo.toString()))}",
                      style: GoogleFonts.poppins(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff52BB6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printRam(build) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(build.ram) - 1;
          global.namaPart = "Ram";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(build.imgRam),
                        fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      build.namaRam,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      "Rp ${formatter
                              .format(int.parse(build.hargaRam.toString()))}",
                      style: GoogleFonts.poppins(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff52BB6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printVga(build) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(build.vga) - 1;
          global.namaPart = "VGA";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(build.imgVga),
                        fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      build.namaVga,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: int.parse(build.hargaVga) == 0
                        ? Text(
                            "Gratis dengan CPU",
                            style: GoogleFonts.poppins(
                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff52BB6F)),
                          )
                        : Text(
                            "Rp ${formatter
                                    .format(
                                        int.parse(build.hargaVga.toString()))}",
                            style: GoogleFonts.poppins(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff52BB6F),
                            ),
                          ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printStorage(String idStorage, String imgStorage, String namaStorage,
      String hargaStorage) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(idStorage) - 1;
          global.namaPart = "Storage";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(imgStorage), fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      namaStorage,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      "Rp ${formatter
                              .format(int.parse(hargaStorage.toString()))}",
                      style: GoogleFonts.poppins(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff52BB6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printPsu(build) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(build.psu) - 1;
          global.namaPart = "PSU";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(build.imgPsu),
                        fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      build.namaPsu,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      "Rp ${formatter
                              .format(int.parse(build.hargaPsu.toString()))}",
                      style: GoogleFonts.poppins(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff52BB6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printCpuCooler(build) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(build.cpuCooler) - 1;
          global.namaPart = "CPU Cooler";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(build.imgCpuCooler),
                        fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      build.namaCpuCooler,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: int.parse(build.hargaCpuCooler) == 0
                        ? Text(
                            "Gratis dengan CPU",
                            style: GoogleFonts.poppins(
                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff52BB6F)),
                          )
                        : Text(
                            "Rp ${formatter
                                    .format(int.parse(
                                        build.hargaCpuCooler.toString()))}",
                            style: GoogleFonts.poppins(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff52BB6F),
                            ),
                          ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printFanCooler(build) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(build.fans) - 1;
          global.namaPart = "Fan";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(build.imgFans),
                        fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      build.namaFans,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      "Rp ${formatter
                              .format(int.parse(build.hargaFans.toString()))}",
                      style: GoogleFonts.poppins(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff52BB6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget printCase(build) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/part/list/detail');
        setState(() {
          global.idDetail = int.parse(build.casing) - 1;
          global.namaPart = "Casing";
        });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        width: 350,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                width: 70,
                height: 70,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(build.imgCasing),
                        fit: BoxFit.contain)),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      build.namaCasing,
                      style: GoogleFonts.poppins(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
                    child: Text(
                      "Rp ${formatter
                              .format(int.parse(build.hargaCasing.toString()))}",
                      style: GoogleFonts.poppins(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff52BB6F),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
