// ignore_for_file: unused_import, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:rakit_pc/Screen/build_begginer/hasil_begginer.dart';
import 'package:rakit_pc/Screen/build_begginer/how_it_works.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:percent_indicator/percent_indicator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';

import '../home_page/floating_button.dart';

class Pertanyaan3 extends StatefulWidget {
  const Pertanyaan3({Key? key}) : super(key: key);

  @override
  State<Pertanyaan3> createState() => _Pertanyaan3State();
}

class _Pertanyaan3State extends State<Pertanyaan3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor("#272B40"),
      body: Center(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.1,
                width: MediaQuery.of(context).size.width * 0.90,
                child: LinearPercentIndicator(
                  width: MediaQuery.of(context).size.width * 0.90,
                  lineHeight: 30,
                  percent: 80 / 100,
                  animation: true,
                  animationDuration: 500,
                  animateFromLastPercent: true,
                  barRadius: const Radius.circular(20),
                  backgroundColor: HexColor("#C9C9C9"),
                  progressColor: HexColor("#EE5584"),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.03,
                width: MediaQuery.of(context).size.width * 0.77,
                alignment: Alignment.topLeft,
                child: Text(
                  "Pertanyaan 3",
                  textAlign: TextAlign.left,
                  style: GoogleFonts.poppins(
                    fontSize: 16.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    height: 1.5,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.09,
                width: MediaQuery.of(context).size.width * 0.77,
                alignment: Alignment.bottomLeft,
                child: Text(
                  "Seberapa besar memori penyimpanan yang anda butuhkan ?",
                  textAlign: TextAlign.left,
                  style: GoogleFonts.poppins(
                    fontSize: 16.0,
                    color: Colors.white,
                    height: 1.5,
                  ),
                ),
              ),
              const SizedBox(
                height: 140,
              ),
              Container(
                  height: 40,
                  width: 320,
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: OutlinedButton(
                    onPressed: () {
                      setState(() {
                        global.storageButuh = 1;
                        global.compatible = "All parts compatible";
                      });
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeftWithFade,
                              child: RekomendasiPc()));
                    },
                    style: OutlinedButton.styleFrom(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      side: const BorderSide(
                        color: Colors.white,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("Sangat Kecil ~256GB",
                          style: GoogleFonts.poppins(
                            fontSize: 16.0,
                            color: Colors.white,
                            height: 1.5,
                          ),
                          textAlign: TextAlign.left),
                    ),
                  )),
              const SizedBox(height: 20),
              Container(
                  height: 40,
                  width: 320,
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: OutlinedButton(
                    onPressed: () {
                      setState(() {
                        global.storageButuh = 2;
                      });
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeftWithFade,
                              child: RekomendasiPc()));
                    },
                    style: OutlinedButton.styleFrom(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      side: const BorderSide(
                        color: Colors.white,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("Kecil ~512GB",
                          style: GoogleFonts.poppins(
                            fontSize: 16.0,
                            color: Colors.white,
                            height: 1.5,
                          ),
                          textAlign: TextAlign.left),
                    ),
                  )),
              const SizedBox(height: 20),
              Container(
                  height: 40,
                  width: 320,
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: OutlinedButton(
                    onPressed: () {
                      setState(() {
                        global.storageButuh = 3;
                      });
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeftWithFade,
                              child: RekomendasiPc()));
                    },
                    style: OutlinedButton.styleFrom(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      side: const BorderSide(
                        color: Colors.white,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("Sedang ~1TB",
                          style: GoogleFonts.poppins(
                            fontSize: 16.0,
                            color: Colors.white,
                            height: 1.5,
                          ),
                          textAlign: TextAlign.left),
                    ),
                  )),
              const SizedBox(height: 20),
              Container(
                  height: 40,
                  width: 320,
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: OutlinedButton(
                    onPressed: () {
                      setState(() {
                        global.storageButuh = 4;
                      });
                      Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.rightToLeftWithFade,
                              child: RekomendasiPc()));
                    },
                    style: OutlinedButton.styleFrom(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      side: const BorderSide(
                        color: Colors.white,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text("Besar 2TB++",
                          style: GoogleFonts.poppins(
                            fontSize: 16.0,
                            color: Colors.white,
                            height: 1.5,
                          ),
                          textAlign: TextAlign.left),
                    ),
                  )),
              const SizedBox(height: 24),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/build/begginer/pertanyaan2');
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    backgroundColor: HexColor("#7A77FF"),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 25, vertical: 10),
                    textStyle: GoogleFonts.poppins(
                      fontSize: 16.0,
                    )),
                child: const Text('Kembali'),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: const BottomBar(),
    );
  }
}
