// ignore_for_file: prefer_const_constructors, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:quickalert/quickalert.dart';
import 'package:rakit_pc/Models/models_simpan_build_cloud.dart';
import 'package:rakit_pc/Screen/home_page/bottom_navbar.dart';
import 'package:rakit_pc/global.dart' as global;
import 'package:intl/intl.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../Api/api_simpan_build_id.dart';
import '../../Api/api_simpan_build_delete.dart';
import '../home_page/floating_button.dart';

class SimpanBuildNew extends StatefulWidget {
  SimpanBuildNew({Key? key}) : super(key: key);

  @override
  State<SimpanBuildNew> createState() => _SimpanBuildNewState();
}

class _SimpanBuildNewState extends State<SimpanBuildNew> {
  var formatter = NumberFormat('#,###,000');
  List<SimpanBuildCloud> builds = [];

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff272B40),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Padding(
          padding: const EdgeInsets.all(8),
          child: GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
            child: const Icon(Icons.arrow_back_rounded),
          ),
        ),
        title: const Text(
          'History Build',
          textAlign: TextAlign.center,
        ),
      ),
      body: FutureBuilder<List>(
          future: fetchSimpanBuildId(global.uidUser),
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data!.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, index) {
                    SimpanBuildCloud simpanBuild = snapshot.data[index];
                    return Container(
                        margin: const EdgeInsets.only(bottom: 5),
                        child: Card(
                            color: Colors.transparent,
                            elevation: 6,
                            margin: const EdgeInsets.fromLTRB(10, 8, 10, 8),
                            child: SizedBox(
                                width: 200,
                                height: 90,
                                child: ListTile(
                                  leading: Image.asset(
                                    "assets/img/partgan.png",
                                    width: 57,
                                    height: 57,
                                  ),
                                  title: Container(
                                    margin: const EdgeInsets.only(top: 5),
                                    child: Text(
                                      simpanBuild.namaBuild
                                          .toString()
                                          .substring(0, 16),
                                      style: GoogleFonts.poppins(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      simpanBuild.compatible == 1
                                          ? Text(
                                              "Semua part kompatible",
                                              style: GoogleFonts.poppins(
                                                color: const Color(0xff52BB6F),
                                                fontSize: 12,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            )
                                          : Text(
                                              "Ada part tidak kompatible",
                                              style: GoogleFonts.poppins(
                                                color: const Color(0xffEE3C6D),
                                                fontSize: 12,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                      Text(
                                        "Rp ${formatter
                                                .format(int.parse(simpanBuild
                                                    .hargaTotal
                                                    .toString()))}",
                                        style: GoogleFonts.poppins(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 15,
                                        ),
                                      ),
                                    ],
                                  ),
                                  trailing: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          QuickAlert.show(
                                              context: context,
                                              type: QuickAlertType.warning,
                                              title: "Warning",
                                              text:
                                                  '\nApakah Yakin Menghapus Build',
                                              confirmBtnText: 'Iya',
                                              cancelBtnText: 'Tidak',
                                              showCancelBtn: true,
                                              confirmBtnColor: Colors.green,
                                              onConfirmBtnTap: () async {
                                                bool response =
                                                    await deleteSimpanBuildId(
                                                        simpanBuild.idSimpan);

                                                if (response) {
                                                  init();
                                                  Navigator.pop(context);
                                                } else {
                                                  QuickAlert.show(
                                                    context: context,
                                                    type: QuickAlertType.error,
                                                    title: 'Error',
                                                    text:
                                                        'Mohon Maaf, Coba Lagi Nanti',
                                                  );
                                                }
                                              },
                                              onCancelBtnTap: () {
                                                Navigator.pop(context);
                                              });
                                        },
                                        icon: const Icon(Icons.delete),
                                        iconSize: 30,
                                      ),
                                    ],
                                  ),
                                  selected: true,
                                  selectedTileColor: const Color(0xFFffffff),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      global.dariSimpan = true;
                                      global.idCaseAdv = simpanBuild.idCasing;
                                      global.hargaCase =
                                          simpanBuild.hargaCasing;
                                      global.idCpuAdv = simpanBuild.idCpu;
                                      global.hargaCpu = simpanBuild.hargaCpu;
                                      global.idMoboAdv =
                                          simpanBuild.idMotherboard;
                                      global.hargaMobo =
                                          simpanBuild.hargaMotherboard;
                                      global.idVgaAdv = simpanBuild.idVga;
                                      global.hargaVga = simpanBuild.hargaVga;
                                      global.idCpuCoolerAdv =
                                          simpanBuild.idCpuCooler;
                                      global.hargaCooler =
                                          simpanBuild.hargaCpuCooler;
                                      global.idFanAdv = simpanBuild.idFan1;
                                      global.hargaFan1 = simpanBuild.hargaFan1;
                                      global.idFan2Adv = simpanBuild.idFan2;
                                      global.hargaFan2 = simpanBuild.hargaFan2;
                                      global.idFan3Adv = simpanBuild.idFan3;
                                      global.hargaFan3 = simpanBuild.hargaFan3;
                                      global.idPsuAdv = simpanBuild.idPsu;
                                      global.hargaPsu = simpanBuild.hargaPsu;
                                      global.idStorageAdv =
                                          simpanBuild.idStorage1;
                                      global.hargaStorage1 =
                                          simpanBuild.hargaStorage1;
                                      global.idStorage2Adv =
                                          simpanBuild.idStorage2;
                                      global.hargaStorage2 =
                                          simpanBuild.hargaStorage2;
                                      global.idRamAdv = simpanBuild.idRam1;
                                      global.hargaRam1 = simpanBuild.hargaRam1;
                                      global.idRam2Adv = simpanBuild.idRam2;
                                      global.hargaRam2 = simpanBuild.hargaRam2;
                                    });
                                    Navigator.pushNamed(
                                        context, '/build/advanced');
                                  },
                                ))));
                  });
            } else {
              return Center(
                child: CircularProgressIndicator(
                  color: Colors.white,
                ),
              );
            }
          }),
      floatingActionButton: FloatingBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: const BottomBar(),
    );
  }
}
