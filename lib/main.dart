// ignore_for_file: prefer_const_constructors

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:rakit_pc/Screen/build_advanced/build_advanced_new.dart';
import 'package:rakit_pc/Screen/build_begginer/hasil_begginer.dart';
import 'package:rakit_pc/Screen/build_begginer/pertanyaan_2.dart';
import 'package:rakit_pc/Screen/build_begginer/pertanyaan_3.dart';
import 'package:rakit_pc/Screen/build_begginer/pertanyaan_awal.dart';
import 'package:rakit_pc/Screen/build_begginer/how_it_works.dart';
import 'package:rakit_pc/Login/Screens/Welcome/welcome_screen.dart';
import 'package:rakit_pc/Screen/compare_part/compare_cpu.dart';

import 'package:rakit_pc/Screen/home_page/homepage.dart';
import 'package:rakit_pc/Screen/build_begginer/pertanyaan_1.dart';
import 'package:rakit_pc/Screen/part_page/detail_part.dart';
import 'package:rakit_pc/Screen/part_page/list_casing.dart';
import 'package:rakit_pc/Screen/part_page/list_cpu.dart';
import 'package:rakit_pc/Screen/part_page/list_cpu_cooler.dart';
import 'package:rakit_pc/Screen/part_page/list_fan.dart';
import 'package:rakit_pc/Screen/part_page/list_keyboard.dart';
import 'package:rakit_pc/Screen/part_page/list_monitor.dart';
import 'package:rakit_pc/Screen/part_page/list_motherboard.dart';
import 'package:rakit_pc/Screen/part_page/list_mouse.dart';
import 'package:rakit_pc/Screen/part_page/list_psu.dart';
import 'package:rakit_pc/Screen/part_page/list_ram.dart';
import 'package:rakit_pc/Screen/part_page/list_storage.dart';
import 'package:rakit_pc/Screen/part_page/list_vga.dart';
import 'package:rakit_pc/Screen/part_page/part_page.dart';

import 'package:rakit_pc/global.dart' as global;
import 'package:firebase_core/firebase_core.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Screen/compare_part/compare_cpu_cooler.dart';
import 'Screen/compare_part/compare_fan.dart';
import 'Screen/compare_part/compare_mobo.dart';
import 'Screen/compare_part/compare_psu.dart';
import 'Screen/compare_part/compare_ram.dart';
import 'Screen/compare_part/compare_storage.dart';
import 'Screen/compare_part/compare_vga.dart';
import 'Screen/compare_part/how_it_works_compare.dart';
import 'Screen/compare_part/list_part_compare.dart';
import 'Screen/simpan_build/simpan_build_new.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SharedPreferences logindata = await SharedPreferences.getInstance();
  var email = logindata.getString('email');
  if (email != null) {
    global.currentState = HomePage();
  } else {
    global.currentState = WelcomeScreen();
  }

  runApp(MaterialApp(
    theme: ThemeData(
      scaffoldBackgroundColor: Color(0xff9837a5),
    ),
    initialRoute: '/',
    routes: {
      '/': (context) => StreamBuilder(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (ctx, snapShot) {
            if (snapShot.hasData) {
              return HomePage();
            } else {
              return WelcomeScreen();
            }
          }),
      '/history': (context) => SimpanBuildNew(),
      '/build': (context) => HowItWorks(), //compare_cpu(),
      '/buildAwal': (context) => PertanyaanAwal(),
      '/build/advanced': (context) => BuildAdvanced(),
      '/build/begginer/pertanyaan1': (context) => Pertanyaan1(),
      '/build/begginer/pertanyaan2': (context) => Pertanyaan2(),
      '/build/begginer/pertanyaan3': (context) => Pertanyaan3(),
      '/build/begginer/hasil': (context) => RekomendasiPc(),
      '/part': (context) => Part(),
      '/part/list/casing': (context) => ListCasing(),
      '/part/list/cooler': (context) => ListCpuCooler(),
      '/part/list/cpu': (context) => ListCpu(),
      '/part/list/fan': (context) => ListFan(),
      '/part/list/motherboard': (context) => ListMotherboard(),
      '/part/list/psu': (context) => ListPsu(),
      '/part/list/ram': (context) => ListRam(),
      '/part/list/storage': (context) => ListStorage(),
      '/part/list/vga': (context) => ListVga(),
      '/part/list/monitor': (context) => ListMonitor(),
      '/part/list/mouse': (context) => ListMouse(),
      '/part/list/keyboard': (context) => ListKeyboard(),
      '/part/list/detail': (context) => DetailPart(),
      '/compare': (context) => HowItWorksCompare(),
      '/compare/list': (context) => ListCompare(),
      '/compare/cpu': (context) => CompareCpu(),
      '/compare/ram': (context) => CompareRam(),
      '/compare/vga': (context) => CompareVga(),
      '/compare/fan': (context) => CompareFan(),
      '/compare/storage': (context) => CompareStorage(),
      '/compare/psu': (context) => ComparePsu(),
      '/compare/cpucooler': (context) => CompareCpuCooler(),
      '/compare/motherboard': (context) => CompareMobo(),
      '/compare/cpu/list': (context) => ListCpu(),
      '/compare/cpucooler/list': (context) => ListCpuCooler(),
      '/compare/ram/list': (context) => ListRam(),
      '/compare/vga/list': (context) => ListVga(),
      '/compare/fan/list': (context) => ListFan(),
      '/compare/storage/list': (context) => ListStorage(),
      '/compare/motherboard/list': (context) => ListMotherboard(),
      '/compare/psu/list': (context) => ListPsu(),
    },
  ));
}
