// To parse this JSON data, do
//
//     final monitor = monitorFromJson(jsonString);

import 'dart:convert';

List<Monitor> monitorFromJson(String str) =>
    List<Monitor>.from(json.decode(str).map((x) => Monitor.fromJson(x)));

String monitorToJson(List<Monitor> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Monitor {
  Monitor({
    required this.idMonitor,
    required this.namaMonitor,
    required this.merkMonitor,
    required this.monitorResolusi,
    required this.monitorRefreshRate,
    required this.monitorAspectRatio,
    required this.monitorPort,
    required this.monitorSize,
    required this.ultrawide,
    required this.panelType,
    required this.hdr,
    required this.screenTechnology,
    required this.harga,
    required this.imageLink,
    required this.links,
  });

  int idMonitor;
  String namaMonitor;
  String merkMonitor;
  String monitorResolusi;
  int monitorRefreshRate;
  String monitorAspectRatio;
  String monitorPort;
  int monitorSize;
  String ultrawide;
  String panelType;
  String hdr;
  String screenTechnology;
  int harga;
  String imageLink;
  String links;

  factory Monitor.fromJson(Map<String, dynamic> json) => Monitor(
        idMonitor: json["idMonitor"],
        namaMonitor: json["NamaMonitor"],
        merkMonitor: json["MerkMonitor"],
        monitorResolusi: json["MonitorResolusi"],
        monitorRefreshRate: json["MonitorRefreshRate"],
        monitorAspectRatio: json["MonitorAspectRatio"],
        monitorPort: json["MonitorPort"],
        monitorSize: json["MonitorSize"],
        ultrawide: json["Ultrawide"],
        panelType: json["PanelType"],
        hdr: json["HDR"],
        screenTechnology: json["ScreenTechnology"],
        harga: json["Harga"],
        imageLink: json["ImageLink"],
        links: json["Links"],
      );

  Map<String, dynamic> toJson() => {
        "idMonitor": idMonitor,
        "NamaMonitor": namaMonitor,
        "MerkMonitor": merkMonitor,
        "MonitorResolusi": monitorResolusi,
        "MonitorRefreshRate": monitorRefreshRate,
        "MonitorAspectRatio": monitorAspectRatio,
        "MonitorPort": monitorPort,
        "MonitorSize": monitorSize,
        "Ultrawide": ultrawide,
        "PanelType": panelType,
        "HDR": hdr,
        "ScreenTechnology": screenTechnology,
        "Harga": harga,
        "ImageLink": imageLink,
        "Links": links,
      };
}
