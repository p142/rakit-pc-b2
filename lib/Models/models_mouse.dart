// To parse this JSON data, do
//
//     final mouse = mouseFromJson(jsonString);

import 'dart:convert';

List<Mouse> mouseFromJson(String str) =>
    List<Mouse>.from(json.decode(str).map((x) => Mouse.fromJson(x)));

String mouseToJson(List<Mouse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Mouse {
  Mouse({
    required this.idMouse,
    required this.namaMouse,
    required this.merkMouse,
    required this.dpiMaxMouse,
    required this.dpiMouse,
    required this.mouseColor,
    required this.sensorMouse,
    required this.totalButton,
    required this.weight,
    required this.wireless,
    required this.imageLinks,
    required this.links,
    required this.harga,
    required this.rgb,
  });

  int idMouse;
  String namaMouse;
  String merkMouse;
  int dpiMaxMouse;
  String dpiMouse;
  String mouseColor;
  String sensorMouse;
  String totalButton;
  int weight;
  String wireless;
  String imageLinks;
  String links;
  int harga;
  String rgb;

  factory Mouse.fromJson(Map<String, dynamic> json) => Mouse(
        idMouse: json["idMouse"],
        namaMouse: json["NamaMouse"],
        merkMouse: json["MerkMouse"],
        dpiMaxMouse: json["DpiMaxMouse"],
        dpiMouse: json["DpiMouse"],
        mouseColor: json["MouseColor"],
        sensorMouse: json["SensorMouse"],
        totalButton: json["TotalButton"],
        weight: json["Weight"],
        wireless: json["Wireless"],
        imageLinks: json["ImageLinks"],
        links: json["Links"],
        harga: json["Harga"],
        rgb: json["RGB"],
      );

  Map<String, dynamic> toJson() => {
        "idMouse": idMouse,
        "NamaMouse": namaMouse,
        "MerkMouse": merkMouse,
        "DpiMaxMouse": dpiMaxMouse,
        "DpiMouse": dpiMouse,
        "MouseColor": mouseColor,
        "SensorMouse": sensorMouse,
        "TotalButton": totalButton,
        "Weight": weight,
        "Wireless": wireless,
        "ImageLinks": imageLinks,
        "Links": links,
        "Harga": harga,
        "RGB": rgb,
      };
}
