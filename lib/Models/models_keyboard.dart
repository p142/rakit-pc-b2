// To parse this JSON data, do
//
//     final keyboard = keyboardFromJson(jsonString);

import 'dart:convert';

List<Keyboard> keyboardFromJson(String str) =>
    List<Keyboard>.from(json.decode(str).map((x) => Keyboard.fromJson(x)));

String keyboardToJson(List<Keyboard> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Keyboard {
  Keyboard({
    required this.idKeyboard,
    required this.namaKeyboard,
    required this.merkKeyboard,
    required this.mechanical,
    required this.switchType,
    required this.keyboardType,
    required this.wireless,
    required this.rgb,
    required this.imageLink,
    required this.keyboardColor,
    required this.harga,
    required this.links,
  });

  int idKeyboard;
  String namaKeyboard;
  String merkKeyboard;
  String mechanical;
  String switchType;
  String keyboardType;
  String wireless;
  String rgb;
  String imageLink;
  String keyboardColor;
  int harga;
  String links;

  factory Keyboard.fromJson(Map<String, dynamic> json) => Keyboard(
        idKeyboard: json["idKeyboard"],
        namaKeyboard: json["NamaKeyboard"],
        merkKeyboard: json["MerkKeyboard"],
        mechanical: json["Mechanical"],
        switchType: json["SwitchType"],
        keyboardType: json["KeyboardType"],
        wireless: json["Wireless"],
        rgb: json["RGB"],
        imageLink: json["ImageLink"],
        keyboardColor: json["KeyboardColor"],
        harga: json["Harga"],
        links: json["Links"],
      );

  Map<String, dynamic> toJson() => {
        "idKeyboard": idKeyboard,
        "NamaKeyboard": namaKeyboard,
        "MerkKeyboard": merkKeyboard,
        "Mechanical": mechanical,
        "SwitchType": switchType,
        "KeyboardType": keyboardType,
        "Wireless": wireless,
        "RGB": rgb,
        "ImageLink": imageLink,
        "KeyboardColor": keyboardColor,
        "Harga": harga,
        "Links": links,
      };
}
