// To parse this JSON data, do
//
//     final simpanBuildCloud = simpanBuildCloudFromJson(jsonString);

import 'dart:convert';

List<SimpanBuildCloud> simpanBuildCloudFromJson(String str) =>
    List<SimpanBuildCloud>.from(
        json.decode(str).map((x) => SimpanBuildCloud.fromJson(x)));

String simpanBuildCloudToJson(List<SimpanBuildCloud> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SimpanBuildCloud {
  SimpanBuildCloud({
    required this.idSimpan,
    required this.namaBuild,
    required this.idUser,
    required this.compatible,
    required this.hargaTotal,
    required this.idCasing,
    required this.hargaCasing,
    required this.idCpu,
    required this.hargaCpu,
    required this.idCpuCooler,
    required this.hargaCpuCooler,
    required this.idMotherboard,
    required this.hargaMotherboard,
    required this.idPsu,
    required this.hargaPsu,
    required this.idRam1,
    required this.hargaRam1,
    required this.idRam2,
    required this.hargaRam2,
    required this.idStorage1,
    required this.hargaStorage1,
    required this.idStorage2,
    required this.hargaStorage2,
    required this.idVga,
    required this.hargaVga,
    required this.idFan1,
    required this.hargaFan1,
    required this.idFan2,
    required this.hargaFan2,
    required this.idFan3,
    required this.hargaFan3,
    required this.createdAt,
    required this.updatedAt,
  });

  int idSimpan;
  DateTime namaBuild;
  String idUser;
  int compatible;
  int hargaTotal;
  int idCasing;
  int hargaCasing;
  int idCpu;
  int hargaCpu;
  int idCpuCooler;
  int hargaCpuCooler;
  int idMotherboard;
  int hargaMotherboard;
  int idPsu;
  int hargaPsu;
  int idRam1;
  int hargaRam1;
  int idRam2;
  int hargaRam2;
  int idStorage1;
  int hargaStorage1;
  int idStorage2;
  int hargaStorage2;
  int idVga;
  int hargaVga;
  int idFan1;
  int hargaFan1;
  int idFan2;
  int hargaFan2;
  int idFan3;
  int hargaFan3;
  DateTime createdAt;
  DateTime updatedAt;

  factory SimpanBuildCloud.fromJson(Map<String, dynamic> json) =>
      SimpanBuildCloud(
        idSimpan: json["IdSimpan"],
        namaBuild: DateTime.parse(json["NamaBuild"]),
        idUser: json["IdUser"],
        compatible: json["Compatible"],
        hargaTotal: json["HargaTotal"],
        idCasing: json["IdCasing"],
        hargaCasing: json["HargaCasing"],
        idCpu: json["IdCpu"],
        hargaCpu: json["HargaCpu"],
        idCpuCooler: json["IdCpuCooler"],
        hargaCpuCooler: json["HargaCpuCooler"],
        idMotherboard: json["IdMotherboard"],
        hargaMotherboard: json["HargaMotherboard"],
        idPsu: json["IdPsu"],
        hargaPsu: json["HargaPsu"],
        idRam1: json["IdRam1"],
        hargaRam1: json["HargaRam1"],
        idRam2: json["IdRam2"],
        hargaRam2: json["HargaRam2"],
        idStorage1: json["IdStorage1"],
        hargaStorage1: json["HargaStorage1"],
        idStorage2: json["IdStorage2"],
        hargaStorage2: json["HargaStorage2"],
        idVga: json["IdVga"],
        hargaVga: json["HargaVga"],
        idFan1: json["IdFan1"],
        hargaFan1: json["HargaFan1"],
        idFan2: json["IdFan2"],
        hargaFan2: json["HargaFan2"],
        idFan3: json["IdFan3"],
        hargaFan3: json["HargaFan3"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "IdSimpan": idSimpan,
        "NamaBuild": namaBuild,
        "IdUser": idUser,
        "Compatible": compatible,
        "HargaTotal": hargaTotal,
        "IdCasing": idCasing,
        "HargaCasing": hargaCasing,
        "IdCpu": idCpu,
        "HargaCpu": hargaCpu,
        "IdCpuCooler": idCpuCooler,
        "HargaCpuCooler": hargaCpuCooler,
        "IdMotherboard": idMotherboard,
        "HargaMotherboard": hargaMotherboard,
        "IdPsu": idPsu,
        "HargaPsu": hargaPsu,
        "IdRam1": idRam1,
        "HargaRam1": hargaRam1,
        "IdRam2": idRam2,
        "HargaRam2": hargaRam2,
        "IdStorage1": idStorage1,
        "HargaStorage1": hargaStorage1,
        "IdStorage2": idStorage2,
        "HargaStorage2": hargaStorage2,
        "IdVga": idVga,
        "HargaVga": hargaVga,
        "IdFan1": idFan1,
        "HargaFan1": hargaFan1,
        "IdFan2": idFan2,
        "HargaFan2": hargaFan2,
        "IdFan3": idFan3,
        "HargaFan3": hargaFan3,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
