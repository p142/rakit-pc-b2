// To parse required this JSON data, do
//
//     final builds = buildsFromJson(jsonString);

import 'dart:convert';

List<Builds> buildsFromJson(String str) =>
    List<Builds>.from(json.decode(str).map((x) => Builds.fromJson(x)));

String buildsToJson(List<Builds> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Builds {
  Builds({
    required this.idBuilds,
    required this.kebutuhanBuilds,
    required this.budgetBuilds,
    required this.kebutuhanStorage,
    required this.motherboard,
    required this.namaMobo,
    required this.hargaMobo,
    required this.imgMobo,
    required this.cpu,
    required this.namaCpu,
    required this.hargaCpu,
    required this.imgCpu,
    required this.ram,
    required this.namaRam,
    required this.hargaRam,
    required this.imgRam,
    required this.vga,
    required this.namaVga,
    required this.hargaVga,
    required this.imgVga,
    required this.psu,
    required this.namaPsu,
    required this.hargaPsu,
    required this.imgPsu,
    required this.cpuCooler,
    required this.namaCpuCooler,
    required this.hargaCpuCooler,
    required this.imgCpuCooler,
    required this.storage,
    required this.namaStorage,
    required this.hargaStorage,
    required this.imgStorage,
    required this.storage2,
    required this.namaStorage2,
    required this.hargaStorage2,
    required this.imgStorage2,
    required this.fans,
    required this.namaFans,
    required this.hargaFans,
    required this.imgFans,
    required this.casing,
    required this.namaCasing,
    required this.hargaCasing,
    required this.imgCasing,
    required this.imgLinks,
    required this.hargaBuilds,
    required this.links,
  });

  String idBuilds;
  String kebutuhanBuilds;
  String budgetBuilds;
  String kebutuhanStorage;
  String motherboard;
  String namaMobo;
  String hargaMobo;
  String imgMobo;
  String cpu;
  String namaCpu;
  String hargaCpu;
  String imgCpu;
  String ram;
  String namaRam;
  String hargaRam;
  String imgRam;
  String vga;
  String namaVga;
  String hargaVga;
  String imgVga;
  String psu;
  String namaPsu;
  String hargaPsu;
  String imgPsu;
  String cpuCooler;
  String namaCpuCooler;
  String hargaCpuCooler;
  String imgCpuCooler;
  String storage;
  String namaStorage;
  String hargaStorage;
  String imgStorage;
  String storage2;
  String namaStorage2;
  String hargaStorage2;
  String imgStorage2;
  String fans;
  String namaFans;
  String hargaFans;
  String imgFans;
  String casing;
  String namaCasing;
  String hargaCasing;
  String imgCasing;
  String imgLinks;
  String hargaBuilds;
  String links;

  factory Builds.fromJson(Map<String, dynamic> json) => Builds(
        idBuilds: json["idBuilds"].toString(),
        kebutuhanBuilds: json["KebutuhanBuilds"].toString(),
        budgetBuilds: json["BudgetBuilds"].toString(),
        kebutuhanStorage: json["KebutuhanStorage"].toString(),
        motherboard: json["Motherboard"].toString(),
        namaMobo: json["NamaMobo"].toString(),
        hargaMobo: json["HargaMobo"].toString(),
        imgMobo: json["ImgMobo"].toString(),
        cpu: json["Cpu"].toString(),
        namaCpu: json["NamaCpu"].toString(),
        hargaCpu: json["HargaCpu"].toString(),
        imgCpu: json["ImgCpu"].toString(),
        ram: json["Ram"].toString(),
        namaRam: json["NamaRam"].toString(),
        hargaRam: json["HargaRam"].toString(),
        imgRam: json["ImgRam"].toString(),
        vga: json["VGA"].toString(),
        namaVga: json["NamaVga"].toString(),
        hargaVga: json["HargaVga"].toString(),
        imgVga: json["ImgVga"].toString(),
        psu: json["PSU"].toString(),
        namaPsu: json["NamaPsu"].toString(),
        hargaPsu: json["HargaPsu"].toString(),
        imgPsu: json["ImgPsu"].toString(),
        cpuCooler: json["CpuCooler"].toString(),
        namaCpuCooler: json["NamaCpuCooler"].toString(),
        hargaCpuCooler: json["HargaCpuCooler"].toString(),
        imgCpuCooler: json["ImgCpuCooler"].toString(),
        storage: json["Storage"].toString(),
        namaStorage: json["NamaStorage"].toString(),
        hargaStorage: json["HargaStorage"].toString(),
        imgStorage: json["ImgStorage"].toString(),
        storage2: json["Storage2"].toString(),
        namaStorage2: json["NamaStorage2"].toString(),
        hargaStorage2: json["HargaStorage2"].toString(),
        imgStorage2: json["ImgStorage2"].toString(),
        fans: json["Fans"].toString(),
        namaFans: json["NamaFans"].toString(),
        hargaFans: json["HargaFans"].toString(),
        imgFans: json["ImgFans"].toString(),
        casing: json["Casing"].toString(),
        namaCasing: json["NamaCasing"].toString(),
        hargaCasing: json["HargaCasing"].toString(),
        imgCasing: json["ImgCasing"].toString(),
        imgLinks: json["ImgLinks"].toString(),
        hargaBuilds: json["HargaBuilds"].toString(),
        links: json["Links"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "idBuilds": idBuilds,
        "KebutuhanBuilds": kebutuhanBuilds,
        "BudgetBuilds": budgetBuilds,
        "KebutuhanStorage": kebutuhanStorage,
        "Motherboard": motherboard,
        "NamaMobo": namaMobo,
        "HargaMobo": hargaMobo,
        "ImgMobo": imgMobo,
        "Cpu": cpu,
        "NamaCpu": namaCpu,
        "HargaCpu": hargaCpu,
        "ImgCpu": imgCpu,
        "Ram": ram,
        "NamaRam": namaRam,
        "HargaRam": hargaRam,
        "ImgRam": imgRam,
        "VGA": vga,
        "NamaVga": namaVga,
        "HargaVga": hargaVga,
        "ImgVga": imgVga,
        "PSU": psu,
        "NamaPsu": namaPsu,
        "HargaPsu": hargaPsu,
        "ImgPsu": imgPsu,
        "CpuCooler": cpuCooler,
        "NamaCpuCooler": namaCpuCooler,
        "HargaCpuCooler": hargaCpuCooler,
        "ImgCpuCooler": imgCpuCooler,
        "Storage": storage,
        "NamaStorage": namaStorage,
        "HargaStorage": hargaStorage,
        "ImgStorage": imgStorage,
        "Storage2": storage2,
        "NamaStorage2": namaStorage2,
        "HargaStorage2": hargaStorage2,
        "ImgStorage2": imgStorage2,
        "Fans": fans,
        "NamaFans": namaFans,
        "HargaFans": hargaFans,
        "ImgFans": imgFans,
        "Casing": casing,
        "NamaCasing": namaCasing,
        "HargaCasing": hargaCasing,
        "ImgCasing": imgCasing,
        "ImgLinks": imgLinks,
        "HargaBuilds": hargaBuilds,
        "Links": links,
      };
}
