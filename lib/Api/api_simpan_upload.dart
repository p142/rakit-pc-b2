import 'package:http/http.dart' as http;

Future postData(
  String idUser,
  String compatible,
  String hargaTotal,
  String idCasing,
  String hargaCasing,
  String idCpu,
  String hargaCpu,
  String idCpuCooler,
  String hargaCpuCooler,
  String idMotherboard,
  String hargaMotherboard,
  String idPsu,
  String hargaPsu,
  String idRam1,
  String hargaRam1,
  String idRam2,
  String hargaRam2,
  String idStorage1,
  String hargaStorage1,
  String idStorage2,
  String hargaStorage2,
  String idVga,
  String hargaVga,
  String idFan1,
  String hargaFan1,
  String idFan2,
  String hargaFan2,
  String idFan3,
  String hargaFan3,
) async {
  if (hargaStorage2 != "No") {
  } else {
    hargaStorage2 = "0";
  }
  final url = Uri.parse("http://103.105.78.75/api/SimpanBuild/Upload");

  final response = await http.post(url, body: {
    'IdUser': idUser,
    'Compatible': compatible.toString(),
    'HargaTotal': hargaTotal.toString(),
    'IdCasing': idCasing.toString(),
    'HargaCasing': hargaCasing.toString(),
    'IdCpu': idCpu.toString(),
    'HargaCpu': hargaCpu.toString(),
    'IdCpuCooler': idCpuCooler.toString(),
    'HargaCpuCooler': hargaCpuCooler.toString(),
    'IdMotherboard': idMotherboard.toString(),
    'HargaMotherboard': hargaMotherboard.toString(),
    'IdPsu': idPsu.toString(),
    'HargaPsu': hargaPsu.toString(),
    'IdRam1': idRam1.toString(),
    'HargaRam1': hargaRam1.toString(),
    'IdRam2': idRam2.toString(),
    'HargaRam2': hargaRam2.toString(),
    'IdStorage1': idStorage1.toString(),
    'HargaStorage1': hargaStorage1.toString(),
    'IdStorage2': idStorage2.toString(),
    'HargaStorage2': hargaStorage2.toString(),
    'IdVga': idVga.toString(),
    'HargaVga': hargaVga.toString(),
    'IdFan1': idFan1.toString(),
    'HargaFan1': hargaFan1.toString(),
    'IdFan2': idFan2.toString(),
    'HargaFan2': hargaFan2.toString(),
    'IdFan3': idFan3.toString(),
    'HargaFan3': hargaFan3.toString(),
  });

  if (response.statusCode == 200) {
    return true;
  } else {
    return false;
  }
}
