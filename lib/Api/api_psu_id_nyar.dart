import 'dart:convert';
import 'package:rakit_pc/Models/models_psu.dart';
import 'package:http/http.dart' as http;

class PsuApi {
  static Future<List<Psu>> fetchPsuIdNyar(String query, String request) async {
    final url = Uri.parse("http://103.105.78.75/api/Psu/Filter");
    //103.105.78.75/api/Cpu/Filter
    final response =
        await http.post(url, body: {'Request': request.toString()});

    if (response.statusCode == 200) {
      final List psuu = json.decode(response.body);

      return psuu.map((json) => Psu.fromJson(json)).where((psu) {
        final namaLower = psu.namaPsu.toLowerCase();
        final merkLower = psu.merkPsu.toLowerCase();
        final searchLower = query.toLowerCase();

        return namaLower.contains(searchLower) ||
            merkLower.contains(searchLower);
      }).toList();
    } else {
      throw Exception();
    }
  }
}
