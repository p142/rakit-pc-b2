import 'package:http/http.dart' as http;

import '../Models/models_simpan_build_cloud.dart';

Future<List<SimpanBuildCloud>> fetchSimpanBuildId(String uid) async {
  final url = Uri.parse("http://103.105.78.75/api/SimpanBuild/User/$uid");
  final response = await http.get(url);

  if (response.statusCode == 200) {
    return simpanBuildCloudFromJson(response.body);
  } else {
    throw Exception('Failed to load ');
  }
}
