import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_keyboard.dart';

Future<List<Keyboard>> fetchKeyboard() async {
  String link = "http://103.105.78.75/api/Keyboard/All";
  final response = await http.get(Uri.parse(link));

  if (response.statusCode == 200) {
    return keyboardFromJson(response.body);
  } else {
    throw Exception('Failed to load Monitor');
  }
}
