import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_cpu.dart';

Future<List<Cpu>> fetchCpuId(int id) async {
  var response = await http
      .get(Uri.parse("http://103.105.78.75/api/Cpu/$id"));

  if (response.statusCode == 200) {
    return cpuFromJson(response.body);
  } else {
    throw Exception('Failed to load ');
  }
}
