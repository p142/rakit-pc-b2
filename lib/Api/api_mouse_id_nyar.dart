import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_mouse.dart';

class MouseApi {
  static Future<List<Mouse>> fetchMouseIdNyar(
      String query, String request) async {
    final url = Uri.parse("http://103.105.78.75/api/Mouse/Filter");
    final response =
        await http.post(url, body: {'Request': request.toString()});

    if (response.statusCode == 200) {
      final List mousee = json.decode(response.body);

      return mousee.map((json) => Mouse.fromJson(json)).where((mouse) {
        final namaLower = mouse.namaMouse.toLowerCase();
        final merkLower = mouse.merkMouse.toLowerCase();
        final searchLower = query.toLowerCase();

        return namaLower.contains(searchLower) ||
            merkLower.contains(searchLower);
      }).toList();
    } else {
      throw Exception();
    }
  }
}
