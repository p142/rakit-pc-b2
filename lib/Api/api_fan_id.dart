import 'package:http/http.dart' as http;

import 'package:rakit_pc/Models/models_fan.dart';

Future<List<Fan>> fetchFanId(int id) async {
  String link = "http://103.105.78.75/api/Fan/$id";
  final response = await http.get(Uri.parse(link));

  if (response.statusCode == 200) {
    return fanFromJson(response.body);
  } else {
    throw Exception('Failed to load ');
  }
}
