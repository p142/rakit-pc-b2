import 'package:http/http.dart' as http;

Future<bool> deleteSimpanBuildId(int id) async {
  final url =
      Uri.parse("http://103.105.78.75/api/SimpanBuild/Delete/$id");
  final response = await http.get(url);

  if (int.parse(response.body) == 1) {
    return true;
  } else {
    return false;
  }
}
