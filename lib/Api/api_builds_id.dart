import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_builds.dart';
import 'package:rakit_pc/global.dart' as global;

class BuildsApi {
  static Future<List<Builds>> fetchBuildsId() async {
    String link = "http://103.105.78.75/api/Builds/Detail";

    var response = await http.post(Uri.parse(link), body: {
      'Kebutuhan': global.kegunaan.toString(),
      'Budget': global.budget.toString(),
      'Storage': global.storageButuh.toString()
    });

  
      return buildsFromJson(response.body);
   
  }
}
