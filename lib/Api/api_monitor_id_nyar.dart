import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_monitor.dart';

class MonitorApi {
  static Future<List<Monitor>> fetchMonitorIdNyar(
      String query, String request) async {
    final url = Uri.parse("http://103.105.78.75/api/Monitor/Filter");
    final response =
        await http.post(url, body: {'Request': request.toString()});

    if (response.statusCode == 200) {
      final List monitorr = json.decode(response.body);

      return monitorr.map((json) => Monitor.fromJson(json)).where((monit) {
        final namaLower = monit.namaMonitor.toLowerCase();
        final merkLower = monit.merkMonitor.toLowerCase();
        final searchLower = query.toLowerCase();

        return namaLower.contains(searchLower) ||
            merkLower.contains(searchLower);
      }).toList();
    } else {
      throw Exception();
    }
  }
}
