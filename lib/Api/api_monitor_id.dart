import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_monitor.dart';

Future<List<Monitor>> fetchMonitorId(int id) async {
  String link = "http://103.105.78.75/api/Monitor/$id";

  final response = await http.get(Uri.parse(link));

  if (response.statusCode == 200) {
    return monitorFromJson(response.body);
  } else {
    throw Exception('Failed to load ');
  }
}
