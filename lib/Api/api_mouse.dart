import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_mouse.dart';

Future<List<Mouse>> fetchMouse() async {
  String link = "http://103.105.78.75/api/Mouse/All";
  final response = await http.get(Uri.parse(link));

  if (response.statusCode == 200) {
    return mouseFromJson(response.body);
  } else {
    throw Exception('Failed to load Monitor');
  }
}
