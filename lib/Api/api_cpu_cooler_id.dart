import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_cpu_cooler.dart';

Future<List<CpuCooler>> fetchCpuCoolerId(int id) async {
  String link = "http://103.105.78.75/api/CpuCooler/$id";
  final response = await http.get(Uri.parse(link));

  if (response.statusCode == 200) {
    return cpuCoolerFromJson(response.body);
  } else {
    throw Exception('Failed to load ');
  }
}
