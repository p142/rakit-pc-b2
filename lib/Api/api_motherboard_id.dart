import 'package:http/http.dart' as http;

import 'package:rakit_pc/Models/models_motherboard.dart';

Future<List<Motherboard>> fetchMotherboardId(int id) async {
  String link = "http://103.105.78.75/api/Motherboard/$id";

  final response = await http.get(Uri.parse(link));

  if (response.statusCode == 200) {
    return motherboardFromJson(response.body);
  } else {
    throw Exception('Failed to load ');
  }
}
