import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_keyboard.dart';

class KeyboardApi {
  static Future<List<Keyboard>> fetchKeyboardIdNyar(
      String query, String request) async {
    final url = Uri.parse("http://103.105.78.75/api/Keyboard/Filter");
    final response =
        await http.post(url, body: {'Request': request.toString()});

    if (response.statusCode == 200) {
      final List keyboardd = json.decode(response.body);

      return keyboardd.map((json) => Keyboard.fromJson(json)).where((keyb) {
        final namaLower = keyb.namaKeyboard.toLowerCase();
        final merkLower = keyb.merkKeyboard.toLowerCase();
        final searchLower = query.toLowerCase();

        return namaLower.contains(searchLower) ||
            merkLower.contains(searchLower);
      }).toList();
    } else {
      throw Exception();
    }
  }
}
