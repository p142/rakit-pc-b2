import 'package:http/http.dart' as http;
import 'package:rakit_pc/Models/models_cpu.dart';

Future<List<Cpu>> fetchCpu() async {
  const String link = "http://103.105.78.75/api/Cpu/All";
  final response = await http.get(Uri.parse(link));

  if (response.statusCode == 200) {
    return cpuFromJson(response.body);
  } else {
    throw Exception('Failed to load Cpu');
  }
}
